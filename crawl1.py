import re
from robobrowser import RoboBrowser
import requests
import pandas as pd
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def get_data(em, pass1):
    browser = RoboBrowser(history=True)

    '''Navigate browser to Wunderground's historical weather page'''
    browser.open("https://cobanqah101.usgovvirginia.cloudapp.usgovcloudapi.net:803/", verify=False)

    '''Find the form to input zip code and date'''
    form = browser.get_forms()
    form['email'] = em
    form['password'] = pass1

#    browser.submit_form(form)
    html = str(browser.parsed)

    data = pd.read_html(html)[0]
    return data

get_data("gsnext1", 123 )