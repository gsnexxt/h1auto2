import ERB24
ERB24_BOARD_NUM = 0


def PowersOn():
    print("Turning on Powers...")
    for xx in range (0, 8):
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=xx)


#
# Entry point for module
#
if (__name__ == "__main__"):
    PowersOn()
