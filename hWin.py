# -*- coding: utf-8 -*-
"""
hWin.py - hWin (Henry Witwicki)

The following is an attempt to compile a set of useful utilities.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""


__version__ = '1.06'
__date__    = '2018/09/07'
__author__  = 'Henry Witwicki'


#--- CHANGES ----------------------------------------------------------------
#
# 2018/09/07 - hWin, added hWin_SetupLogging()
#hWin_WriteToLog


#--- IMPORTS ----------------------------------------------------------------
import json
import logging
import logging.config
import os


# 
# Values of interest:
SIZE_LBA = 512
SIZE_KB  = 1024
SIZE_MB  = SIZE_KB * SIZE_KB
SIZE_GB  = SIZE_MB * SIZE_KB

SIZE_siKB = 1000
SIZE_siMB = SIZE_siKB * SIZE_siKB
#


#
# The following will calculate the NMEA 0183 checksum.
#
#     Note: http://www.hhhh.org/wiml/proj/nmeaxor.html can be used to verify
#
def hWin_NMEA0183Checksum(s):
    """Calculate a NMEA 0183 checksum."""
    DEBUG = False

    result = 0

    if DEBUG:
        s = "$GPRMC,183733.000,A,4913.4407,N,12249.2003,W,0.06,31.11,091008,,*2D"

    for c in s:
        if (c == '$'):
            continue
        elif (c == '*'):
            break
        else:
            result ^= ord(c)

    if DEBUG:
        print("result = %02X" %result)

    return result


#
# The following will reverse the order of a string.
#
def hWin_strReverse(s):
    """Simply reverse the characters in a srting."""

    txt = []
    for i in range(len(s)-1, -1, -1):
        txt.append(s[i])
    return ''.join(txt)


#
# The following will append a string to a log file.
#
def hWin_WriteToLog(filename, s):
    """Append a string to a log file."""

    try:
        hFile = open(filename, 'a')
    except IOError:
        print("Error: The file (%s) could not be opened!" % (filename))
    else:
        try:
            hFile.write(s)
        except IOError:
            print("Error: The file (%s) could not be written!" % (filename))

        hFile.close()


#
# The following will get the current line number.
#
# Usage:  print "Warning: Something is wrong at line %d" % (hWin_GetLineNum())
#
from inspect import currentframe
def hWin_GetLineNum():
    """Returns the current line number."""

    cf = currentframe()
    return cf.f_back.f_lineno


#
# The following makes use of the logging module.
#
# Usage:
#        logging.debug('Debug something')
#        logging.info('Info something')
#        logging.warning('Warning something')
#        logging.error('Error something')
#
def hWin_SetupLogging( 
    default_path = 'Logging.json', 
    default_level = logging.DEBUG, 
    env_key = 'LOG_CFG'):
    """Setup logging module"""

    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)

