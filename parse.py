import logging
import time

def meta_matcher(meta, metafile):
#    try:
        #metafile = 'Metadata_file.log'
        #meta=[['3456', '3885014', '7889280', 'Dernwood Street', 'David', 'mnop', 'SPD', 'SPD', 'Gender = MaleRace = Caucassssianthe truck ', 'Marks'], ['7890', '1011925', '7219242', 'Ellington Street', 'Michael', 'qrst', 'CAD', 'CAD', 'Gender = FemaleRace = CaucassssianThere was the into ', 'Bowman'], ['7890', '4990607', '4814198', 'Ellington Street', 'Trevor', 'qrst', 'CAD', 'CAD', 'Gender = FemaleRace = Caucassssianinto the ', 'Bowman']]
        #len(meta[2])
        #time.sleep(10)
        file1 = open(metafile, 'r')
        #file1 = open('Metadata_file.log', 'r')
        lines = file1.readlines()

        print("The metadata list has following number of metadata sets:: ", len(meta))
        i = 0

        match_list = []

        for i in range((len(meta)) - 1):

            for line in lines:
                if meta[i][5] in line:
                    if 'Description="ticket2' in line:
                        file=(line.split('"')[1])
                        if file in match_list:
                            continue
                        else:
                            match_list.append(file)
        print("Enterred metadata matches in the following video list, this must have been the list of videos recorded ::\n", match_list)
        file1.close()
        return match_list

#    except:
#        print("Some Error Ocurred While Matchin Video List with Metadata")

def parser(msgtype):
    try:
        file1 = open('Logfile.log', 'r')
        lines = file1.readlines()
        Warns = []
        Errors = list()
        Infos = list()
        restofem = list()

        for line in lines:
            if "WARN" in line:
                Warns.append(line)

            elif "INFO" in line:
                Infos.append(line)

            elif "ERROR" in line:
                Errors.append(line)

            else:
                restofem.append(line)
        print("loop ended")

        message_to_get = msgtype
        if message_to_get in ["E", "e", "error", "ERROR", "Error"]:
            logging.info("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: Following ERRORS WERE SEEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
            for error in Errors:
                print(error)
        elif message_to_get in ["W", "w", "warning", "WARN", "Warning"]:
            logging.info("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: Following WARNS WERE SEEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
            for warning in Warns:
                print(warning)
        elif message_to_get in ["I", "i", "info", "INFO", "Info"]:
            logging.info("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: Following INFO MSGS WERE SEEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
            for info in Infos:
                print(info)
            file1.close()
    except:
        print("Something Went Wrong While Getting Runtime Info")

def ipc_checker(msgtype):
    try:
        file1 = open('Logfile.log', 'r')
        lines1 = file1.readlines()
        Warns = []
        IpcYes = list()
        IpcNo = list()
        restofem = list()
        file2 = open('ipc.log', 'r')
        lines2 = file2.readlines()

        for line1 in lines1:
            for line2 in lines2:
                if line2 in line1:
                    IpcYes.append(line2)
                else:
                    print("ipc based log line not found")
                    #IpcNo.append(line2)
        print("IPC Log Check ended")

        message_to_get = msgtype
        if message_to_get in ["ipc"]:
            print(":::::::::::::::::::::::::::::::::::::::::::::::::::: Following IPC Related Messages WERE SEEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
            for log1 in IpcYes:
                print(log1)
            print(":::::::::::::::::::::::::::::::::::::::::::::::::::: Following IPC Related Messages WERE NOT SEEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
            for log2 in IpcNo:
                print(log2)
        else:
            print("ipc keyword not found, check argument passed")
        file1.close()
        file2.close()
    except:
        print("Something Went Wrong While Getting Runtime Info")
#parser("E")
def template_checker():
        try:
            file2 = open('template.config', 'r')
            lines = file2.readlines()
            if (len(lines) <= 270):
                print("Template.config not of expected size and has the following number of lines" , len(lines))
            else:
                print("Template not empty, number of lines:: ", len(lines))
                pass
            file2.close()
        except:
            print("template size checker function failed")

def fwtimer():
    try:
        file2 = open('Logfile.log', 'r')
        lines = file2.readlines()
        timestamps = []
        dates = list()
        times = list()
        datetimes = list()
        restofem = list()
        from datetime import datetime

        for line in lines:
            if "[tcp" in line:
                timestamp = line.split(".")[0]
                date1 = timestamp.split("T")[0]
                dates.append(date1)
                time1 = timestamp.split("T")[1]
                datetime_object = datetime.strptime(time1, '%H:%M:%S')
                datetimes.append(datetime_object)
                times.append(time1)
                timestamps.append(timestamp)
        logging.info("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: RUNTIME INFO ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
        print("Finished time check")
        print("FW Start time:: " + timestamps[0] + "    FW End Time:: " + timestamps[-1])
        #print("Timestamps:: ")#print(timestamps)#print("Times::  ")#print(times)#print("Datetimes:: ")#print(datetimes)
        runtime = datetimes[-1] - datetimes[0]
        print("Firmware Runtime:: " + str(runtime))
    except:
        print("Something Went Wrong While Fetching Runtime Info")

#Mosquitto ver: 1.6.8
#fwtimer()
'''
def autotimer():
    file2 = open('Test_run.log.txt', 'r')
    lines = file2.readlines()
    timestamps = list()
    dates = list()
    times = list()
    datetimes = list()
    restofem = list()
    from datetime import datetime
    try:
        for line in lines:
            if "Starting H1 Automation" or "ENDED" in line:
                timestamp = line.split(" - ")[0]
                date1 = timestamp.split(" ")[0]
                dates.append(date1)
                time1 = timestamp.split(" ")[1]
                time_object = time1.split(",")[0]
                datetime_object = datetime.strptime(time_object, '%H:%M:%S')
                datetimes.append(datetime_object)
                times.append(time_object)
                timestamps.append(timestamp)
    except(IndexError):
        print("Saw Index Error")

    except:
        print("Something went wrong while fetching the rest")
'''
#autotimer()
    #print("en ded time check")
    #print("FW Start time:: " + timestamps[0] + "    FW End Time:: " + timestamps[-1])
    ##print("Timestamps:: ")#print(timestamps)#print("Times::  ")#print(times)#print("Datetimes:: ")#print(datetimes)
    #runtime = datetimes[-1] - datetimes[0]
    #print("Firmware Runtime:: " + str(runtime))
'''
    log_file = GetLogFile(h1, 'runlog')
    text_file = open('Logfile.log', 'w')
    text_file.writelines(log_file)
    text_file.close()
'''