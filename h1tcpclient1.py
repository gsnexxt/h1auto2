#!/usr/bin/python

from __future__ import print_function
import socket
# import fcntl
import os
import errno
import struct
import binascii
import json
import select

json.dumps
try:
    import tkinter as tk
except:
    import Tkinter as tk

root = tk.Tk()
tk.Tk()

def spawnprocess(args):
    if os.fork() ==  0:
        os.execvp(args[0],args)

class H1:
    def __init__(self):
        self.socket = None
        #root.after(100,self.poll)
        self.buffer = b''
        self.host = "10.0.1.248"
        self.pollstatus = False
        self.pollstatusperiod = 1000
        self.serialized = ""

    def setpollstatus(self, on):
        self.pollstatus = on

    def getpollstatus(self):
        return self.pollstatus

    def serializedResponse(self):
        return self.serialized

    def connect(self,host,port):
        if not self.socket:
            print("connect")
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            try:
                self.socket.connect((str(host), int(port)))

            except Exception as e:
                print("something's wrong with %s:%d. Exception is %s" % (str(host), int(port), e))
                self.socket.close()
                self.socket = None
                return False

        self.host = host
        # fcntl.fcntl(self.socket, fcntl.F_SETFL, os.O_NONBLOCK)
        self.socket.setblocking(False)
        return True

    def disconnect(self):
        if self.socket:
            print("disconnect")
            self.socket.close()
            self.socket = None
            self.buffer = b''

    def receiveBinaryData(self):
        if self.socket:
            notready = False
            #data = ''
            data = bytearray(8192)
            bMoreData = True

            recv_buffer_size = 8192
            bytesReceived = 0
            total_data = bytearray(1024 * 1024 * 4)
            tempBuf = bytearray(1024 * 1024 * 4)
            dst = 0

            self.socket.setblocking(0)

            ready = select.select([self.socket], [], [], 5)

            if ready[0]:
                while bMoreData:
                    try:
                        bytesReceived = self.socket.recv_into(data, 8192)
                        i = 0
                        if data:
                            #bytesReceived += data
                            for x in range (bytesReceived):
                                total_data[dst + i] = data[x]
                                i += 1
                            dst += bytesReceived
                            if bytesReceived < 8192:
                                bMoreData = False             
                        else:
                            bMoreData = false

                    except socket.error as e:            
                        self.socket.setblocking(1)
                        return ""

                len = total_data[3]
                more = total_data[len + 4];
                value1 = (total_data[len] << 24) | (total_data[len + 1] << 16) | (total_data[len + 2] << 8) | total_data[len + 3]

                dst = 0

                index = len + 8;
                srcIndex = value1 + len;

                for x in range (index, srcIndex):
                    tempBuf[dst] = total_data[x]
                    dst += 1
                    index += 1

                while (more > 0):
                    srcIndex += (total_data[index] << 24) | (total_data[index + 1] << 16) | (total_data[index + 2] << 8) | total_data[index + 3]
                    more = total_data[index + 4]
                    index += 8
                    for x in range (index, srcIndex):
                        tempBuf[dst] = total_data[x]
                        dst += 1
                        index += 1

                newBuf = bytearray(dst + 1)

                for i in range(dst):
                    newBuf[i] = tempBuf[i]
                encoding = 'utf-8'
                output = str(newBuf, encoding)
                self.socket.setblocking(1)
                return output
            #print(output)

    def ReceiveStringResponse(self):
        notready = False
        tempBuf = bytearray(1024 * 1024 * 4)
        bytesReceived = 0

        if self.socket:

            self.socket.setblocking(0)
            ready = select.select([self.socket], [], [], 5)

            if ready[0]:
                try:
                    bytesReceived = self.socket.recv_into(tempBuf)
                    newBuf = bytearray(bytesReceived + 1)

                    dst = 0

                    for src in range( 8, bytesReceived):
                        source = tempBuf[src] 
                        if tempBuf[src] == 10:
                            #src += 1
                            continue
                        newBuf[dst] = tempBuf[src]
                        dst += 1

                    encoding = 'utf-8'
                    output = str(newBuf, encoding)


                    fin = output.find("}")
                    s =   output[0:fin + 1]

                    #print(output)
                    dict = json.loads(s)
                    self.socket.setblocking(1)
                    return dict

                except socket.error as e:            
                    self.socket.setblocking(1)
                    return ""

    def poll(self):
        if self.socket:
            # print("poll")
            notready = False
            data = ''
            try:
                data = self.socket.recv(8192)
            except socket.error as e:
                err = e.args[0]
                if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
                    notready = True
                else:
                    print('Error', err)

            if len(data) == 0:
                if not notready:
                    self.socket.close()
                    self.socket = None
                    print("disconnected")
            else:
                self.gotdata(data)

            if self.socket and self.pollstatus:
                self.pollstatusperiod -= 100
                if self.pollstatusperiod <= 0:
                    self.sendjson({"command":"status"})
                    self.pollstatusperiod = 1000

        #root.after(100,self.poll)

    def gotdata(self,data):
        self.buffer += data
        while True:
            if len(self.buffer) < 4:
                if len(self.buffer) != 0:
                    print("too short for length")
                break
            (length,) = struct.unpack(">i",self.buffer[0:4])
            if len(self.buffer) < length:
                print("too short for data: buffer is", len(self.buffer), "want", length)
                break
            # print("message length",length,"buffer",len(self.buffer))
            m = self.buffer[4:length]
            self.gotmessage(m)
            # print("after got", len(self.buffer))
            self.buffer = self.buffer[length:]

    def gotmessage(self,message):
        # print("Got Message:",len(message))
        if len(message) < 4:
            print("No Message header")
            return
        (mtype,more,d1,d2) = struct.unpack("BBBB",message[0:4])
        # mtype = int(message[0])
        # print("Got Message header type", mtype)
        #hWin, print("<--")
        if mtype == 0:
            #hWin, print("JSON:")
            self.serialized = json.loads(message[4:])
            # print(serialized['command'])
            #hWin, print(message[4:])
        elif mtype == 1:
            print("Binary, more:", more)
            print(message[4:])
        else:
            print("Unhandled message type",mtype)

    def send(self,buffer):
        if self.socket:
            # print("Hexsending %s" % (binascii.hexlify(buffer)))
            self.socket.send(buffer)

    def framejson(self,message):
        l = struct.pack(">i",len(message) + 8)
        h = struct.pack("BBBB",0,0,0,0)
        print("--> Framing: %s" % (message))
        return l + h + message.encode('utf-8')

    def sendcommand(self,message):
        self.send(self.framejson(message))

    def sendmultiplecommands(self,messages):
        b = ''
        for m in messages:
            b += self.framejson(m)
        self.send(b)

    def sendjson(self,object):
        s = json.dumps(object)
        self.sendcommand(s)



h1 = H1()




class Connect(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.host = tk.StringVar(self,value='172.18.4.87')
        self.port = tk.StringVar(self,value='9999')
        self.statuspoll = tk.BooleanVar(self,value=False)

        self.hlabel = tk.Label(self, text="Host")
        self.hlabel.pack(side=tk.LEFT)
        self.hentry = tk.Entry(self,textvariable=self.host)
        self.hentry.pack(side=tk.LEFT)
        self.plabel = tk.Label(self, text="Port")
        self.plabel.pack(side=tk.LEFT)
        self.pentry = tk.Entry(self,textvariable=self.port,width=5)
        self.pentry.pack(side=tk.LEFT)
        self.connect_button = tk.Button(self, text="Connect", command=self.connect)
        self.connect_button.pack(side=tk.LEFT)
        self.disconnect_button = tk.Button(self, text="Disconnect", command=self.disconnect)
        self.disconnect_button.pack(side=tk.LEFT)

        self.spentry = tk.Entry(self,textvariable=self.statuspoll,width=5)
        self.spentry.pack(side=tk.LEFT)

        self.sp_button = tk.Button(self, text="Set Poll Status", command=self.pollstatus)
        self.sp_button.pack(side=tk.LEFT)

    def connect(self):
        h1.connect(self.host.get(),self.port.get())

    def disconnect(self):
        h1.disconnect()

    def pollstatus(self):
        h1.setpollstatus(self.statuspoll.get())


class Send(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.message = tk.StringVar(self,value='{"command":"ping"}')
        self.mlabel = tk.Label(self,text="Message")
        self.mlabel.pack(side=tk.LEFT)
        self.mentry = tk.Entry(self,textvariable=self.message)
        self.mentry.pack(expand=True,fill='x',side=tk.LEFT)
        self.send_button = tk.Button(self, text="Send", command=self.send)
        self.send_button.pack(side=tk.LEFT)

        # protocol debug
        # self.dsend_button = tk.Button(self, text="Double Send", command=self.senddouble)
        # self.dsend_button.pack(side=tk.LEFT)

    def send(self):
        h1.sendcommand(self.message.get())

    def senddouble(self):
        h1.sendmultiplecommands([self.message.get(),self.message.get()])

class ConnectionManager(tk.LabelFrame):
    def __init__(self, parent):
        tk.LabelFrame.__init__(self, parent, text="Connection Manger")

        self.start_transfer_button = tk.Button(self, text="Start Transfer", command=self.start_transfer)
        self.start_transfer_button.pack(side=tk.LEFT)

        self.stop_transfer_button = tk.Button(self, text="Stop Transfer", command=self.stop_transfer)
        self.stop_transfer_button.pack(side=tk.LEFT)

        self.start_import_button = tk.Button(self, text="Start Import", command=self.start_import)
        self.start_import_button.pack(side=tk.LEFT)

        self.stop_import_button = tk.Button(self, text="Stop Import", command=self.stop_import)
        self.stop_import_button.pack(side=tk.LEFT)

        self.remake_connection_button = tk.Button(self, text="Remake Connection", command=self.remake_connection)
        self.remake_connection_button.pack(side=tk.LEFT)

    def start_transfer(self):
        h1.sendjson({"command":"cm_starttransfer"})

    def stop_transfer(self):
        h1.sendjson({"command":"cm_stoptransfer"})

    def start_import(self):
        h1.sendjson({"command":"cm_startx1import"})

    def stop_import(self):
        h1.sendjson({"command":"cm_stopx1import"})

    def remake_connection(self):
        h1.sendjson({"command":"cm_remakeconnection"})

class Memory(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.size = tk.IntVar(self,value=538)

        self.mlabel = tk.Label(self,text="Memory Pool Size, MB")
        self.mlabel.pack(side=tk.LEFT)

        self.mentry = tk.Entry(self,textvariable=self.size,width=4)
        self.mentry.pack(side=tk.LEFT)

        self.init_button = tk.Button(self, text="Init", command=self.init)
        self.init_button.pack(side=tk.LEFT)

    def init(self):
        h1.sendjson({"command":"pm_initpool","size":self.size.get()})

class InitCamera(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.camera = tk.IntVar(self,value=0)
        self.width = tk.IntVar(self,value=1920)
        self.height = tk.IntVar(self,value=1080)
        self.fps = tk.IntVar(self,value=30)
        self.gop = tk.IntVar(self,value=30)
        self.controlrate = tk.IntVar(self,value=2)
        self.bitrate = tk.IntVar(self,value=6000000)
        self.quality = tk.IntVar(self,value=1)
        self.buffersize = tk.IntVar(self,value=90)
        self.audioid = tk.IntVar(self,value=0)

        self.camera_label = tk.Label(self,text="camera")
        self.camera_label.pack(side=tk.LEFT)
        self.camera_entry = tk.Entry(self,textvariable=self.camera,width=2)
        self.camera_entry.pack(side=tk.LEFT)

        self.audioid_label = tk.Label(self,text="audioid")
        self.audioid_label.pack(side=tk.LEFT)
        self.audioid_entry = tk.Entry(self,textvariable=self.audioid,width=2)
        self.audioid_entry.pack(side=tk.LEFT)

        self.width_label = tk.Label(self,text="width")
        self.width_label.pack(side=tk.LEFT)
        self.width_entry = tk.Entry(self,textvariable=self.width,width=4)
        self.width_entry.pack(side=tk.LEFT)

        self.height_label = tk.Label(self,text="height")
        self.height_label.pack(side=tk.LEFT)
        self.height_entry = tk.Entry(self,textvariable=self.height,width=4)
        self.height_entry.pack(side=tk.LEFT)

        self.fps_label = tk.Label(self,text="fps")
        self.fps_label.pack(side=tk.LEFT)
        self.fps_entry = tk.Entry(self,textvariable=self.fps,width=2)
        self.fps_entry.pack(side=tk.LEFT)

        self.gop_label = tk.Label(self,text="gop")
        self.gop_label.pack(side=tk.LEFT)
        self.gop_entry = tk.Entry(self,textvariable=self.gop,width=2)
        self.gop_entry.pack(side=tk.LEFT)

        self.controlrate_label = tk.Label(self,text="controlrate,none,VBR,CBR")
        self.controlrate_label.pack(side=tk.LEFT)
        self.controlrate_entry = tk.Entry(self,textvariable=self.controlrate,width=2)
        self.controlrate_entry.pack(side=tk.LEFT)

        self.bitrate_label = tk.Label(self,text="bitrate")
        self.bitrate_label.pack(side=tk.LEFT)
        self.bitrate_entry = tk.Entry(self,textvariable=self.bitrate,width=8)
        self.bitrate_entry.pack(side=tk.LEFT)

        self.quality_label = tk.Label(self,text="quality")
        self.quality_label.pack(side=tk.LEFT)
        self.quality_entry = tk.Entry(self,textvariable=self.quality,width=2)
        self.quality_entry.pack(side=tk.LEFT)

        self.buffersize_label = tk.Label(self,text="buffersize,secs")
        self.buffersize_label.pack(side=tk.LEFT)
        self.buffersize_entry = tk.Entry(self,textvariable=self.buffersize,width=3)
        self.buffersize_entry.pack(side=tk.LEFT)

        self.init_button = tk.Button(self,text="Init", command=self.init)
        self.init_button.pack(side=tk.LEFT)

    def init(self):
        h1.sendjson({
            "command":"pm_recordinitcam",
            "camera":self.camera.get(),
            "width":self.width.get(),
            "height":self.height.get(),
            "fps":self.fps.get(),
            "gop":self.gop.get(),
            "controlrate":self.controlrate.get(),
            "bitrate":self.bitrate.get(),
            "quality":self.quality.get(),
            "buffersize":self.buffersize.get(),
            "audioid":self.audioid.get()
        })

class Init(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.init_button = tk.Button(self, text="Initialize", command=self.init)
        self.init_button.pack(side=tk.LEFT)

    def init(self):
        h1.sendjson({"command":"init"})

class Login(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.officer = tk.StringVar(self,value='gsnext1')
        self.password = tk.StringVar(self,value='123')
        self.partner = tk.StringVar(self,value='gsof1')
        self.unit = tk.StringVar(self,value='gsnext1')

        self.olabel = tk.Label(self,text="Officer")
        self.olabel.pack(side=tk.LEFT)
        self.oentry = tk.Entry(self,textvariable=self.officer)
        self.oentry.pack(side=tk.LEFT)
        self.passlabel = tk.Label(self,text="Password")
        self.passlabel.pack(side=tk.LEFT)
        self.passentry = tk.Entry(self,textvariable=self.password)
        self.passentry.pack(side=tk.LEFT)
        self.partnerlabel = tk.Label(self,text="Partner")
        self.partnerlabel.pack(side=tk.LEFT)
        self.partnerentry = tk.Entry(self,textvariable=self.partner)
        self.partnerentry.pack(side=tk.LEFT)
        self.unitlabel = tk.Label(self,text="Unit")
        self.unitlabel.pack(side=tk.LEFT)
        self.unitentry = tk.Entry(self,textvariable=self.unit)
        self.unitentry.pack(side=tk.LEFT)

        self.login_button = tk.Button(self, text="Login", command=self.login)
        self.login_button.pack(side=tk.LEFT)

        self.logout_button = tk.Button(self, text="Logout", command=self.logout)
        self.logout_button.pack(side=tk.LEFT)

    def login(self):
        h1.sendjson({"command":"login","officer":self.officer.get(),"password":self.password.get(),"partner":self.partner.get(),"unit":self.unit.get()})

    def logout(self):
        h1.sendjson({"command":"logout"})


class GetEvent(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.eventname = tk.StringVar(self,value="3539@20180325120422-0")

        self.eventname_label = tk.Label(self,text="Event Name")
        self.eventname_label.pack(side=tk.LEFT)

        self.eventname_entry = tk.Entry(self,textvariable=self.eventname,width=30)
        self.eventname_entry.pack(side=tk.LEFT)

        self.get_button = tk.Button(self, text="Get", command=self.get)
        self.get_button.pack(side=tk.LEFT)

    def get(self):
        h1.sendjson({"command":"getevent","eventname":self.eventname.get()})

class ModifyEvent(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.eventname = tk.StringVar(self,value="3539@20180325120422-0")

        self.eventid = tk.StringVar(self,value="0")	# int?
        self.caseno = tk.StringVar(self,value="case1")
        self.caseno2 = tk.StringVar(self,value="case2")
        self.dispatch = tk.StringVar(self,value="dispatch1")
        self.dispatch2 = tk.StringVar(self,value="dispatch2")
        self.ticket = tk.StringVar(self,value="ticket1")
        self.ticket2 = tk.StringVar(self,value="ticket2")
        self.firstname = tk.StringVar(self,value="FirstName")
        self.lastname = tk.StringVar(self,value="LastName")
        self.location = tk.StringVar(self,value="Location Here")
        self.remarks= tk.StringVar(self,value="Remarks Here, but how long can they be? It is not known why the wombat eats fish just on Sundays.")

        self.eventname_label = tk.Label(self,text="Event Name")
        self.eventname_label.pack(side=tk.LEFT)

        self.eventname_entry = tk.Entry(self,textvariable=self.eventname,width=30)
        self.eventname_entry.pack(side=tk.LEFT)

        self.eventid_label = tk.Label(self,text="EventId")
        self.eventid_label.pack(side=tk.LEFT)

        self.eventid_entry = tk.Entry(self,textvariable=self.eventid,width=10)
        self.eventid_entry.pack(side=tk.LEFT)

        self.case1_label = tk.Label(self,text="Case1")
        self.case1_label.pack(side=tk.LEFT)

        self.case1_entry = tk.Entry(self,textvariable=self.caseno,width=10)
        self.case1_entry.pack(side=tk.LEFT)

        self.case2_label = tk.Label(self,text="Case2")
        self.case2_label.pack(side=tk.LEFT)

        self.case2_entry = tk.Entry(self,textvariable=self.caseno2,width=10)
        self.case2_entry.pack(side=tk.LEFT)

        self.addevent_button = tk.Button(self, text="Modify", command=self.modifyevent)
        self.addevent_button.pack(side=tk.LEFT)

    def modifyevent(self):
        h1.sendjson({"command":"modifyevent","eventname":self.eventname.get(),
                     "event":{
                         "event_id":self.eventid.get(),
                         "case":self.caseno.get(),
                         "case2":self.caseno2.get(),
                         "dispatch":self.dispatch.get(),
                         "dispatch2":self.dispatch2.get(),
                         "ticket":self.dispatch.get(),
                         "ticket2":self.dispatch.get(),
                         "first_name":self.firstname.get(),
                         "last_name":self.lastname.get(),
                         "location":self.location.get(),
                         "notes":self.remarks.get()
                     }})

class Status(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.get_button = tk.Button(self, text="Get Status", command=self.get)
        self.get_button.pack(side=tk.LEFT)

        self.getpaths_button = tk.Button(self, text="Get Paths", command=self.getpaths)
        self.getpaths_button.pack(side=tk.LEFT)

        self.getevents_button = tk.Button(self, text="Get Events", command=self.getevents)
        self.getevents_button.pack(side=tk.LEFT)

        self.getpendingevents_button = tk.Button(self, text="Get Pending Events", command=self.getpendingevents)
        self.getpendingevents_button.pack(side=tk.LEFT)

        self.version_button = tk.Button(self, text="Get Version", command=self.getversion)
        self.version_button.pack(side=tk.LEFT)

        self.gps_button = tk.Button(self, text="Get GPS", command=self.getgps)
        self.gps_button.pack(side=tk.LEFT)

        self.network_button = tk.Button(self, text="Get Network", command=self.getnetwork)
        self.network_button.pack(side=tk.LEFT)

    def get(self):
        h1.sendjson({"command":"status"})

    def getpaths(self):
        h1.sendjson({"command":"paths"})

    def getevents(self):
        h1.sendjson({"command":"eventlist"})

    def getpendingevents(self):
        h1.sendjson({"command":"pendingeventlist"})
        pass

    def getversion(self):
        h1.sendjson({"command":"version"})

    def getgps(self):
        h1.sendjson({"command":"gps"})

    def getnetwork(self):
        h1.sendjson({"command":"network"})

class Upload(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.icvstart_button = tk.Button(self, text="ICV Start", command=self.icvstart)
        self.icvstart_button.pack(side=tk.LEFT)

        self.icvstop_button = tk.Button(self, text="ICV Stop", command=self.icvstop)
        self.icvstop_button.pack(side=tk.LEFT)

        self.bwcstart_button = tk.Button(self, text="BWC Start", command=self.bwcstart)
        self.bwcstart_button.pack(side=tk.LEFT)

        self.bwcstop_button = tk.Button(self, text="BWC Stop", command=self.bwcstop)
        self.bwcstop_button.pack(side=tk.LEFT)

    def icvstart(self):
        h1.sendjson({"command":"upload","icv":True})

    def icvstop(self):
        h1.sendjson({"command":"upload","icv":False})

    def bwcstart(self):
        h1.sendjson({"command":"upload","bwc":True})

    def bwcstop(self):
        h1.sendjson({"command":"upload","bwc":False})

class Mic(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.mic = tk.StringVar(self,value="cam1")
        self.mute = tk.BooleanVar(self,value=False)

        self.mlabel = tk.Label(self,text="Microphone")
        self.mlabel.pack(side=tk.LEFT)

        self.mentry = tk.Entry(self,textvariable=self.mic,width=6)
        self.mentry.pack(side=tk.LEFT)

        self.vlabel = tk.Label(self,text="Mute")
        self.vlabel.pack(side=tk.LEFT)

        self.ventry = tk.Entry(self,textvariable=self.mute,width=2)
        self.ventry.pack(side=tk.LEFT)

        self.set_button = tk.Button(self, text="Set", command=self.set)
        self.set_button.pack(side=tk.LEFT)

        self.get_button = tk.Button(self, text="Get", command=self.get)
        self.get_button.pack(side=tk.LEFT)

        self.getall_button = tk.Button(self, text="Get All", command=self.getall)
        self.getall_button.pack(side=tk.LEFT)

    def set(self):
        h1.sendjson({"command":"setmic","mic":self.mic.get(),"mute":self.mute.get()})

    def get(self):
        h1.sendjson({"command":"getmic","mic":self.mic.get()})

    def getall(self):
        h1.sendjson({"command":"getmic"})

class Volume(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.device = tk.StringVar(self,value="speaker")
        self.percent = tk.IntVar(self,value=50)

        self.dlabel = tk.Label(self,text="Device")
        self.dlabel.pack(side=tk.LEFT)

        self.dentry = tk.Entry(self,textvariable=self.device,width=10)
        self.dentry.pack(side=tk.LEFT)

        self.vlabel = tk.Label(self,text="Volume")
        self.vlabel.pack(side=tk.LEFT)

        self.ventry = tk.Entry(self,textvariable=self.percent,width=4)
        self.ventry.pack(side=tk.LEFT)

        self.set_button = tk.Button(self, text="Set", command=self.set)
        self.set_button.pack(side=tk.LEFT)

        self.get_button = tk.Button(self, text="Get", command=self.get)
        self.get_button.pack(side=tk.LEFT)

    def set(self):
        h1.sendjson({"command":"volume","device":self.device.get(),"percent":self.percent.get()})

    def get(self):
        h1.sendjson({"command":"volume"})

class Camera(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.camera = tk.IntVar(self,value=0)

        self.clabel = tk.Label(self,text="Camera")
        self.clabel.pack(side=tk.LEFT)

        self.centry = tk.Entry(self,textvariable=self.camera)
        self.centry.pack(side=tk.LEFT)

        self.rec_button = tk.Button(self, text="Record", command=self.record)
        self.rec_button.pack(side=tk.LEFT)

        self.stop_button = tk.Button(self, text="Stop", command=self.stop)
        self.stop_button.pack(side=tk.LEFT)

        self.snap_button = tk.Button(self, text="Snapshot", command=self.snapshot)
        self.snap_button.pack(side=tk.LEFT)

        self.book_button = tk.Button(self, text="Bookmark", command=self.bookmark)
        self.book_button.pack(side=tk.LEFT)

    def record(self):
        h1.sendjson({"command":"record","camera":self.camera.get()})

    def stop(self):
        h1.sendjson({"command":"stoprecord","camera":self.camera.get()})

    def snapshot(self):
        h1.sendjson({"command":"snapshot","camera":self.camera.get()})

    def bookmark(self):
        h1.sendjson({"command":"bookmark","camera":self.camera.get()})

class Server(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.name = tk.Label(self,text="Server")
        self.name.pack(side=tk.LEFT)

        self.start_button = tk.Button(self, text="Start", command=self.start)
        self.start_button.pack(side=tk.LEFT)

        self.stop_button = tk.Button(self, text="Stop", command=self.stop)
        self.stop_button.pack(side=tk.LEFT)

    def start(self):
        h1.sendjson({"command":"pm_serverstart"})

    def stop(self):
        h1.sendjson({"command":"pm_serverstop"})

class LiveView(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.camera = tk.IntVar(self,value=0)

        self.x = tk.IntVar(self,value=0)
        self.y = tk.IntVar(self,value=0)
        self.cx = tk.IntVar(self,value=390)
        self.cy = tk.IntVar(self,value=220)
        self.display = tk.IntVar(self,value=0)

        self.name = tk.Label(self,text="Live View")
        self.name.pack(side=tk.LEFT)

        self.xlabel = tk.Label(self,text="X")
        self.xlabel.pack(side=tk.LEFT)
        self.xentry = tk.Entry(self,textvariable=self.x,width=5)
        self.xentry.pack(side=tk.LEFT)

        self.ylabel = tk.Label(self,text="Y")
        self.ylabel.pack(side=tk.LEFT)
        self.yentry = tk.Entry(self,textvariable=self.y,width=5)
        self.yentry.pack(side=tk.LEFT)

        self.cxlabel = tk.Label(self,text="CX")
        self.cxlabel.pack(side=tk.LEFT)
        self.cxentry = tk.Entry(self,textvariable=self.cx,width=5)
        self.cxentry.pack(side=tk.LEFT)

        self.cylabel = tk.Label(self,text="CY")
        self.cxlabel.pack(side=tk.LEFT)
        self.cyentry = tk.Entry(self,textvariable=self.cy,width=5)
        self.cyentry.pack(side=tk.LEFT)

        self.clabel = tk.Label(self,text="Camera")
        self.clabel.pack(side=tk.LEFT)

        self.centry = tk.Entry(self,textvariable=self.camera,width=3)
        self.centry.pack(side=tk.LEFT)

        self.dlabel = tk.Label(self,text="Display")
        self.dlabel.pack(side=tk.LEFT)

        self.dentry = tk.Entry(self,textvariable=self.display,width=3)
        self.dentry.pack(side=tk.LEFT)

        self.on_button = tk.Button(self, text="On", command=self.on)
        self.on_button.pack(side=tk.LEFT)

        self.off_button = tk.Button(self, text="Off", command=self.off)
        self.off_button.pack(side=tk.LEFT)

    def on(self):
        h1.sendjson({
            "command":"pm_liveviewstart",
            "camera":self.camera.get(),
            "x":self.x.get(),
            "y":self.y.get(),
            "cx":self.cx.get(),
            "cy":self.cy.get(),
            "display":self.display.get()
        })

    def off(self):
        h1.sendjson({"command":"pm_liveviewstop","camera":self.camera.get()})

class Live(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.camera = tk.IntVar(self,value=0)

        self.name = tk.Label(self,text="Live Stream")
        self.name.pack(side=tk.LEFT)

        self.clabel = tk.Label(self,text="Camera")
        self.clabel.pack(side=tk.LEFT)

        self.centry = tk.Entry(self,textvariable=self.camera)
        self.centry.pack(side=tk.LEFT)

        self.on_button = tk.Button(self, text="On", command=self.on)
        self.on_button.pack(side=tk.LEFT)

        self.off_button = tk.Button(self, text="Off", command=self.off)
        self.off_button.pack(side=tk.LEFT)

        self.view_button = tk.Button(self, text="View", command=self.view)
        self.view_button.pack(side=tk.LEFT)

    def on(self):
        h1.sendjson({"command":"pm_livestream","camera":self.camera.get(),"on":True})

    def off(self):
        h1.sendjson({"command":"pm_livestream","camera":self.camera.get(),"on":False})

    def view(self):
        spawnprocess(["vlc","http://" + h1.host + ":8080/live.ts"])

class PMRecordMP4(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.camera = tk.IntVar(self,value=0)
        self.file = tk.StringVar(self,value="foo.mp4")
        self.pretime = tk.IntVar(self,value=10)

        self.name = tk.Label(self,text="Record MP4")
        self.name.pack(side=tk.LEFT)

        self.clabel = tk.Label(self,text="Camera")
        self.clabel.pack(side=tk.LEFT)

        self.camera_entry = tk.Entry(self,textvariable=self.camera,width=2)
        self.camera_entry.pack(side=tk.LEFT)

        self.plabel = tk.Label(self,text="Pretime")
        self.plabel.pack(side=tk.LEFT)

        self.pretime_entry = tk.Entry(self,textvariable=self.pretime,width=4)
        self.pretime_entry.pack(side=tk.LEFT)

        self.flabel = tk.Label(self,text="File")
        self.flabel.pack(side=tk.LEFT)

        self.fentry = tk.Entry(self,textvariable=self.file)
        self.fentry.pack(expand=True,fill='x',side=tk.LEFT)

        self.start_button = tk.Button(self, text="Start", command=self.startrecord)
        self.start_button.pack(side=tk.LEFT)

        self.stop_button = tk.Button(self, text="Stop", command=self.stoprecord)
        self.stop_button.pack(side=tk.LEFT)

    def startrecord(self):
        h1.sendjson({"command":"pm_startrecordmp4","camera":self.camera.get(),"filename":self.file.get(),"pretime":self.pretime.get()})

    def stoprecord(self):
        h1.sendjson({"command":"pm_stoprecordmp4","camera":self.camera.get(),"filename":self.file.get()})

class PMRecordTS(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.camera = tk.IntVar(self,value=0)
        self.file = tk.StringVar(self,value="foo.ts")
        self.pretime = tk.IntVar(self,value=10)

        self.name = tk.Label(self,text="Record TS")
        self.name.pack(side=tk.LEFT)

        self.clabel = tk.Label(self,text="Camera")
        self.clabel.pack(side=tk.LEFT)

        self.camera_entry = tk.Entry(self,textvariable=self.camera,width=2)
        self.camera_entry.pack(side=tk.LEFT)

        self.plabel = tk.Label(self,text="Pretime")
        self.plabel.pack(side=tk.LEFT)

        self.pretime_entry = tk.Entry(self,textvariable=self.pretime,width=4)
        self.pretime_entry.pack(side=tk.LEFT)

        self.flabel = tk.Label(self,text="File")
        self.flabel.pack(side=tk.LEFT)

        self.fentry = tk.Entry(self,textvariable=self.file)
        self.fentry.pack(expand=True,fill='x',side=tk.LEFT)

        self.start_button = tk.Button(self, text="Start", command=self.startrecord)
        self.start_button.pack(side=tk.LEFT)

        self.stop_button = tk.Button(self, text="Stop", command=self.stoprecord)
        self.stop_button.pack(side=tk.LEFT)

    def startrecord(self):
        h1.sendjson({"command":"pm_startrecordts","camera":self.camera.get(),"filename":self.file.get(),"pretime":self.pretime.get()})

    def stoprecord(self):
        h1.sendjson({"command":"pm_stoprecordts","camera":self.camera.get(),"filename":self.file.get()})

class SyncNextMP4(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.camera = tk.IntVar(self,value=0)
        self.file = tk.StringVar(self,value="foo-2.mp4")

        self.name = tk.Label(self,text="Sync to next MP4")
        self.name.pack(side=tk.LEFT)

        self.clabel = tk.Label(self,text="Camera")
        self.clabel.pack(side=tk.LEFT)

        self.camera_entry = tk.Entry(self,textvariable=self.camera,width=2)
        self.camera_entry.pack(side=tk.LEFT)

        self.flabel = tk.Label(self,text="File")
        self.flabel.pack(side=tk.LEFT)

        self.fentry = tk.Entry(self,textvariable=self.file)
        self.fentry.pack(expand=True,fill='x',side=tk.LEFT)

        self.sync_button = tk.Button(self, text="Sync", command=self.sync)
        self.sync_button.pack(side=tk.LEFT)

    def sync(self):
        h1.sendjson({"command":"pm_recsyncnextmp4","camera":self.camera.get(),"filename":self.file.get()})

class SyncNextTS(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.camera = tk.IntVar(self,value=0)
        self.file = tk.StringVar(self,value="foo-2.ts")

        self.name = tk.Label(self,text="Sync to next TS")
        self.name.pack(side=tk.LEFT)

        self.clabel = tk.Label(self,text="Camera")
        self.clabel.pack(side=tk.LEFT)

        self.camera_entry = tk.Entry(self,textvariable=self.camera,width=2)
        self.camera_entry.pack(side=tk.LEFT)

        self.flabel = tk.Label(self,text="File")
        self.flabel.pack(side=tk.LEFT)

        self.fentry = tk.Entry(self,textvariable=self.file)
        self.fentry.pack(expand=True,fill='x',side=tk.LEFT)

        self.sync_button = tk.Button(self, text="Sync", command=self.sync)
        self.sync_button.pack(side=tk.LEFT)

    def sync(self):
        h1.sendjson({"command":"pm_recsyncnextts","camera":self.camera.get(),"filename":self.file.get()})

class PMFileStream(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.file = tk.StringVar('')

        self.name = tk.Label(self,text="File Stream")
        self.name.pack(side=tk.LEFT)

        self.fentry = tk.Entry(self,textvariable=self.file)
        self.fentry.pack(expand=True,fill='x',side=tk.LEFT)

        self.start_button = tk.Button(self, text="Start", command=self.start)
        self.start_button.pack(side=tk.LEFT)

        self.stop_button = tk.Button(self, text="Stop", command=self.stop)
        self.stop_button.pack(side=tk.LEFT)

        self.view_button = tk.Button(self, text="View", command=self.view)
        self.view_button.pack(side=tk.LEFT)

    def start(self):
        h1.sendjson({"command":"pm_streamstartfile","filename":self.file.get()})

    def stop(self):
        h1.sendjson({"command":"pm_streamstopfile"})

    def view(self):
        spawnprocess(["vlc","http://" + h1.host + ":8080/1.mp4"])

class OSD(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.x = tk.IntVar(self,value=0)
        self.y = tk.IntVar(self,value=0)
        self.camera = tk.IntVar(self,value=0)
        self.block = tk.IntVar(self,value=0)
        self.stats = tk.IntVar(self,value=0)
        self.content = tk.StringVar(self,value='I hate cheeze')

        self.label = tk.Label(self,text="OSD")
        self.label.pack(side=tk.LEFT)

        self.xy_label = tk.Label(self,text="X,Y")
        self.xy_label.pack(side=tk.LEFT)

        self.x_entry = tk.Entry(self,textvariable=self.x,width=5)
        self.x_entry.pack(side=tk.LEFT)

        self.y_entry = tk.Entry(self,textvariable=self.y,width=5)
        self.y_entry.pack(side=tk.LEFT)

        self.camera_label = tk.Label(self,text="Camera")
        self.camera_label.pack(side=tk.LEFT)

        self.camera_entry = tk.Entry(self,textvariable=self.camera,width=2)
        self.camera_entry.pack(side=tk.LEFT)

        self.block_label = tk.Label(self,text="Block")
        self.block_label.pack(side=tk.LEFT)

        self.block_entry = tk.Entry(self,textvariable=self.block,width=6)
        self.block_entry.pack(side=tk.LEFT)

        self.stats_label = tk.Label(self,text="Stats")
        self.stats_label.pack(side=tk.LEFT)

        self.stats_entry = tk.Entry(self,textvariable=self.stats,width=6)
        self.stats_entry.pack(side=tk.LEFT)

        self.content_entry = tk.Entry(self,textvariable=self.content)
        self.content_entry.pack(side=tk.LEFT)

        self.setcontent_button = tk.Button(self, text="Set Content", command=self.setcontent)
        self.setcontent_button.pack(side=tk.LEFT)

        self.setstats_button = tk.Button(self, text="Set Stats", command=self.setstats)
        self.setstats_button.pack(side=tk.LEFT)

    def setcontent(self):
        h1.sendjson({"command":"pm_setosdcontent","x":self.x.get(),"y":self.y.get(),"camera":self.camera.get(),"block":self.block.get(),"content":self.content.get()})

    def setstats(self):
        h1.sendjson({"command":"pm_setosdstats","stats":self.stats.get()})


class FileStream(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.file = tk.StringVar('')

        self.name = tk.Label(self,text="File Stream")
        self.name.pack(side=tk.LEFT)

        self.fentry = tk.Entry(self,textvariable=self.file)
        self.fentry.pack(expand=True,fill='x',side=tk.LEFT)

        self.start_button = tk.Button(self, text="Start", command=self.start)
        self.start_button.pack(side=tk.LEFT)

        self.view_button = tk.Button(self, text="View", command=self.view)
        self.view_button.pack(side=tk.LEFT)

    def start(self):
        h1.sendjson({"command":"streamfile","filename":self.file.get()})

    def view(self):
        spawnprocess(["vlc","http://" + h1.host + ":8080/1.mp4"])

class PMSnapshot(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.camera = tk.IntVar(self,value=0)
        self.filename = tk.StringVar(self,value='foo.jpg')

        self.clabel = tk.Label(self,text="Camera")
        self.clabel.pack(side=tk.LEFT)

        self.centry = tk.Entry(self,textvariable=self.camera)
        self.centry.pack(side=tk.LEFT)

        self.flabel = tk.Label(self,text="File")
        self.flabel.pack(side=tk.LEFT)

        self.fentry = tk.Entry(self,textvariable=self.filename)
        self.fentry.pack(expand=True,fill='x',side=tk.LEFT)

        self.snap_button = tk.Button(self, text="Snapshot", command=self.snapshot)
        self.snap_button.pack(side=tk.LEFT)


    def snapshot(self):
        h1.sendjson({"command":"pm_snapshot","camera":self.camera.get(),"filename":self.filename.get()})


class FileInfo(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.filename = tk.StringVar(self,value='xxx.mp4')

        self.flabel = tk.Label(self,text="File")
        self.flabel.pack(side=tk.LEFT)

        self.fentry = tk.Entry(self,textvariable=self.filename)
        self.fentry.pack(expand=True,fill='x',side=tk.LEFT)

        self.get_button = tk.Button(self, text="Get Info", command=self.getinfo)
        self.get_button.pack(side=tk.LEFT)

    def getinfo(self):
        h1.sendjson({"command":"pm_fileinfo","filename":self.filename.get()})

class GetFile(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.filename = tk.StringVar(self,value='units.config')

        self.flabel = tk.Label(self,text="File")
        self.flabel.pack(side=tk.LEFT)

        self.fentry = tk.Entry(self,textvariable=self.filename)
        self.fentry.pack(expand=True,fill='x',side=tk.LEFT)

        self.get_button = tk.Button(self, text="Get", command=self.get)
        self.get_button.pack(side=tk.LEFT)

    def get(self):
        h1.sendjson({"command":"readfile","filename":self.filename.get()})

class GetDir(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.dirname = tk.StringVar(self,value='/media/ubuntu/USB/cobanvideos')
        self.filter = tk.StringVar(self,value='*.mp4')
        self.sort = tk.StringVar(self,value='time')

        self.dlabel = tk.Label(self,text="Directory")
        self.dlabel.pack(side=tk.LEFT)

        self.dentry = tk.Entry(self,textvariable=self.dirname)
        self.dentry.pack(expand=True,fill='x',side=tk.LEFT)

        self.flabel = tk.Label(self,text="Filter")
        self.flabel.pack(side=tk.LEFT)

        self.fentry = tk.Entry(self,textvariable=self.filter)
        self.fentry.pack(side=tk.LEFT)

        self.slabel = tk.Label(self,text="Sort")
        self.slabel.pack(side=tk.LEFT)

        self.sentry = tk.Entry(self,textvariable=self.sort)
        self.sentry.pack(side=tk.LEFT)

        self.get_button = tk.Button(self, text="Get", command=self.get)
        self.get_button.pack(side=tk.LEFT)

    def get(self):
        h1.sendjson({"command":"ls","path":self.dirname.get(),"filters":[self.filter.get()],"sort":self.sort.get()})

class Shutdown(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.shutdown_button = tk.Button(self, text="Shutdown", command=self.shutdown)
        self.shutdown_button.pack(side=tk.LEFT)

    def shutdown(self):
        h1.sendjson({"command":"shutdown"})

class WMIC(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.label = tk.Label(self, text="WMIC")
        self.label.pack(side=tk.LEFT)

        self.enable_button = tk.Button(self, text="Enable", command=self.enable)
        self.enable_button.pack(side=tk.LEFT)

        self.disable_button = tk.Button(self, text="Disable", command=self.disable)
        self.disable_button.pack(side=tk.LEFT)

        self.coverton_button = tk.Button(self, text="Covert On", command=self.coverton)
        self.coverton_button.pack(side=tk.LEFT)

        self.covertoff_button = tk.Button(self, text="Covert Off", command=self.covertoff)
        self.covertoff_button.pack(side=tk.LEFT)

        self.on_button = tk.Button(self, text="On", command=self.on)
        self.on_button.pack(side=tk.LEFT)

        self.off_button = tk.Button(self, text="Off", command=self.off)
        self.off_button.pack(side=tk.LEFT)

    def enable(self):
        h1.sendjson({"command":"mm_wmicenable"})

    def disable(self):
        h1.sendjson({"command":"mm_wmicdisable"})

    def coverton(self):
        h1.sendjson({"command":"mm_wmiccoverton"})

    def covertoff(self):
        h1.sendjson({"command":"mm_wmiccovertoff"})

    def on(self):
        h1.sendjson({"command":"mm_wmicon"})

    def off(self):
        h1.sendjson({"command":"mm_wmicoff"})

class Speaker(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.label = tk.Label(self, text="Speaker Mute")
        self.label.pack(side=tk.LEFT)

        self.on_button = tk.Button(self, text="On", command=self.on)
        self.on_button.pack(side=tk.LEFT)

        self.off_button = tk.Button(self, text="Off", command=self.off)
        self.off_button.pack(side=tk.LEFT)

    def on(self):
        h1.sendjson({"command":"mm_speakermuteon"})

    def off(self):
        h1.sendjson({"command":"mm_speakermuteoff"})


class MM(tk.LabelFrame):
    def __init__(self, parent):
        tk.LabelFrame.__init__(self, parent,text="Metadata Manager")

        self.wmic = WMIC(self)
        self.wmic.pack()

        self.speaker = Speaker(self)
        self.speaker.pack()

class PM(tk.LabelFrame):
    def __init__(self, parent):
        tk.LabelFrame.__init__(self, parent,text="Playback Manager")

        self.memory = Memory(self)
        self.memory.pack()
        self.initcamera = InitCamera(self)
        self.initcamera.pack()
        self.server = Server(self)
        self.server.pack()
        self.fileinfo = FileInfo(self)
        self.fileinfo.pack(expand=True,fill='x')
        self.bookmark = PMSnapshot(self)
        self.bookmark.pack(expand=True,fill='x')
        self.liveview = LiveView(self)
        self.liveview.pack()
        self.live = Live(self)
        self.live.pack()
        self.recordmp4 = PMRecordMP4(self)
        self.recordmp4.pack(expand=True,fill='x')
        self.recordts = PMRecordTS(self)
        self.recordts.pack(expand=True,fill='x')
        self.syncnextmp4 = SyncNextMP4(self)
        self.syncnextmp4.pack(expand=True,fill='x')
        self.syncnextts = SyncNextTS(self)
        self.syncnextts.pack(expand=True,fill='x')
        self.osd = OSD(self)
        self.osd.pack(expand=True,fill='x')
        self.filestream = PMFileStream(self)
        self.filestream.pack(expand=True,fill='x')

class Trigger(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.code = tk.IntVar(self,value=42)

        self.label = tk.Label(self,text="Trigger Code")
        self.label.pack(side=tk.LEFT)

        self.code_entry = tk.Entry(self,textvariable=self.code)
        self.code_entry.pack(side=tk.LEFT)

        self.send_button = tk.Button(self,text="Send",command=self.send)
        self.send_button.pack(side=tk.LEFT)

    def send(self):
        h1.sendjson({"command":"trigger","code":self.code.get()})

class Debug(tk.LabelFrame):
    def __init__(self, parent):
        tk.LabelFrame.__init__(self, parent,text="Debug")

        self.trigger = Trigger(self)
        self.trigger.pack()

class HL(tk.LabelFrame):
    def __init__(self, parent):
        tk.LabelFrame.__init__(self, parent, text="High Level Interface")

        self.init = Init(self)
        self.init.pack()

        self.login = Login(self)
        self.login.pack()

        self.getevent = GetEvent(self)
        self.getevent.pack()

        self.addevent = ModifyEvent(self)
        self.addevent.pack()

        self.status = Status(self)
        self.status.pack()

        self.mic = Mic(self)
        self.mic.pack()

        self.volume = Volume(self)
        self.volume.pack()

        self.upload = Upload(self)
        self.upload.pack()

        self.camera = Camera(self)
        self.camera.pack()

        self.getfile = GetFile(self)
        self.getfile.pack(expand=True,fill='x')
        self.getdir = GetDir(self)
        self.getdir.pack(expand=True,fill='x')

        self.filestream = FileStream(self)
        self.filestream.pack(expand=True,fill='x')

        self.shutdown = Shutdown(self)
        self.shutdown.pack()

class Main(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.parent.title("H1")
        self.connect = Connect(self)
        self.connect.pack()
        self.send = Send(self)
        self.send.pack(expand=True,fill='x')

        self.cm = ConnectionManager(self)
        self.cm.pack(expand=True,fill='both')

        self.mm = MM(self)
        self.mm.pack(expand=True,fill='both')
        self.pm = PM(self)
        self.pm.pack(expand=True,fill='both')
        self.debug = Debug(self)
        self.debug.pack(expand=True,fill='both')
        self.hl = HL(self)
        self.hl.pack(expand=True,fill='both')

def main():
    m = Main(root)
    m.pack(expand=True,fill='both')
    root.mainloop()

if __name__ == "__main__":
    main()
