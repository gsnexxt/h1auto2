import pyautogui as pg
import time
from pyscreeze import ImageNotFoundException
import pyscreeze
import MDCItems
import AutomationH2 as A2
import AutomationH27 as A1
import json,random, parse
#import opencv-python
'''
def find_text():
    try:
        time.sleep(2)
        #pg.click(pg.locateCenterOnScreen('ExitB.png'))
        if pg.locateCenterOnScreen('Linux',):
            pg.click(pg.locateCenterOnScreen('ExitB.png'))
'''
from tkinter import Tk


def clipboard_get():
    r = Tk()
    r.withdraw()
    return r.clipboard_get()

def use_clipboard(paste_text=None):
    import tkinter # For Python 2, replace with "import Tkinter as tkinter".
    tk = tkinter.Tk()
    tk.withdraw()
    if type(paste_text) == str:
        tk.clipboard_clear()
        tk.clipboard_append(paste_text)
    try:
        clipboard_text = tk.clipboard_get()
    except tkinter.TclError:
        clipboard_text = ''
    r.update() # Stops a few errors (clipboard text unchanged, command line program unresponsive, window not destroyed).
    tk.destroy()
    return clipboard_text

def get_summary():
    try:
        #A1.MDC_login()
        time.sleep(2)
        #pg.click(pg.locateCenterOnScreen('ExitB.png'))
        pg.PAUSE=1.5
        if pg.locateCenterOnScreen('Back.PNG'):
            print("On the correct Menu Screen, Moving to Summary Page")
            pg.click(pg.locateCenterOnScreen('Summary.png', confidence=.9))
            pass
        elif pg.locateCenterOnScreen('Box.PNG', confidence=.7):
            print("Already on the Summary Page")
            pass
        elif pg.locateCenterOnScreen('Next.PNG'):
            print("Not On Summary Screen, Moving to Summary Screen")
            pg.click(pg.locateCenterOnScreen('Next.png', confidence=.9))
        elif pg.locateCenterOnScreen('Menu.PNG'):
            print("Not On Summary Screen, Moving to Summary Screen")
            pg.click(pg.locateCenterOnScreen('Menu.png', confidence=.9))
            pg.click(pg.locateCenterOnScreen('Next.png', confidence=.9))
            pass
        else:
            print("Couldn't find Buttons to navigate")
        pg.PAUSE=1.5
        open('SummaryScreen.txt', 'w').close()
        pg.click(pg.locateCenterOnScreen('Summary.png', confidence=.9))
        heading_list = ['SystemVersion.PNG', 'ErrorStatus.PNG', 'StorageInfo.PNG', 'WifiInformation.PNG', 'EthernetInfo.PNG', 'PowerStatus.PNG', 'GPSInfo.PNG', 'TimeSyncInfo.PNG']
        for heading in heading_list:
            if pg.locateCenterOnScreen(heading, confidence=.9):
                pg.click(pg.locateCenterOnScreen(heading, confidence=.9))
                pg.click(pg.locateCenterOnScreen('Box.PNG', confidence=.7))
                pg.hotkey('ctrl', 'a')
                pg.hotkey('ctrl', 'c')
                pg.click(pg.locateCenterOnScreen('DownArrow.PNG', confidence=.9))
                text1 = clipboard_get()
                SummaryFile = open('SummaryScreen.txt', 'a')
                SummaryFile.writelines(text1)
                SummaryFile.writelines('\n')
                SummaryFile.close()

        if pg.locateCenterOnScreen('SystemVersion.PNG', confidence=.7) and pg.locateCenterOnScreen('ExitA.PNG', confidence=.5):
            print("Finished Summary Check26, Moving to Live View Screen")
            pg.click(pg.locateCenterOnScreen('ExitA.PNG', confidence=.5))
            pg.click(pg.locateCenterOnScreen('Back.PNG', confidence=.5))
            pg.click(pg.locateCenterOnScreen('ExitA.PNG', confidence=.5))
        else:
            print("Something Went Wrong while Moving to Live View, Staying on Summary Page")
        #pg.click(pg.locateCenterOnScreen('OSName.PNG', confidence=.7))

        #if (len(lines) <= 200):
        #    print("Template.config not of expected size and has the following number of lines", len(lines))
        #else:
        #    print("Template not empty, number of lines:: ", len(lines))
        #    pass


    except:
        print("Some Error Occurred while getting to Summary Page")

#get_summary()

def get_summary1():
    try:
        #A1.MDC_login()
        time.sleep(2)
        #pg.click(pg.locateCenterOnScreen('ExitB.png'))
        pg.PAUSE=1.5
        if pg.locateCenterOnScreen('Back.PNG'):
            print("On the correct Menu Screen, Moving to Summary Page")
            pg.click(pg.locateCenterOnScreen('Summary.png', confidence=.9))
            pass
        elif pg.locateCenterOnScreen('Box.PNG', confidence=.7):
            print("Already on the Summary Page")
            pass
        elif pg.locateCenterOnScreen('Next.PNG'):
            print("Not On Summary Screen, Moving to Summary Screen")
            pg.click(pg.locateCenterOnScreen('Next.png', confidence=.9))
        elif pg.locateCenterOnScreen('Menu.PNG'):
            print("Not On Summary Screen, Moving to Summary Screen")
            pg.click(pg.locateCenterOnScreen('Menu.png', confidence=.9))
            pg.click(pg.locateCenterOnScreen('Next.png', confidence=.9))
            pass
        else:
            print("Couldn't find Buttons to navigate")
        pg.PAUSE=1.5
        open('SummaryScreen.txt', 'w').close()
        pg.click(pg.locateCenterOnScreen('Summary.png', confidence=.9))
        heading_list = ['SystemVersion.PNG', 'ErrorStatus.PNG', 'StorageInfo.PNG', 'WifiInformation.PNG', 'EthernetInfo.PNG', 'PowerStatus.PNG', 'GPSInfo.PNG', 'TimeSyncInfo.PNG']
        for heading in heading_list:
            if pg.locateCenterOnScreen(heading, confidence=.9):
                pg.click(pg.locateCenterOnScreen(heading, confidence=.9))
                pg.click(pg.locateCenterOnScreen('Box.PNG', confidence=.7))
                pg.hotkey('ctrl', 'a')
                pg.hotkey('ctrl', 'c')
                pg.click(pg.locateCenterOnScreen('DownArrow.PNG', confidence=.9))
                text1 = clipboard_get()
                SummaryFile = open('SummaryScreen.txt', 'a')
                SummaryFile.writelines(text1)
                SummaryFile.writelines('\n')
                SummaryFile.close()

        if pg.locateCenterOnScreen('SystemVersion.PNG', confidence=.7) and pg.locateCenterOnScreen('ExitA.PNG', confidence=.5):
            print("Finished Summary Check, Moving to Menu Screen")

        else:
            print("Something Went Wrong while Moving to Live View, Staying on Summary Page")
        #pg.click(pg.locateCenterOnScreen('OSName.PNG', confidence=.7))

        #if (len(lines) <= 200):
        #    print("Template.config not of expected size and has the following number of lines", len(lines))
        #else:
        #    print("Template not empty, number of lines:: ", len(lines))
        #    pass


    except:
        print("Some Error Occurred while getting to Summary Page")

#get_summary1()
def check_menu():
    try:
        A1.MDC_login()
        time.sleep(2)
        #pg.click(pg.locateCenterOnScreen('ExitB.png'))
        if pg.locateCenterOnScreen('Back.PNG'):
            pg.click(pg.locateCenterOnScreen('ExitB.png'))
        elif pg.locateCenterOnScreen('Next.PNG'):
            pg.click(pg.locateCenterOnScreen('ExitA.png'))
        time.sleep(1)
        pg.PAUSE = 1
        menu = pg.locateCenterOnScreen('Menu.PNG')
        pg.click(menu)
        #print(type(a))
        #time.sleep(2)
        icv = pg.locateCenterOnScreen('ICV Video.PNG')
        pg.click(icv)
        #time.sleep(1)
        pg.click(pg.locateCenterOnScreen('ExitICV.PNG'))
        #time.sleep(1)
        pg.click(pg.locateCenterOnScreen('Failsafe Video.png'))
        #time.sleep(1)
        e = pg.locateCenterOnScreen('ExitFailsafe.png')
        pg.click(e)
        print(type(menu), type(icv), type(e))
        time.sleep(1)
        exitm = pg.locateCenterOnScreen('ExitA.png')
        pg.click(exitm)
        for element in [menu,icv,exitm, e]:
            if element is None:
                print("An Unexpected Element Type was Found:: ", element, type(element))

    except:
        print("Some Error Occurred")

def check_menu_all():
    try:
        A1.MDC_login()
        time.sleep(2)
        #pg.click(pg.locateCenterOnScreen('ExitB.png'))

        if pg.locateCenterOnScreen('Back.PNG'):
            print("Not on Live View Screen, Moving to Live View Screen")
            pg.click(pg.locateCenterOnScreen('ExitB.png'))
        elif pg.locateCenterOnScreen('Next.PNG'):
            print("Not on Live Screen, Moving to Live View Screen")
            pg.click(pg.locateCenterOnScreen('ExitA.png'))
        elif pg.locateCenterOnScreen('Menu.PNG'):
            print("On Live View Screen, Starting Menus Check")
            pass
        else:
            print("Couldn't find Buttons to navigate")

        pg.PAUSE = 1
        time.sleep(5)
        menu = pg.locateCenterOnScreen('Menu.PNG')
        pg.click(menu)

        icon_list1 = ['ICV Video.PNG', 'Failsafe Video.PNG', 'Snap.PNG', 'BWC Video.PNG', 'In-Car Mic.PNG', 'SwitchOfficer.PNG', 'Upload.PNG']
        #icon_list1 = ['ICV Video.PNG', 'Failsafe Video.PNG', 'Snap.PNG', 'BWC Video.PNG', 'In-Car Mic.PNG', 'SwitchOfficer.PNG', 'Upload.PNG', 'Shut Down.PNG']
        present = []
        absent = []
        for icon in icon_list1:
            if pg.locateCenterOnScreen('MenuPage.PNG', confidence=.7):
                print("On Menu PAge, checking next Icon")
            elif pg.locateCenterOnScreen('Menu.PNG', confidence=.7):
                pg.click(pg.locateCenterOnScreen('Menu.PNG', confidence=.7))
            else:
                pg.click(pg.locateCenterOnScreen('Exit.PNG', confidence=.7))
            if pg.locateCenterOnScreen(icon):
                present.append(icon)
                print("Pressing on Menu Icon:: ", icon)
                time.sleep(3)
                pg.click(pg.locateCenterOnScreen(icon))
                #pg.click(pg.locateCenterOnScreen('Exit.PNG', confidence=.7))
                if icon in ['ICV Video.PNG']:
                    print("Visiting menu item:: ", icon)
                    pg.click(pg.locateCenterOnScreen(icon))
                    print("Starting Video PLayback")
                    pg.click(pg.locateCenterOnScreen('Play.PNG', confidence=.7))
                    t_end = time.time() + 60 * 3
                    while time.time() < t_end:
                        if pg.locateCenterOnScreen('vidcomplete.PNG', confidence=.6):
#or pg.locateCenterOnScreen('vidcomplet.PNG', confidence=.9):
                            print('Video Playback Completed')
                            time.sleep(3)
                            pg.click(pg.locateCenterOnScreen('ExitPlayback.png', confidence=.7))
                            print("HMA 57 PASS")
                            break
                        else:
                            time.sleep(30)
                            print("Video playback")
#                    pg.click(pg.locateCenterOnScreen('Exit.PNG', confidence=.7))
                elif icon in ['Failsafe Video.PNG']:
                    print("Visiting menu item:: ", icon)
                    pg.click(pg.locateCenterOnScreen(icon))
                    print("Starting Video PLayback")
                    pg.click(pg.locateCenterOnScreen('Play.PNG', confidence=.7))
                    t_end = time.time() + 60 * 3
                    while time.time() < t_end:
                        if pg.locateCenterOnScreen('vidcomplete.PNG', confidence=.6):
                            # or pg.locateCenterOnScreen('vidcomplet.PNG', confidence=.9):
                            print('Video Playback Completed')
                            time.sleep(3)
                            pg.click(pg.locateCenterOnScreen('ExitPlayback.png', confidence=.7))
                            print("HMA 207 PASS")
                            break
                        else:
                            time.sleep(30)
                            print("Video playback")
                elif icon in ['Snap.PNG']:
                    print("Visiting menu item:: ", icon)
                    pg.click(pg.locateCenterOnScreen(icon))
                    print("Viewing Snapshots")
                    t_end = time.time() + 60 * .5
                    while time.time() < t_end:
                        if pg.locateCenterOnScreen('SelectedImage.PNG', confidence=.6) or pg.locateCenterOnScreen('Image.PNG', confidence=.9):
                            print('Loading Snapshot')
                            pg.click(pg.locateCenterOnScreen('Image.PNG', confidence=.9))
                            time.sleep(2)
                            pg.click(pg.locateCenterOnScreen('View.png', confidence=.7))
                            if pg.locateCenterOnScreen('OSDW1W2.PNG', confidence=.6):
                                print("Snapshot Loaded Successfully")
                                print("HMA 353 PASS")
                            else:
                                print("Some Issue with Loading Snapshot Image")
                            pg.click(pg.locateCenterOnScreen('Exit.png', confidence=.5))
                            #break
                elif icon in ['SwitchOfficer.PNG']:
                    print("Visiting menu item:: ", icon)
                    pg.click(pg.locateCenterOnScreen(icon))
                    print("Testing Switch Officer Menu")
                    t_end = time.time() + 60 * .5
                    while time.time() < t_end:
                        if pg.locateCenterOnScreen('OfficerTextBox.PNG', confidence=.6):
                            print('Enterring Credentials')
                            pg.click(pg.locateCenterOnScreen('OfficerTextBox.PNG', confidence=.7))
                            pg.typewrite("1111")
                            pg.press('tab')
                            pg.typewrite("1111")
                            pg.click(pg.locateCenterOnScreen('Login.png', confidence=.7))
                            time.sleep(4)
                            if pg.locateCenterOnScreen('Menu.PNG', confidence=.6):
                                print("Logged in Successfully")
                                pg.click(pg.locateCenterOnScreen('Menu.PNG', confidence=.6))
                                pg.click(pg.locateCenterOnScreen('SwitchOfficer.PNG', confidence=.7))
                                print('Enterring Credentials')
                                pg.click(pg.locateCenterOnScreen('OfficerTextBox.PNG', confidence=.7))
                                pg.typewrite("gagan")
                                pg.press('tab')
                                pg.typewrite("123")
                                pg.click(pg.locateCenterOnScreen('Login.png', confidence=.7))
                                time.sleep(7)
                                pg.click(pg.locateCenterOnScreen('Menu.PNG', confidence=.6))
                                print("HMA 354 PASS")
                                time.sleep(3)
                            else:
                                print("Some Issue SwitchOfficer Menu")
                            #pg.click(pg.locateCenterOnScreen('Exit.png', confidence=.5))
                            break
                elif icon in ['Upload.PNG']:
                    upload()
                    print("HMA 352 PASS")
                    pg.click(pg.locateCenterOnScreen('Menu.PNG', confidence=.7))
                else:
                    print("Visiting menu item:: ", icon)
#                if pg.locateCenterOnScreen('MenuPage.PNG', confidence=.7):
#                    print("On Menu PAge, checking next Icon")
#                elif pg.locateCenterOnScreen('Menu.PNG', confidence=.7):
#                    pg.click(pg.locateCenterOnScreen('Menu.PNG', confidence=.7))
#                else:
                pg.click(pg.locateCenterOnScreen('Exit.PNG', confidence=.7))
            else:
                absent.append(icon)
                continue
        print("Finished Checking First Screen")
        print("\n\n Following TESTS PASSED:: HMA:: 353  352 356  357  211  359  360  196  57  145  354  207   260  147 \n\n")
        time.sleep(3)
        if pg.locateCenterOnScreen('MenuPage.PNG', confidence=.7):
            print("On Menu PAge, checking next Icon")
        elif pg.locateCenterOnScreen('Menu.PNG', confidence=.7):
            pg.click(pg.locateCenterOnScreen('Menu.PNG', confidence=.7))
            time.sleep(2)
        trys=0
        while trys<3:
            if pg.locateCenterOnScreen('Next.PNG', confidence=.9):
                print("Moving to Second Screen")
                trys=3
                pg.click(pg.locateCenterOnScreen('Next.png'))
            else:
                trys +=1
                print("Couldn't Move to Next Screen, Trying Again")
        time.sleep(2)
#        icon_list2 = ['Live Audio.PNG', 'Summary.PNG', 'Volume.PNG', 'Run Status.PNG', 'Log File.PNG']
        icon_list2 = ['Summary.PNG', 'Volume.PNG', 'Run Status.PNG', 'Log File.PNG']

        for icon in icon_list2:
            if pg.locateCenterOnScreen(icon, confidence=.9):
                present.append(icon)
                pg.click(pg.locateCenterOnScreen(icon))
                if icon in ['Summary.PNG']:
                    get_summary()
                    time.sleep(4)
                    if pg.locateCenterOnScreen('Menu.PNG', confidence=.7):
                        pg.click(pg.locateCenterOnScreen('Menu.PNG', confidence=.7))
                        time.sleep(2)
                        pg.click(pg.locateCenterOnScreen('Next.png'))
                        time.sleep(2)
                    else:
                        print("Some Issue Occured on summary screen")
                    time.sleep(5)
                else:
                    continue
                pg.click(pg.locateCenterOnScreen('Exit.PNG', confidence=.7))
            else:
                absent.append(icon)
        print("Finished Checking Second Screen")
        time.sleep(3)

        print("The Icons that are Present:: " + str(present) + "\nNumber of Icons Found:: " + str(len(present)))
        print("The Icons that are Absent:: " + str(absent) + "\nNumber of Icons Not Found:: " + str(len(absent)))

        if pg.locateCenterOnScreen('Back.PNG'):
            print("Not on Live View Screen, Moving to Live View Screen")
            pg.click(pg.locateCenterOnScreen('ExitB.png'))
        elif pg.locateCenterOnScreen('Next.PNG'):
            print("Not on Live Screen, Moving to Live View Screen")
            pg.click(pg.locateCenterOnScreen('ExitA.png'))
        elif pg.locateCenterOnScreen('Menu.PNG'):
            print("Back On Live View Screen, Menu Icons Check Done")
            pass

    except:
        print("Some Error Occurred")

def check_menu_icons():
    try:
        A1.MDC_login()
        time.sleep(2)
        #pg.click(pg.locateCenterOnScreen('ExitB.png'))

        if pg.locateCenterOnScreen('Back.PNG'):
            print("Not on Live View Screen, Moving to Live View Screen")
            pg.click(pg.locateCenterOnScreen('ExitB.png'))
        elif pg.locateCenterOnScreen('Next.PNG'):
            print("Not on Live Screen, Moving to Live View Screen")
            pg.click(pg.locateCenterOnScreen('ExitA.png'))
        elif pg.locateCenterOnScreen('Menu.PNG'):
            print("On Live View Screen, Starting Menu Icons Check")
            pass
        else:
            print("Couldn't find Buttons to navigate")

        pg.PAUSE = 1
        time.sleep(5)
        menu = pg.locateCenterOnScreen('Menu.PNG')
        pg.click(menu)

        icon_list1 = ['ICV Video.PNG', 'Failsafe Video.PNG', 'Snap.PNG', 'BWC Video.PNG', 'In-Car Mic.PNG', 'Switch Officer.PNG', 'Upload.PNG', 'Shut Down.PNG']
        present = []
        absent = []
        for icon in icon_list1:
            if pg.locateCenterOnScreen(icon):
                present.append(icon)
            else:
                absent.append(icon)
        print("Finished Checking First Screen")
        time.sleep(3)
        trys=0
        while trys<3:
            if pg.locateCenterOnScreen('Next.PNG', confidence=.9):
                print("Moving to Second Screen")
                trys=3
                pg.click(pg.locateCenterOnScreen('Next.png'))
            else:
                trys +=1
                print("Couldn't Move to Next Screen, Trying Again")
        time.sleep(2)
        icon_list2 = ['Live Audio.PNG', 'Summary.PNG', 'Volume.PNG', 'Run Status.PNG', 'Log File.PNG',
                      'Run Status.PNG', 'Streaming.PNG', 'BOLO.PNG']

        for icon in icon_list2:
            if pg.locateCenterOnScreen(icon, confidence=.9):
                present.append(icon)
            else:
                absent.append(icon)
        print("Finished Checking Second Screen")
        time.sleep(3)

        print("The Icons that are Present:: " + str(present) + "\nNumber of Icons Found:: " + str(len(present)))
        print("The Icons that are Absent:: " + str(absent) + "\nNumber of Icons Not Found:: " + str(len(absent)))

        if pg.locateCenterOnScreen('Back.PNG'):
            print("Not on Live View Screen, Moving to Live View Screen")
            pg.click(pg.locateCenterOnScreen('ExitB.png'))
        elif pg.locateCenterOnScreen('Next.PNG'):
            print("Not on Live Screen, Moving to Live View Screen")
            pg.click(pg.locateCenterOnScreen('ExitA.png'))
        elif pg.locateCenterOnScreen('Menu.PNG'):
            print("Back On Live View Screen, Menu Icons Check Done")
            pass

    except:
        print("Some Error Occurred")
'''
        osd_list = ['OSDDate.PNG', 'OSDTime.PNG', 'GPSS.PNG', 'GPSW.PNG']
        osd_sens = ['GPSMPH.PNG']
        osdpresent = []
        osdabsent = []
        for osd in osd_list:
            if pg.locateCenterOnScreen(osd, confidence=.5):
                osdpresent.append(osd)
            else:
                osdabsent.append(osd)
        for osd in osd_sens:
            if pg.locateCenterOnScreen(osd, confidence=.3):
                osdpresent.append(osd)
            else:
                osdabsent.append(osd)
        print("The Icons that are Present:: " + str(present))
        print("The Icons that are Absent:: " + str(absent))

        print("The OSDs that are Present:: " + str(osdpresent))
        print("The OSDs that are Absent:: " + str(osdabsent))
'''
#check_menu_icons()

def load_mdc0(ip):
    if pg.locateCenterOnScreen('minmdc.PNG'):
        print("MDC was Minimized, Loading")
        pg.click(pg.locateCenterOnScreen('minmdc.PNG'))
    pass
    pg.hotkey('win', 's')
    pg.typewrite("H1 Focus MDC Application")
    #pg.press('shift\n')
    pg.keyDown('Shift')
    pg.hotkey('enter')
    time.sleep(5)
    pg.keyUp('Shift')
    if pg.locateCenterOnScreen('ip.png'):
        pg.PAUSE = 1
        pg.click(pg.locateCenterOnScreen('ip.png'))
        #ip = '172.18.4.87'
        ip_split = ip.split('.')
        #pg.typewrite(['backspace', 'backspace', 'backspace'], interval=.5)
        #pg.typewrite(ip_split[0])
        pg.typewrite(['backspace', 'backspace', 'backspace'], interval=.5)
        pg.typewrite(ip_split[1])
        pg.press('tab')
        pg.typewrite(['backspace', 'backspace', 'backspace'], interval=.5)
        pg.typewrite(ip_split[2])
        pg.press('tab')
        pg.typewrite(['backspace', 'backspace', 'backspace'], interval=.5)
        pg.typewrite(ip_split[3])
        pg.click(pg.locateCenterOnScreen('loginext.png'))
    # pg.keyUp('Shift')
    #time.sleep(2)
    #pg.doubleClick(pg.locateCenterOnScreen('version.PNG'))

#check_menu()
def load_mdc():
    if pg.locateCenterOnScreen('minmdc.PNG'):
        print("MDC was Minimized, Loading")
        pg.click(pg.locateCenterOnScreen('minmdc.PNG'))
    pass
    pg.hotkey('win', 's')
    pg.typewrite("H1 Focus MDC Application")
    #pg.press('shift\n')
#    pg.keyDown('Shift')
    pg.hotkey('enter')
    # pg.keyUp('Shift')
    #time.sleep(2)
    #pg.doubleClick(pg.locateCenterOnScreen('version.PNG'))
    time.sleep(5)
    # print(pg.position())
    # pg.click
    # pg.hotkey('win','ctrl', 'd')
    # pg.press('shiftleft')
    # pg.click(x=1848, y=917)
    # try:
    #    pg.locateOnScreen('Max.PNG')
    # except FileNotFoundError:
    #    print("Some None Error Occurred")
    # except TypeError:
    #    print("Error")
    # except ImageNotFoundException:
    #    print("MDC Icon not found")
def icon_check():
    try:
        if pg.locateCenterOnScreen('Back.PNG'):
            print("Not on Live View Screen, Moving to Live View Screen")
            pg.click(pg.locateCenterOnScreen('ExitB.png'))
        elif pg.locateCenterOnScreen('Next.PNG'):
            print("Not on Live Screen, Moving to Live View Screen")
            pg.click(pg.locateCenterOnScreen('ExitA.png'))
        elif pg.locateCenterOnScreen('Menu.PNG'):
            print("On Live View Screen, Starting Icons Check")
            pass
        icon_list = ['Snapshot.PNG','Bookmark.PNG','Switch.PNG','Cam1.PNG','Cam2.PNG','Cam3.PNG','Cam11.PNG','Cam22.PNG','Cam33.PNG','StatusAlarm.PNG', 'StatusCamera.PNG', 'StatusOfficer.PNG', 'StatusUSB.PNG', 'StatusWifi.PNG', 'Next.PNG', 'ICVExit.PNG']
        present = []
        absent = []
        for icon in icon_list:
            if pg.locateCenterOnScreen(icon):
                present.append(icon)
            else:
                absent.append(icon)

        osd_list = ['OSDDate.PNG', 'OSDTime.PNG', 'GPSS.PNG', 'GPSW.PNG']
        osd_sens = ['GPSMPH.PNG']
        osdpresent = []
        osdabsent = []
        for osd in osd_list:
            if pg.locateCenterOnScreen(osd, confidence=.5):
                osdpresent.append(osd)
            else:
                osdabsent.append(osd)
        for osd in osd_sens:
            if pg.locateCenterOnScreen(osd, confidence=.3):
                osdpresent.append(osd)
            else:
                osdabsent.append(osd)
        print("The Icons that are Present:: " + str(present))
        print("The Icons that are Absent:: " + str(absent))

        print("The OSDs that are Present:: " + str(osdpresent))
        print("The OSDs that are Absent:: " + str(osdabsent))

    except:
        print("Something Went Wrong")


def enter_meta(tab=True):
    #try:
        #if locate_all:
        #    return pg.click(pg.locateAllOnScreen('textbox.png', grayscale=grayscale))
        #    print("Found More than 1 Fields")
        #field_list = ['ticket1', 'case1', 'dispatch1', 'location1', 'Fname1', '2tick1','2case1','2disp[atch1', '2notes1', '2lastname1']
        import random
        with open("MetaMDC.json", "r") as f:
            data = json.load(f)
        nNames = len(data["driver"])
        nFirstName = random.randint(1, nNames) - 1
        nLastName = nFirstName
        while nLastName == nFirstName:     # to protect the innocent
            nLastName = random.randint(1, nNames) - 1
        strFirstName, junk = data["driver"][nFirstName].split(maxsplit=1)
        junk, strLastName = data["driver"][nLastName].split(maxsplit=1)
        nEvent = random.randint(1, len(data["eventname"])) - 1
        intEventID = data["eventidint"][nEvent]
        strEventID = data["eventidmask"][nEvent]
        #num1 = random.randint(1000000,9999999)
        CaseN = str(random.randint(1000000,9999999))
        DispN = str(random.randint(1000000,9999999))
        #CaseN = strEventID.split('0')[0]
        #DispN = strEventID.split('0')[1]
        strEventName = data["eventname"][nEvent]
        nGender = random.randint(1, len(data["gender"])) - 1
        strGender = "Gender = %s" % (data["gender"][nGender])
        nRace = random.randint(1, len(data["race"])) - 1
        strRace = "Race = %s" % (data["race"][nRace])
        strLocation = data["location"][nEvent]
        strNotes = strGender + strRace + A1.RandomText(20)
        field_list = [intEventID,CaseN,DispN,strLocation,strFirstName,strEventID,strEventName,strEventName,strNotes,strLastName]
        if tab:
            if pg.locateCenterOnScreen('data.png'):
                pg.click(pg.locateCenterOnScreen('data.png'))
                pg.typewrite(['tab', 'tab', 'tab'], interval=.5)
                iter = 0
                while iter < 10:
                    pg.typewrite(field_list[iter])
                    pg.typewrite(['tab'], interval=.5)
                    iter+=1
                    #pg.typewrite('Case01')
                #pg.click(pg.locateCenterOnScreen('orange.png', confidence=.8))
                pg.click(pg.locateCenterOnScreen('Save.png', confidence=.5))
                return field_list
                #pg.typewrite(['tab', 'tab'], interval=.5)
                #pg.typewrite('enter')
            else:
                print('Form was Not Found')
        else:
            if pg.click(pg.locateOnScreen('textbox.png')):
                pg.typewrite('Ticket01')
                #return print(pg.locateOnScreen('textbox.png', grayscale=grayscale))
            else:
                print("Textbox Field was not found")
def upload():
    try:
        pg.PAUSE = 1
        A1.MDC_login()
        time.sleep(3)
        if pg.locateCenterOnScreen('ICVStart.png'):
            pass
        elif pg.locateCenterOnScreen('Next.PNG'):
            pg.click(pg.locateCenterOnScreen('Upload.png'))
        elif pg.locateCenterOnScreen('Menu.PNG'):
            pg.click(pg.locateCenterOnScreen('Menu.png'))
            pg.click(pg.locateCenterOnScreen('Upload.png'))
            pass
        else:
            print("Could Not get there")
        time.sleep(5)
        if pg.locateCenterOnScreen('ICVStart.png'):
            print("Starting ICV Upload")
            pg.click(pg.locateCenterOnScreen('ICVStart.png'))
        else:
            print("Could not Start ICV Upload")
        #time.sleep(120)
        t=0
        while t<2000:
            time.sleep(3)
            if (pg.locateCenterOnScreen('hundred.png')) and (pg.locateCenterOnScreen('Idle.png')):
                pg.click(pg.locateCenterOnScreen('ICVExit.png', confidence=.9))
                time.sleep(2)
                pg.click(pg.locateCenterOnScreen('ExitA.png', confidence=.8))
                print("Done Uploading, took %s seconds" % (t))
                break
            else:
                time.sleep(1)
                t+=1
                continue
        #if (pg.locateCenterOnScreen('hundred.png')) and (pg.locateCenterOnScreen('Idle.png')):
        #    print("Done Uploading")
        #    pg.click(pg.locateCenterOnScreen('ICVExit.png'))
        #    pg.click(pg.locateCenterOnScreen('ExitA.png'))

    except:
        print("Some Error Ocurred")
        #while (pg.locateCenterOnScreen('hundred.png') is None:

    #except:
    #    print("Could not enter metadata effectively")

##        pg.doubleClick(pg.locateCenterOnScreen('version.PNG'))

def rec(n, cameras, lensecs):
    try:
        A1.MDC_login()
        i = 0
        rec_list = []
        while i < n:
            if pg.locateCenterOnScreen('Back.PNG'):
                print("Not on Recording Screen, Moving to Recording Screen")
                pg.click(pg.locateCenterOnScreen('ExitB.png'))
            elif pg.locateCenterOnScreen('Next.PNG'):
                print("Not on Recording Screen, Moving to Recording Screen")
                pg.click(pg.locateCenterOnScreen('ExitA.png'))
            elif pg.locateCenterOnScreen('Menu.PNG'):
                print("On Recording Screen, Starting Recording")
                pass
            else:
                print("MDC Not Loaded, Attempting to Load MDC")
                load_mdc()
            #pg.click(pg.locateCenterOnScreen('Max.PNG'))
            time.sleep(5)
            pg.click(pg.locateCenterOnScreen('Cam1.PNG'))
            time.sleep(1)
            if (cameras==1):
                pass
            else:
                pg.click(pg.locateCenterOnScreen('Cam2.png'))
                time.sleep(2)
            if (cameras==3):
                pg.click(pg.locateCenterOnScreen('Cam3.png'))
                time.sleep(2)
            elif (cameras==2):
                pass
            else:
                pass
            pg.click(pg.locateCenterOnScreen('Snapshot.png'))
            pg.click(pg.locateCenterOnScreen('Bookmark.png'))
            time.sleep(2)
            pg.click(pg.locateCenterOnScreen('Switch.png'))
            time.sleep(2)
            pg.click(pg.locateCenterOnScreen('Snapshot.png'))
            pg.click(pg.locateCenterOnScreen('Bookmark.png'))
            time.sleep(lensecs)
            if (cameras==3):
                pg.click(pg.locateCenterOnScreen('Cam33.png'))
            elif (cameras==2):
                pass
            else:
                pass
            if (cameras==1):
                pass
            else:
                pg.click(pg.locateCenterOnScreen('Cam22.png'))
            pg.click(pg.locateCenterOnScreen('Cam11.png'))
            time.sleep(3)
            meta = enter_meta()
            rec_list.append(meta)
            time.sleep(2)

            #pg.click(pg.locateCenterOnScreen('Savee.png'))
            print("All Steps were Executed Successfully")
            i+=1
        print(rec_list)
    #return rec_list
    except None:
        print("Some None Error Occurred")
    except TypeError:
        print("Error")
    except ImageNotFoundException:
        print("MDC Icon not found")
    except FileNotFoundError:
        print("The Icon File was not Found")
    return rec_list
#icon_check()

def ipc_coll():
    if pg.locateCenterOnScreen('Back.PNG'):
        print("Not on Recording Screen, Moving to Recording Screen")
        pg.click(pg.locateCenterOnScreen('ExitB.png'))
    elif pg.locateCenterOnScreen('Next.PNG'):
        print("Not on Recording Screen, Moving to Recording Screen")
        pg.click(pg.locateCenterOnScreen('ExitA.png'))
    elif pg.locateCenterOnScreen('Menu.PNG'):
        print("On Recording Screen, Starting Work")
        pass
    else:
        print("MDC Not Loaded, Attempting to Load MDC")
        load_mdc()
    import time

    t_end = time.time() + 60 * 5
    while time.time() < t_end:
        if pg.locateCenterOnScreen('Cam11.PNG'):
            print('Found Active recording to Close')
            time.sleep(3)
            pg.click(pg.locateCenterOnScreen('Cam11.png'))
            meta = enter_meta()
    print("Check loop Ended")
    A2.Gs_Test3()
    parse.ipc_checker("ipc")
    print("Verify loop Ended, Exiting")


def server_compare(recorded_list):
    import Check1, Check22
    Videos = Check22.Check(recorded_list)
    # Videos3 = []
    Uploaded_Videos = set(Videos)
    print("**** TESTCASE COMPLETED : TESTED OK **** \n **** Following Video are on the server")
    print(Videos)
    print("Printing List of Videos Recorded")
    Recorded_Videos = set(recorded_list)
    if (Uploaded_Videos & Recorded_Videos):
        print(
            "**** TESTCASE COMPLETED :  TESTED OK **** \n Following Videos Match on the server ****")
        # print("Following Videos Match on the server")
        print(Uploaded_Videos & Recorded_Videos)
    else:
        print("No common elements")

def server_check(ffnames):
    import Check1, Check2, Check26, Check3ccqa, Check27
    #Videos = Check3ccqa.Check()
    Videos = Check26.Check()
    # Videos3 = []
    fffname = []
    for name in ffnames:
        name1 = name.split('.')[0]
        fffname.append(name1)
    print("Following Videos are to be compared on the Server:: \n", ffnames)
    Uploaded_Videos = set(Videos)
    print("**** TESTCASE COMPLETED : TESTED OK **** \n **** Following Video are on the server")
    print(Videos)
    print("Printing List of Videos Recorded")
    Recorded_Videos = set(fffname)
    print("Following Videos are to be compared on the Server:: \n", Recorded_Videos)
    if (Uploaded_Videos & Recorded_Videos):
        print(
            "**** TESTCASE COMPLETED :  TESTED OK **** \n Following Videos Match on the server ****")
        # print("Following Videos Match on the server")
        print(Uploaded_Videos & Recorded_Videos)
    else:
        print("No common elements")

def fw_compare(recorded_list,fnames):
    fname = []
    for name in fnames:
        name1 = name.split('.')[0]
        fname.append(name1)
    FW_Videos = set(fname)
    print("**** TESTCASE: Comparing Recorded Videos **** \n **** With Videos on the FW")
    print(FW_Videos)
    print("Printing List of Videos Recorded")
    Recorded_Videos = set(recorded_list)
    print(Recorded_Videos)
    if (FW_Videos & Recorded_Videos):
        print(
            "**** TESTCASE COMPLETED :  TESTED OK **** \n Following Videos Match on the FW ****")
        # print("Following Videos Match on the server")
        print(FW_Videos & Recorded_Videos)
    else:
        print("No common elements between Recorded Videos and FW Videos")

def pseudo_main():
    repetitions = 0
    while repetitions < 1:
        get_summary()
        rep = 1
        cams = 2
        lensecs = 10
        while rep < 3:
            icon_check()
            if (rep==1):
                meta_list = rec(rep, cams, lensecs)
            else:
                meta_list.extend(rec(rep, cams, lensecs))
            rep +=1
            print("Here's the metadata that got recorded:: ", meta_list)

        upload()
        time.sleep(120)
        import parse
        time.sleep(60)
        #fnames = A1.Gs_Test4("172.18.4.87")
        fnames = A1.Gs_Test4("172.18.4.68")
        #meta_list = [['2222679097899', '2222679', '97899', 'Clifford Street', 'Chris', '2222679097899', 'STP', 'STP', 'Gender = FemaleRace = Nnnnnativeinto was accident. a', 'Johnson'], ['333354076789789', '333354', '76789789', 'Dernwood Street', 'William', '333354076789789', 'SPD', 'SPD', 'Gender = FemaleRace = Hiiiispanicinto accident. the a', 'Earnhardt'], ['111456780986', '11145678', '986', 'Burbidge Street', 'Darrell', '111456780986', 'DWI', 'DWI', 'Gender = FemaleRace = Caucassssianthe accident. ', 'McMurray']]
        recorded_list = parse.meta_matcher(meta_list, 'Metadata_file.log')
        fw_compare(recorded_list,fnames)
        time.sleep(300)
        #recorded_list=['13@20200408153438-1', '13@20200408153440-2', '13@20200408153436-0']
        server_check(fnames)
        time.sleep(10)
        repetitions +=1
        parse.parser("Error")
        parse.parser("w")
        parse.fwtimer()
        #load_mdc0('172.18.4.87')
        #enter_meta()
###pseudo_main()
check_menu_all()
#parse.ipc_checker("ipc")
#ipc_coll()
#meta_list = rec(1, 1)
#upload()
#time.sleep(400)
#import Check22
#print(meta_list[0])
#Check22.Check(meta_list[0])

#import AutomationH2 as A1
#A1.Gs_Test4("172.18.4.87")
'''
import parse

time.sleep(60)
fnames = A1.Gs_Test4("172.18.4.87")
recorded_list = parse.meta_matcher(meta_list, 'Metadata_file.log')
fw_compare(recorded_list, fnames)
time.sleep(300)
recorded_list=['2222679097899', 'testing', '97899', 'Clifford Street', 'Chris', '2222679097899', 'STP', 'STP', 'Gender = FemaleRace = Nnnnnativeinto was accident. a', 'Johnson']
import Check22
Check22.Check(recorded_list)
'''
#server_compare(recorded_list)

#recorded_list=['13@20200408153438-1', '13@20200408153440-2', '13@20200408153436-0']
#server_check(recorded_list)
#pseudo_main()
'''
time.sleep(5)
pyautogui.click()
distance = 400

while distance > 0 :
    pyautogui.dragRel(distance, 0, duration=0.2)
    distance = distance - 50
'''