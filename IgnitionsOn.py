import ERB24
ERB24_BOARD_NUM = 0


def IgnitionsOn():
    print("Turning on Ignitions...")
    for xx in range (8, 16):
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=xx)


#
# Entry point for module
#
if (__name__ == "__main__"):
    IgnitionsOn()
