# -*- coding: utf-8 -*-
"""
ERB24.py - hWin (Henry Witwicki)

The following is an attempt to start to automate.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""


__version__ = '1.02'
__date__    = '2018/10/30'
__author__  = 'Henry Witwicki'


#--- CHANGES ----------------------------------------------------------------
#
#  2014/12/24 - hWin, initial layout
#  2018/09/05 - hWin, exposed callable routines
#  2018/10/30 - hWin, added some simple test routines
#


#--- IMPORTS ----------------------------------------------------------------
import time


DEBUG = False
DEBUG = True

import ctypes as ctypes
#changed link to 32 bit from 64 bit
mcUL = ctypes.WinDLL("C:\Program Files (x86)\Measurement Computing\DAQ\cbw32.dll")


BOARD_NUM_0 = 0
FIRSTPORTA = 10
RELAY_01 = 0
RELAY_02 = 1
RELAY_03 = 2
RELAY_04 = 3
RELAY_05 = 4
RELAY_06 = 5
RELAY_07 = 6
RELAY_08 = 7
RELAY_09 = 8
RELAY_10 = 9
RELAY_11 = 10
RELAY_12 = 11
RELAY_13 = 12
RELAY_14 = 13
RELAY_15 = 14
RELAY_16 = 15
RELAY_17 = 16
RELAY_18 = 17
RELAY_19 = 18
RELAY_20 = 19
RELAY_21 = 20
RELAY_22 = 21
RELAY_23 = 22
RELAY_24 = 23
RELAY_OFF = 0
RELAY_ON = 1


def hWin_ERB24RelayOn(nBoard, nRelay):
    mcUL.cbDBitOut(nBoard, FIRSTPORTA, nRelay, RELAY_ON)


def hWin_ERB24RelayOff(nBoard, nRelay):
    mcUL.cbDBitOut(nBoard, FIRSTPORTA, nRelay, RELAY_OFF)


def myTestAllOff():
    for xx in range(0, 24):
        hWin_ERB24RelayOff(BOARD_NUM_0, xx)


def myTestAllOn():
    for xx in range(0, 24):
        hWin_ERB24RelayOn(BOARD_NUM_0, xx)


def myTest():
    if DEBUG:
        mcUL.cbFlashLED(BOARD_NUM_0)
    print("Turning relay 18 on...")
    hWin_ERB24RelayOn(BOARD_NUM_0, RELAY_17)
    time.sleep(3)
    hWin_ERB24RelayOff(BOARD_NUM_0, RELAY_17)
    print("Turning relay 18 off...")


if __name__=='__main__':
    myTest()
