# Function to calculate even sum and odd product
def even_sum_odd_product(lst, n):
    even = 0
    odd = 1
    result = [None]*n

    for i in range(0, n):

        if (i % 2 == 0):
            j=i
            while j>=0:
                even += lst[j]
                j -= 1
            result[i] = even
        else:
            j=i
            while j>=0:
                odd *= lst[j]
                j -=1
            result[i] = odd

    print("Even Index Product : ", even)
    print("Odd Index Product : ", odd)
    print(result)

lst = [1,2,3,4,5]
n = len(lst)
even_sum_odd_product(lst, n)