import json
import os
import random


def RandomText(nMaxLength):
    """A simple routine to return a random string built using the panagram."""
    DEBUG = False
    DEBUG = True

    Panagram = ['The', 'quick', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy', 'dog.']     # simple panagram
    #Panagram = ['The', 'quick', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy', 'dog.', "#%*/+-()'", ',;@=?!:"']
    str = ""
    print(Panagram)
    random.shuffle(Panagram)

    nLen = random.randint(1, len(Panagram))
    for xx in range (nLen):
        str += Panagram[xx] + " "
    #str = str.rstrip()     # strip the trailing space
    str = str[:nMaxLength]

    if 0:
        print(str)

    return str
#end RandomText()


def jsonTest():

    with open("MetadataH1.json", "r") as f:
        data = json.load(f)

    nItem1 = len(data["eventname"])
    nItem2 = len(data["metadata"])
    if nItem1 > nItem2:
        print("Not enough metadata entrys!")
        exit()

    if 1:
        nCount = min(nItem1, nItem2)
        bob1 = data["metadata"]
        bob2 = data["metadata"][0]
        bob3 = data["eventname"]
        tt = data["eventname"][0]
        #for key in data["eventname"]:
        #    print("%s:"% (key))

    for xx in range(0, 10):
        # randomize the event data
        nNames = len(data["driver"])
        nFirstName = random.randint(1, nNames) - 1
        nLastName = nFirstName
        while nLastName == nFirstName:     # protect the innocent
            nLastName = random.randint(1, nNames) - 1
        strFirstName, junk = data["driver"][nFirstName].split(maxsplit=1)
        junk, strLastName = data["driver"][nLastName].split(maxsplit=1)

        nEvent = len(data["eventname"])
        nEvent = random.randint(1, nEvent) - 1
        nGender = len(data["gender"])
        nGender = random.randint(1, nGender) - 1
        nRace = len(data["race"])
        nRace = random.randint(1, nRace) - 1

        print("%s:" % (data["eventname"][nEvent]))
        print("\tName = %s %s" % (strFirstName, strLastName))
        print("")
        print("\tTicket# = ")
        print("\tCase# = ")
        print("\tDisp# = ")
        for key in (data["metadata"][nEvent]):
            print("\t%s = "% (key), end='' )
            if False:
                pass
            elif key == "race":
                print(data["race"][nRace])
            elif key == "gender":
                print(data["gender"][nGender])
            elif data["metadata"][nEvent][key] == "":
                #print("anything goes")
                print(RandomText(40))
            else:
                print( data["metadata"][nEvent][key])

        # build json strings
        # "key": "value",
        jMetaData = '{ "metadata": [ {'
        jMetaData += '"event_name": "%s", ' % (data["eventname"][nEvent])
        jMetaData += '"first_name": "%s", ' % (strFirstName)
        jMetaData += '"last_name": "%s", ' % (strLastName)
        jMetaData += '"gender": "%s", ' % (data["gender"][nGender])
        jMetaData += '"race": "%s",' % (data["race"][nRace])
        jMetaData += '"location": "%s"' % (RandomText(40))
        jMetaData += '} ] }'
        print(jMetaData)

    x=1
#end of jsonTest()


#
# Entry point for module
#
if (__name__ == "__main__"):
    jsonTest()
    print ("Done!")

