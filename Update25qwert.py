from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys

# allow easy time-boxing: 'for sec in max_seconds(42): do_something()'
def max_seconds(max_seconds, *, interval=1):
    interval = int(interval)
    start_time = time.time()
    end_time = start_time + max_seconds
    yield 0
    while time.time() < end_time:
        if interval > 0:
            next_time = start_time
            while next_time < time.time():
                next_time += interval
            time.sleep(int(round(next_time - time.time())))
        yield int(round(time.time() - start_time))
        if int(round(time.time() + interval)) > int(round(end_time)):
            return

def Update():
    import time
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    User = 'gagan'
    # input("\nEnter Valid User Name: \n")
    Pass = '123'
    # input("\nEnter Valid Password: \n")
    # B = input("\nEnter Time Value in Seconds for Brake Delay: \n")

    browser = webdriver.Chrome("C:\Program Files (x86)\Google\Chrome\Application\chromedriver.exe")
    browser.maximize_window()
    #browser.get('https://cobanqah104.usgovvirginia.cloudapp.usgovcloudapi.net:803/#/login')
    browser.implicitly_wait(20000)

    if (browser.find_element_by_xpath("/html/body/div/div[2]/button[3]")):
        browser.find_element_by_xpath(
            "/html/body/div/div[2]/button[3]").click()
        browser.find_element_by_xpath("/html/body/div/div[3]/p[2]/a").click()
        time.sleep(5)
        browser.find_element_by_xpath(
            "/html/body/c3-app/c3-login/div/div[2]/div[1]/div/form/div/div[2]/input").send_keys(User)
        browser.find_element_by_xpath(
            "/html/body/c3-app/c3-login/div/div[2]/div[1]/div/form/div/div[2]/div[1]/div[1]/input").send_keys(Pass)
        browser.find_element_by_xpath(
            "/html/body/c3-app/c3-login/div/div[2]/div[1]/div/form/div/div[2]/div[1]/div[2]/input").click()

    else:
        browser.find_element_by_xpath(
            "/html/body/c3-app/c3-login/div/div[2]/div[1]/div/form/div/div[2]/input").send_keys(User)
        browser.find_element_by_xpath(
            "/html/body/c3-app/c3-login/div/div[2]/div[1]/div/form/div/div[2]/div[1]/div[1]/input").send_keys(Pass)
        browser.find_element_by_xpath(
            "/html/body/c3-app/c3-login/div/div[2]/div[1]/div/form/div/div[2]/div[1]/div[2]/input").click()

    time.sleep(6)
    browser.find_element_by_xpath("/html/body/c3-app/ng-component/div[1]/div/block-ui/c3-adm-home/div/div[2]/div[1]/div[2]/div[1]/a/div").click()
    browser.find_element_by_xpath("/html/body/c3-app/ng-component/div[1]/div/block-ui/c3-adm-navigator/div/ng-component/div/c3-adm-template-dashboard/div/div/div/ul/li[5]").click()
    browser.find_element_by_xpath("/html/body/c3-app/ng-component/div[1]/div/block-ui/c3-adm-navigator/div/ng-component/div/c3-adm-template/div/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]/h5/a").click()


#    browser.find_element_by_xpath(
#        "/html/body/c3-app/ng-component/block-ui/div/div[1]/div/c3-adm-navigator/div/ng-component/div/c3-adm-template/div/div/kendo-grid/div/kendo-grid-list/div/div[1]/table/tbody/tr[2]/td[4]/button").click()
    #browser.find_element_by_xpath("/html/body/c3-app/ng-component/block-ui/div/div[1]/div/c3-adm-navigator/div/ng-component/div/c3-adm-template/div/div/kendo-grid/div/kendo-grid-list/div/div[1]/table/tbody/tr/td[4]/button").click()
    browser.implicitly_wait(20000)
    from selenium.webdriver.support.wait import WebDriverWait
    import time

    configurations = ["audio", "system", "ds", "w"]
    #len(configurations)
    #for conf in range(len(configurations)):
    #    print(configurations[conf])
    #    print((type(configurations[conf])))

    try:
        print(configurations)
        config = configurations
        for conf in config:
            print(conf)
            if conf in ["audio-target", "audio"]:
                element = WebDriverWait(browser, 10).until(
                    lambda x: x.find_element_by_id("audio-target"))
                time.sleep(5)
                element.click()
                import random
                option_list = ['Mute', 'Marker', 'Covert Interview', 'Trigger Camera2 Recording', 'Snapshot']
                from selenium.webdriver.support.ui import Select
                select1 = Select(browser.find_element_by_id("wirelessMicPB1Function"))
                select1.select_by_visible_text(random.choice(option_list))

                select2 = Select(browser.find_element_by_id("wirelessMicPB2Function"))
                select2.select_by_visible_text(random.choice(option_list))

            elif conf in ["system", "System"]:
                element = WebDriverWait(browser, 10).until(
                    lambda x: x.find_element_by_id("system-target"))
                time.sleep(5)
                element.click()
                import random
                option_list1 = ['Default', 'DWI', 'SPD', 'STP', 'CAD']
                from selenium.webdriver.support.ui import Select
                select1 = Select(browser.find_element_by_id("defaultEvent"))
                select1.select_by_visible_text(random.choice(option_list1))

            elif conf in ["DS", "ds", "digsig"]:
                element = WebDriverWait(browser, 10).until(
                    lambda x: x.find_element_by_id("system-target"))
                time.sleep(5)
                element.click()
                import random
                option_list1 = ['MD5','SHA3-512']
                from selenium.webdriver.support.ui import Select
                select1 = Select(browser.find_element_by_id("digitalSignatureType"))
                select1.select_by_visible_text(random.choice(option_list1))

            elif conf in ["video", "Video", "vid"]:
                element = WebDriverWait(browser, 10).until(
                    lambda x: x.find_element_by_id("video-target"))
                time.sleep(5)
                element.click()
                import random
                #for sec in max_seconds(10):
                #    option_list1 = ['H.264','H.265']
                #    print("Inside first secs loop")
                #    from selenium.webdriver.support.ui import Select
                #    select1 = Select(browser.find_element_by_name("VideoCodec"))
                #    select1.select_by_visible_text(random.choice(option_list1))
                #    break
                for sec in max_seconds(15):
                    print("In the second loop")
                    option_list2 = ['SD-480p','HD-720p','HD-1080p']
                    from selenium.webdriver.support.ui import Select
                    select1 = Select(browser.find_elements_by_xpath("/html/body/c3-app/ng-component/div[1]/div/block-ui/c3-adm-navigator/div/ng-component/div/c3-adm-h1-template-edit/div/div/div[5]/c3-adm-h1-template-video-edit/div/form/div[2]/div/div[2]/table/tbody/tr[1]/td[2]/select"))
                    select1.select_by_visible_text(random.choice(option_list2))
                    option_list3 = ['SD-480p','HD-720p','HD-1080p']
                    from selenium.webdriver.support.ui import Select
                    select1 = Select(browser.find_element_by_name("Camera2Resolution"))
                    select1.select_by_visible_text(random.choice(option_list3))
                    option_list4 = ['SD-480p','HD-720p','HD-1080p']
                    from selenium.webdriver.support.ui import Select
                    select1 = Select(browser.find_element_by_name("Camera3Resolution"))
                    select1.select_by_visible_text(random.choice(option_list4))
                    break

            elif conf in ["wireless", "wire", "w"]:
                element = WebDriverWait(browser, 10).until(
                    lambda x: x.find_element_by_id("wireless-target"))
                time.sleep(5)
                element.click()
                import random
                from selenium.webdriver.support.ui import Select
                #####select1 = Select(browser.find_element_by_id("gpsSamplingFrequency"))
                #####select1.click()send_keys(random.randint(3, 20))
                #####browser.find_element_by_xpath(
                #####    "/html/body/c3-app/ng-component/block-ui/div/div[1]/div/c3-adm-navigator/div/ng-component/div/c3-adm-h1-template-edit/div/div/div[5]/c3-adm-h1-template-wireless-edit/div/form/div[2]/div/div[2]/div/div/div[5]/div[2]/span/input").clear()
                #####browser.find_element_by_xpath("/html/body/c3-app/ng-component/block-ui/div/div[1]/div/c3-adm-navigator/div/ng-component/div/c3-adm-h1-template-edit/div/div/div[5]/c3-adm-h1-template-wireless-edit/div/form/div[2]/div/div[2]/div/div/div[5]/div[2]/span/input").send_keys(random.randint(3, 20))
                #browser.find_element_by_id("gpsSamplingFrequence").send_keys(random.randint(3, 20))
            else:
                print("Couldn't find the options")
    except:
        print("Enterred Except catch block, something went wrong")
#Trigger Camera2 Recording
    browser.find_element_by_xpath("/html/body/c3-app/ng-component/div[1]/div/block-ui/c3-adm-navigator/div/ng-component/div/c3-adm-h1-template-edit/div/div/div[2]/input").click()

    time.sleep(20)
    browser.close()
