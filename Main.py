#!python3
# -*- coding: utf-8 -*-
"""
Main.py

The following is an attmpt to start to automate.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""

#--- CHANGES ----------------------------------------------------------------
#
#  2018/08/10 - hWin, initial layout
#


#--- IMPORTS ----------------------------------------------------------------
import logging

import hWin

import AutomationH1
import AutomationH01
import parse
import AutomationH2
import AutomationH27
import AutomationH3
import AutomationH4
import AutomationH5
import AutomationDES

def main1():
    DEBUG = False
    # DEBUG = True

    hWin.hWin_SetupLogging()
    logging.info("Logging module started...")

    if DEBUG:
        logging.debug('Debug something')
        logging.info('Info something')
        logging.warning('Warning something')
        logging.error('Error something')
    pass

    logging.info('***')
    logging.info('Starting H1 Automation...')
    AutomationH27.main()
    #AutomationDES.main()

def main2():
    DEBUG = False
    # DEBUG = True

    hWin.hWin_SetupLogging()
    logging.info("Logging module started...")

    if DEBUG:
        logging.debug('Debug something')
        logging.info('Info something')
        logging.warning('Warning something')
        logging.error('Error something')
    pass

    logging.info('***')
    logging.info('Starting H1 Automation...')
    AutomationH3.main()

def main():

    for i in range(0, 2, 1):
        if (i==0):

            DEBUG = False
            #DEBUG = True

            hWin.hWin_SetupLogging()
            logging.info("Logging module started...")

            if DEBUG:
                logging.debug('Debug something')
                logging.info('Info something')
                logging.warning('Warning something')
                logging.error('Error something')
            pass

            logging.info('***')
            logging.info('Starting H1 Automation...')
            AutomationH27.main()

        #elif (i==1):

        else:
            DEBUG = False
            # DEBUG = True

            hWin.hWin_SetupLogging()
            logging.info("Logging module started...")

            if DEBUG:
                logging.debug('Debug something')
                logging.info('Info something')
                logging.warning('Warning something')
                logging.error('Error something')
            pass

            logging.info('***')
            logging.info('Starting H1 Automation...')
            AutomationH3.main()

        #else:
        #    print("Done looping through Following number of DVRS:: ")
        #    print(i)
#
# Entry point for module
#
#if (__name__ == "__main__"):
#    main()
x=0
while (x<2):
    if (x==0):
        #main2()
        import time
        #time.sleep(1200)
        x+=1
    elif (x==1):
        main1()
        x+=1
    else:
        print("Done!")

'''
for i in range(0, 2, 1):
    if (i==0):
        print("first loop")
    elif (i==1):
        print("second loop")
    else:
        print("loop ended")
'''