import ERB24
ERB24_BOARD_NUM = 0


def IgnitionsOff():
    print("Turning off Ignitions...")
    for xx in range (8, 16):
        ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=xx)


#
# Entry point for module
#
if (__name__ == "__main__"):
    IgnitionsOff()
