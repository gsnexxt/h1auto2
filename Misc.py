#!python3
# -*- coding: utf-8 -*-
"""
Misc.py - hWin (Henry Witwicki)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""


__version__ = '0.99.04'
__date__    = '2019/08/01'
__author__  = 'Henry Witwicki'


#--- CHANGES ----------------------------------------------------------------
#
#  2018/11/28 - hWin, initial layout
#


#--- IMPORTS ----------------------------------------------------------------
import datetime
import names
import shutil
import time

import hWin

# Measurement and Computing ERB24 stuff
import ERB24
mutexERB24 = ""
ERB24_BOARD_NUM = 0
RELAY_01 = 0
RELAY_09 = 8
RELAY_16 = 15
RELAY_17 = 16
RELAY_24 = 23
# leaving room for 8 DVRs
nRelayOffset_Power = RELAY_01 - 1     # I am indexing the DVRs from 1, hence the "-1"s
nRelayOffset_Ignition = RELAY_09 - 1
nRelayOffset_Event1 = RELAY_16
nRelayOffset_Event2 = RELAY_17
nRelayOffset_WiFi = RELAY_24

from h1tcpclient import H1
jsonResponse = ""


def connect(h1, ip, port):
    return h1.connect(ip, port)

def getJsonResponse(h1):
    global jsonResponse
    time.sleep(0.200)
    h1.poll()
    jsonResponse = h1.serializedResponse()
    pass

def login(h1, officer, password, unit):
    h1.sendjson({"command":"login","officer":officer,"password":password,"partner":"","unit":unit})
    getJsonResponse(h1)
    pass

def startRecord(h1, camera):
    h1.sendjson({"command":"record","camera":camera})
    getJsonResponse(h1)
    pass

def stopRecord(h1, camera):
    h1.sendjson({"command":"stoprecord","camera":camera})
    getJsonResponse(h1)
    pass

def getStatus(h1):
    h1.sendjson({"command":"status"})
    getJsonResponse(h1)
    pass

def getPendingEventList(h1):
    h1.sendjson({"command":"pendingeventlist"})
    getJsonResponse(h1)
    pass

def sendModifyEvent(h1, eventName, eventID):
    h1.sendjson({"command":"modifyevent","eventname" : eventName,
                 "event":{
                     "event_id":eventID,
                     "case":"",
                     "case2":"",
                     "dispatch":"",
                     "dispatch2":"",
                     "ticket":"",
                     "ticket2":"",
                     "first_name":"",
                     "last_name":"",
                     "location":"",
                     "notes":""
                 }})
    getJsonResponse(h1)
    pass
#end of Sid's stubs


def SSDTest():
    """The following is to provide a simple test for the SSD Drive(s)."""
    global jsonResponse
    nCount = 0
    nStepDelay = 0.25

    h1 = H1()
    h2 = H1()
    h3 = H1()
    h4 = H1()
    h5 = H1()
    h6 = H1()
    h7 = H1()
    h8 = H1()

    DEBUG = False
    #DEBUG = True

    print ("Powering off...")
    ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+1)
    ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+2)
    ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+3)
    ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+4)
    ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+5)
    ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+6)
    ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+7)
    ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+8)
    #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+1)
    #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+2)
    #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+3)
    #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+4)
    #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+5)
    #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+6)
    #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+7)
    #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+8)
    time.sleep(60)

    while True:
        print ("Powering on...")
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+1)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+2)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+3)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+4)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+5)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+6)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+7)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+8)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+1)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+2)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+3)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+4)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+5)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+6)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+7)
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+8)
        time.sleep(80)     # need to give the H1 a chance to boot to the login screen

        print ("Trying to connect...")
        resp = connect(h1, "172.18.4.170", "9999")
#        resp = connect(h2, "172.30.69.12", "9999")
#        resp = connect(h3, "172.30.69.13", "9999")
#        resp = connect(h4, "172.30.69.14", "9999")
#        resp = connect(h5, "172.30.69.15", "9999")
#        resp = connect(h6, "172.30.69.16", "9999")
#        resp = connect(h7, "172.30.69.17", "9999")
#        resp = connect(h8, "172.30.69.18", "9999")
        h1.sendjson({"command":"init"})
        h2.sendjson({"command":"init"})
        h3.sendjson({"command":"init"})
        h4.sendjson({"command":"init"})
        h5.sendjson({"command":"init"})
        h6.sendjson({"command":"init"})
        h7.sendjson({"command":"init"})
        h8.sendjson({"command":"init"})
        time.sleep(5)

        print ("Trying to login...")

        # new login logic
        nDVRs = [1, 2, 3, 4, 5, 6, 7, 8]
        for xx in nDVRs:
            print(str(xx))
            nMaxTrys = 5
            nTrys = 0
            while True:
                nTrys += 1
                if nTrys > nMaxTrys:
                    print("DVR%d: Unable to login to the DVR!" % (xx))
                    break
                if xx == 1:
                    getStatus(h1)
                if xx == 2:
                    getStatus(h2)
                if xx == 3:
                    getStatus(h3)
                if xx == 4:
                    getStatus(h4)
                if xx == 5:
                    getStatus(h5)
                if xx == 6:
                    getStatus(h6)
                if xx == 7:
                    getStatus(h7)
                if xx == 8:
                    getStatus(h8)
                time.sleep(nStepDelay)
                if jsonResponse["command"] != "status":     # get the status message
                    continue
                if jsonResponse["login"] == False:
                    print("DVR%d: Logging in..." % (xx))
                    if xx == 1:
                        login(h1, "Rack1", "6666", "SeonRack1")
                    if xx == 2:
                        login(h2, "Rack2", "6666", "SeonRack2")
                    if xx == 3:
                        login(h3, "Rack3", "6666", "SeonRack3")
                    if xx == 4:
                        login(h4, "Rack4", "6666", "SeonRack4")
                    if xx == 5:
                        login(h5, "Rack5", "6666", "SeonRack5")
                    if xx == 6:
                        login(h6, "Rack6", "6666", "SeonRack6")
                    if xx == 7:
                        login(h7, "Rack7", "6666", "SeonRack7")
                    if xx == 8:
                        login(h8, "Rack8", "6666", "SeonRack8")
                    time.sleep(1)
                    continue
                elif jsonResponse["login"] == True:
                    # good to go
                    break
        pass
        #end 

        # I may need to add the dreaded Poll here.
        time.sleep(20)

        print ("Trying to logout...")
        h1.sendjson({"command":"logout"})
        h2.sendjson({"command":"logout"})
        h3.sendjson({"command":"logout"})
        h4.sendjson({"command":"logout"})
        h5.sendjson({"command":"logout"})
        h6.sendjson({"command":"logout"})
        h7.sendjson({"command":"logout"})
        h8.sendjson({"command":"logout"})
        time.sleep(5)
        #h1.sendjson({"command":"shutdown"})
        #h2.sendjson({"command":"shutdown"})
        #h3.sendjson({"command":"shutdown"})
        #h4.sendjson({"command":"shutdown"})
        #h5.sendjson({"command":"shutdown"})
        #h6.sendjson({"command":"shutdown"})
        #h7.sendjson({"command":"shutdown"})
        #h8.sendjson({"command":"shutdown"})
        time.sleep(5)
        print ("Powering off...")
        ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+1)
        ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+2)
        ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+3)
        ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+4)
        ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+5)
        ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+6)
        ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+7)
        ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition+8)
        #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+1)
        #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+2)
        #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+3)
        #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+4)
        #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+5)
        #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+6)
        #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+7)
        #ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+8)
        h1.disconnect()
        h2.disconnect()
        h3.disconnect()
        h4.disconnect()
        h5.disconnect()
        h6.disconnect()
        h7.disconnect()
        h8.disconnect()
        nCount += 1
        print("nCount = %d" % (nCount))
        time.sleep(20)
    pass
#end of SSDTest()


def USBTest():
    h1 = H1()
    h2 = H1()
    h3 = H1()
    h4 = H1()
    global mutexH1

    DEBUG = False
    DEBUG = True

    nCounter = 0
    bRecording = False
    resp = connect(h1, "172.30.69.11", "9999")
    time.sleep(0.250)
    h1.sendjson({"command":"init"})
    resp = connect(h2, "172.30.69.12", "9999")
    time.sleep(0.250)
    h2.sendjson({"command":"init"})
    resp = connect(h3, "172.30.69.13", "9999")
    time.sleep(0.250)
    h3.sendjson({"command":"init"})
    resp = connect(h4, "172.30.69.14", "9999")
    time.sleep(0.250)
    h4.sendjson({"command":"init"})

    while True:
        getStatus(h1)
        #time.sleep(0.250)
        getStatus(h2)
        #time.sleep(0.250)
        getStatus(h3)
        #time.sleep(0.250)
        getStatus(h4)
        #time.sleep(0.250)
        if nCounter % 60 == 0:
            if not bRecording:
                startRecord(h1, 0)
                #time.sleep(0.250)
                startRecord(h2, 0)
                #time.sleep(0.250)
                startRecord(h3, 0)
                #time.sleep(0.250)
                startRecord(h4, 0)
                #time.sleep(0.250)
                bRecording = True
            else:
                bRecording = False

                if True:
                    stopRecord(h1,0)
                    time.sleep(2)
                    getPendingEventList(h1)
                    time.sleep(2)
                    if not jsonResponse['events']:
                        getPendingEventList(h1)
                        time.sleep(2)
                    numEvents = len(jsonResponse['events'])
                    sendModifyEvent(h1,jsonResponse['events'][0],"0")
                if True:
                    stopRecord(h2,0)
                    time.sleep(2)
                    getPendingEventList(h2)
                    time.sleep(2)
                    if not jsonResponse['events']:
                        getPendingEventList(h2)
                        time.sleep(2)
                    numEvents = len(jsonResponse['events'])
                    sendModifyEvent(h2,jsonResponse['events'][0],"0")
                if True:
                    stopRecord(h3,0)
                    time.sleep(2)
                    getPendingEventList(h3)
                    time.sleep(2)
                    if not jsonResponse['events']:
                        getPendingEventList(h3)
                        time.sleep(2)
                    numEvents = len(jsonResponse['events'])
                    sendModifyEvent(h3,jsonResponse['events'][0],"0")
                if True:
                    stopRecord(h4,0)
                    time.sleep(2)
                    getPendingEventList(h4)
                    time.sleep(2)
                    if not jsonResponse['events']:
                        getPendingEventList(h4)
                        time.sleep(2)
                    numEvents = len(jsonResponse['events'])
                    sendModifyEvent(h4,jsonResponse['events'][0],"0")
        else:
            pass

        time.sleep(1)
        nCounter += 1
#end USBTest()


def USBSessionBuilder():
    """    The purpose is to fill a H1 USB stick with sample session data files."""

    # The following code was based on an e-mail I received from Douglas Foulds.
    #
    # My concern was that I want to ensure that no performance degradation
    #   occurs on the H1 because of the number of files on the USB stick and
    #   the manner in which they are stored.
    #
    # Where there are:
    #    nSessions per shift,
    #    nShifts per day, for
    #    nDays
    #

    DEBUG = False
    DEBUG = True

    # Edit the following to set the number of sesions in total to generate etc.
    ########################################################
    nTotalSessions = 504
    if DEBUG:
        nTotalSessions = 20
    strSrcPath = "C:\\Work\\CobanH1\\SampleFiles\\"
    strDestPath = "D:\\cobanvideos\\"
    if DEBUG:
        strDestPath = "C:\\Temp\\Output\\"
    strUnitID = "SeonRack9"
    nOfficerID = 3     # user "hWin" on H106
    ########################################################

    # The following is based on Douglas Foulds worst case estimates for a H1
    nShifts = 2        # shifts per day
    nSnapshots = 2     # I am adding some snapsots per shift because I can ;-)
    nSessions = 50     # sessions per shift
    nCameras = 1       # number of cameras installed

    # stats
    nSessionsCopied = 0
    nFilesCopied = 0

    dStartDate = datetime.datetime.now() + datetime.timedelta(days = -2)     # I don't want to overwrite any recent existing files
    strTime = '{:%Y%m%d%H%M%S}'.format(dStartDate)


    nH1LogVersion = 1     # the iteration of the log files created by the H1
    if 1:
        strVersion = "v1.0.8.15"
        if strVersion > "v1.0.5.00":     # nH1LogVersion = 2
            nH1LogVersion = 2
            print("nH1LogVersion = 2")
        if strVersion > "v1.0.7.00":     # nH1LogVersion = 3
            nH1LogVersion = 3
            print("nH1LogVersion = 3")
        if strVersion > "v1.0.8.11":     # nH1LogVersion = 4
            nH1LogVersion = 4
            print("nH1LogVersion = 4")
        if strVersion > "v2.0.0.01":     # nH1LogVersion = 5
            nH1LogVersion = 5
            print("nH1LogVersion = 5")
        else:                            # nH1LogVersion = 5
            nH1LogVersion = 1
            print("nH1LogVersion = 1")

    while nSessionsCopied < nTotalSessions:
        print("Writing the days files...")
        start = time.time()

        # files per day

        dStartDate += datetime.timedelta(-1)     # go back 1 day
        dStartDate = dStartDate.replace(hour=6, minute=0, second=0)     # start the day at 06:00:00
        strTime = '{:%Y%m%d}'.format(dStartDate)

        src = strSrcPath + "dummy.CobanH1"
        if nH1LogVersion > 1:
            dest = strDestPath + ("%s_CobanH1.%s.log" % (strUnitID, strTime))
        else:
            dest = strDestPath + ("CobanH1.%s" % (strTime))
        shutil.copy(src, dest)
        nFilesCopied += 1

        src = strSrcPath + "dummy.ok"
        if nH1LogVersion > 1:
            dest = strDestPath + ("%s_CobanH1.%s.log.ok" % (strUnitID, strTime))
        else:
            dest = strDestPath + ("CobanH1.%s.ok" % (strTime))
        shutil.copy(src, dest)
        nFilesCopied += 1

        src = strSrcPath + "dummy.syslog"
        if nH1LogVersion > 1:
            dest = strDestPath + ("%s_syslog.%s.log" % (strUnitID, strTime))
        else:
            dest = strDestPath + ("syslog.%s" % (strTime))
        shutil.copy(src, dest)
        nFilesCopied += 1
        src = strSrcPath + "dummy.ok"
        if nH1LogVersion > 1:
            dest = strDestPath + ("%s_syslog.%s.log.ok" % (strUnitID, strTime))
        else:
            dest = strDestPath + ("syslog.%s.ok" % (strTime))
        shutil.copy(src, dest)
        nFilesCopied += 1

        # new log file(s) added circa v1.0.5.x
        if nH1LogVersion == 2 or nH1LogVersion == 3:
            src = strSrcPath + "dummy.console"
            dest = strDestPath + ("%s_console.%s.log" % (strUnitID, strTime))
            shutil.copy(src, dest)
            nFilesCopied += 1
            src = strSrcPath + "dummy.ok"
            dest = strDestPath + ("%s_console.%s.log.ok" % (strUnitID, strTime))
            shutil.copy(src, dest)
            nFilesCopied += 1

            src = strSrcPath + "dummy.console"
            dest = strDestPath + ("zzzz_console.%s.log" % (strTime))
            shutil.copy(src, dest)
            nFilesCopied += 1
            src = strSrcPath + "dummy.ok"
            dest = strDestPath + ("zzzz_console.%s.log.ok" % (strTime))
            shutil.copy(src, dest)
            nFilesCopied += 1

        # Unitid_ files
        if nH1LogVersion >= 3 :
            src = strSrcPath + "dummy.CobanH1"
            dest = strDestPath + ("UnitID_CobanH1.%s.log" % (strTime))
            shutil.copy(src, dest)
            nFilesCopied += 1
            src = strSrcPath + "dummy.ok"
            dest = strDestPath + ("UnitID_CobanH1.%s.log.ok" % (strTime))
            shutil.copy(src, dest)
            nFilesCopied += 1

        if nH1LogVersion == 3 :
            src = strSrcPath + "dummy.console"
            dest = strDestPath + ("UnitID_console.%s.log" % (strTime))
            shutil.copy(src, dest)
            nFilesCopied += 1
            src = strSrcPath + "dummy.ok"
            dest = strDestPath + ("UnitID_console.%s.log.ok" % (strTime))
            shutil.copy(src, dest)
            nFilesCopied += 1

        if nH1LogVersion >= 3 :
            src = strSrcPath + "dummy.syslog"
            dest = strDestPath + ("UnitID_syslog.%s.log" % (strTime))
            shutil.copy(src, dest)
            nFilesCopied += 1
            src = strSrcPath + "dummy.ok"
            dest = strDestPath + ("UnitID_syslog.%s.log.ok" % (strTime))
            shutil.copy(src, dest)
            nFilesCopied += 1

        # files per shift
        for shift in range(0, nShifts):     # add files per shift
            strTime = '{:%Y%m%d%H%M%S}'.format(dStartDate)
            src = strSrcPath + "dummy.c"
            if nH1LogVersion > 3:
                dStartDate2 = dStartDate + datetime.timedelta(0, 0, 0, 0, 0, 1)     # offset 1sec/session
                strTime2 = '{:%Y%m%d%H%M%S}'.format(dStartDate2)

                dest = strDestPath + ("%d@%s.c" % (nOfficerID, strTime2))
                shutil.copy(src, dest)
                nFilesCopied += 1

                src = strSrcPath + "dummy.ok"
                dest = strDestPath + ("%d@%s.ok" % (nOfficerID, strTime2))
                shutil.copy(src, dest)
                nFilesCopied += 1
            else:
                dest = strDestPath + ("%d@%s.c" % (nOfficerID, strTime))
                shutil.copy(src, dest)
                nFilesCopied += 1

            src = strSrcPath + "dummy.d"
            dest = strDestPath + ("%d@%s.d" % (nOfficerID, strTime))
            shutil.copy(src, dest)
            nFilesCopied += 1

            src = strSrcPath + "dummy.ok"
            dest = strDestPath + ("%d@%s.ok" % (nOfficerID, strTime))
            shutil.copy(src, dest)
            nFilesCopied += 1

            for jpg in range (0, nSnapshots):     # add snapshot(s) per shift
                src = strSrcPath + "dummy.jpg"
                dest = strDestPath + ("%d@%s-0-%03d.jpg" % (nOfficerID, strTime, jpg))
                shutil.copy(src, dest)
                nFilesCopied += 1

                src = strSrcPath + "dummy.ok"
                dest = strDestPath + ("%d@%s-0-%03d.ok" % (nOfficerID, strTime, jpg))
                shutil.copy(src, dest)
                nFilesCopied += 1

            # files per session
            for session in range (0, nSessions):
                nSessionsCopied += 1
                if  nSessionsCopied > nTotalSessions:
                    break

                # add files per session

                dStartDate += datetime.timedelta(0, 0, 0, 0, 10)     # offset 10min/session
                strTime = '{:%Y%m%d%H%M%S}'.format(dStartDate)

                for camera in range (0, nCameras):     
                    src = strSrcPath + "dummy.a"
                    dest = strDestPath + ("%d@%s-%1d.a" % (nOfficerID, strTime, camera))
                    shutil.copy(src, dest)
                    nFilesCopied += 1

                    src = strSrcPath + "dummy.l"
                    dest = strDestPath + ("%d@%s-%1d.l" % (nOfficerID, strTime, camera))
                    shutil.copy(src, dest)
                    nFilesCopied += 1

                    src = strSrcPath + "dummy.mp4"
                    dest = strDestPath + ("%d@%s-%1d.mp4" % (nOfficerID, strTime, camera))
                    shutil.copy(src, dest)
                    nFilesCopied += 1

                    src = strSrcPath + "dummy.v"
                    dest = strDestPath + ("%d@%s-%1d.v" % (nOfficerID, strTime, camera))
                    shutil.copy(src, dest)
                    nFilesCopied += 1

                    src = strSrcPath + "dummy.ok"
                    dest = strDestPath + ("%d@%s-%1d.ok" % (nOfficerID, strTime, camera))
                    shutil.copy(src, dest)
                    nFilesCopied += 1

                pass     # per session
            pass     # per shift
        end = time.time()
        print("%d sec. to write days files" % (end-start))
        pass     # per day
    bob = 1
    print("Done, %d sessions, %d files copied." % (nSessionsCopied, nFilesCopied))
    #end while

#end USBSessionBuilder()


def GPSFilter():
    Debug = False

    # trigger speed
    fTriggerSpeed = 1.0     # Knots

    infile = open('C:\\Temp\\GPS\\GPSRaw.txt', 'r')
    outfileGPRMC = open('C:\\Temp\\GPS\\GPRMC.txt', 'w')
    outfileGPVTG = open('C:\\Temp\\GPS\\GPVTG.txt', 'w')
    outfileGPRMC_V = open('C:\\Temp\\GPS\\GPRMC_V.txt', 'w')
    outfileGPRMC_N = open('C:\\Temp\\GPS\\GPRMC_N.txt', 'w')
    outfileGPVTG_N = open('C:\\Temp\\GPS\\GPVTG_N.txt', 'w')
    outfileGPRMCSpeed = open('C:\\Temp\\GPS\\GPRMCSpeed.txt', 'w')
    outfileGPVTGSpeed = open('C:\\Temp\\GPS\\GPVTGSpeed.txt', 'w')

    cnt = 0
    cntGPRMC = 0
    cntGPVTG = 0
    nBadGPRMC = 0
    nBadGPVTG = 0
    nLastTime = 0
    nTransition = 0
    nSpeedWatermark = 0
    bLastValid = False
    for line in infile:
        if len(line) < 10:
            continue
        cnt += 1

        # strip the Tera Term ts
        #message = line[31:]
        bob = line.split('$')
        if len(bob) < 2:
            continue
        message = '$' + bob[1]

        # filter based on message type
        split = message.split(',')

        if split[0] == "$GPRMC":
            #print("GPRMC")
            cntGPRMC += 1
            if len(split) < 13:
                nBadGPRMC += 1
                continue
            if split[2] == 'A':
                bLastValid = True
            if split[2] != 'A':     # record is not marked as valid
                outfileGPRMC_V.write(str(line))
                if bLastValid:
                    nTransition += 1
                    bLastValid = False
                continue
            eXtra = split[12]
            eXtraSplit = eXtra.split('*')
            if eXtraSplit[0] == 'N':     # filter out the 'N' records
                outfileGPRMC_N.write(str(line))
                continue
            speed = float(split[7])
            nSpeedWatermark = max(nSpeedWatermark, speed)
            if speed > fTriggerSpeed:
                outfileGPRMCSpeed.write(str(line))
            outfileGPRMC.write(str(line))

        elif split[0] == "$GPVTG":
            #print("GPVTG")
            cntGPVTG += 1
            if len(split) < 10:
                nBadGPVTG += 1
                continue
            eXtra = split[9]
            eXtraSplit = eXtra.split('*')
            if eXtraSplit[0] == 'N':     # filter out the 'N' records
                outfileGPVTG_N.write(str(line))
                continue
            speed = float(split[7])
            if speed > fTriggerSpeed * 1.852:     # Knots to km/h.
                outfileGPVTGSpeed.write(str(line))
            outfileGPVTG.write(str(line))

        if 0:
            if cnt > 100:
                break

    print("Total=%d, GPRMC=%d, GPVTG=%d, LostLock=%d, MaxSpeed=%4.2f" %(cnt, cntGPRMC, cntGPVTG, nTransition, nSpeedWatermark))
    infile.close()
    outfileGPRMC.close()
    outfileGPVTG.close()
    outfileGPRMC_V.close()
    outfileGPRMC_N.close()
    outfileGPVTG_N.close()
    outfileGPRMCSpeed.close()
    outfileGPVTGSpeed.close()
#end of GPSFilter()


def mkUnits():
    print ("Starting units.config...")
    nMaxCount = 1000
    strSrcPath = "C:\\Work\\CobanH1\\SampleFiles\\"

    outFile = open(strSrcPath + 'units.config', 'w')

    outFile.write("<units>\n")

    strTemp = '''<row unit_id="996" unit_name="raj" description="raj's unit" flag="1" unit_ip="" />'''

    for xx in range(10, nMaxCount + 10):

        if xx == 996:
            outFile.write(strTemp + '\n')
            continue

        outFile.write('<row')
        outFile.write(' unit_id="' + str(xx)+ '"')
        myName = names.get_first_name(gender="female")
        outFile.write(' unit_name="'+ myName + str(xx) + '"')
        outFile.write(' description="' + str(xx) + myName + '"')
        outFile.write(' flag="1"')
        outFile.write(' unit_ip=""')
        outFile.write(' />\n')

        if xx % 100 == 0:
            print (xx)

    outFile.write("</units>\n")

    outFile.close()
#end of mkUnits()


def mkUsers():
    print ("Starting users.config...")
    nMaxCount = 10000
    strSrcPath = "C:\\Work\\CobanH1\\SampleFiles\\"

    strTemp = '''<row user_id="9996" first_name="raj1" last_name="seon" officer_id="raj1" password_officer="pmWkWSBCL51Bfkhn79xPuKBKHz//H6B+mY6G9/eieuM=" password_standard="" flag="1" failsafeall="0" failsafeown="1" />'''

    outFile = open(strSrcPath + 'users.config', 'w')

    outFile.write("<users>\n")

    for xx in range(10, nMaxCount + 10):

        if xx == 9996:
            outFile.write(strTemp + '\n')
            continue

        myNameFirst = names.get_first_name()
        myNameLast = names.get_last_name()

        outFile.write('<row')
        outFile.write(' user_id="' + str(xx) + '"')
        outFile.write(' first_name="' + myNameFirst + '"')
        outFile.write(' last_name="' + myNameLast + '"')
        outFile.write(' officer_id="' + myNameFirst + str(xx) + '"')
        outFile.write(' password_officer="pmWkWSBCL51Bfkhn79xPuKBKHz//H6B+mY6G9/eieuMX"')
        outFile.write(' password_standard=""')
        outFile.write(' flag="1"')
        outFile.write(' failsafeall="0"')
        outFile.write(' failsafeown="1"')
        outFile.write(' />\n')

        if xx % 100 == 0:
            print (xx)

    outFile.write("</users>\n")

    outFile.close()
#end of mkUsers()


#
# Entry point for module
#
if (__name__ == "__main__"):
    SSDTest()
    #USBTest()
    #USBSessionBuilder()
    #GPSFilter()
    #mkUnits()
    #mkUsers()
    print ("Done!")

