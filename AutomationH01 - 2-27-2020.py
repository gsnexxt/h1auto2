# !python3
# -*- coding: utf-8 -*-
"""
AutomationH1.py - hWin (Henry Witwicki)

The following is an attmpt to start to automate.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""

__version__ = '1.06.0029'
__date__ = '2019/09/17'
__author__ = 'Henry Witwicki'

# --- CHANGES ----------------------------------------------------------------
#
#  2018/08/10 - hWin, initial layout
#  2018/08/11 - hWin, WIP
#  2018/12/12 - hWin, rewrite recording logic based on Sid's review
#  2019/01/04 - hWin, minor changes from unit testing
#  2019/02/06 - hWin, added Snapshots and Bookmaarks
#  2019/02/15 - hWin, added reversing camera order for start/stop record
#  2019/02/20 - hWin, added ERB24 port to aDVRs to acconut for rack partial runs
#  2019/03/12 - hWin, better handling for jsonResponse["login"] and other corrections
#  2019/03/19 - hWin, better handling of message types and simpler retry logic
#  2019/03/25 - hWin, added logic for stopping record based on camera status
#  2019/04/10 - hWin, fix to using numeric EvnetId for CC Server v2.4.5+
#  2019/05/30 - hWin, better handling of jsonResponse
#  2019/08/13 - hWin, rewrote RecordSession() to catch more problems
#  2019/08/14 - hWin, general improvements/fixes...
#  2019/08/23 - hWin, added try/catch to RecordSession() to deel with nLoops
#  2019/09/11 - Sid, added GetICVFileNames() and GetLogFile() etc.
#  2019/09/12 - hWin, clean-up of new code for Sid-ification
#  2019/09/17 - hWin, added some more retry loop logic to RecordSession()
#


# --- IMPORTS ----------------------------------------------------------------
import datetime
import json  # python3  -m  pip  install  jsonlib-python3
import logging
import numpy  # python3  -m  pip  install  numpy
import pprint
import random
import threading
import time
import Update1
import Update
import Check
import Check1
import hWin
import parse

# ERB24 stuff from Measurement and Computing
import ERB24

ERB24_BOARD_NUM = 0
RELAY_01 = 0  # relays are zero based
RELAY_09 = 8
RELAY_17 = 16
# leaving room for 8 DVRs
nRelayOffset_Power = RELAY_01
nRelayOffset_Ignition = RELAY_09
nRelayOffset_DigitalIO = RELAY_17

from h1tcpclient import H1, Status, GetEvent

# Threadsafe shared resources
mutexERB24 = threading.Lock()
mutexH1 = threading.Lock()
mutexReportFile = threading.Lock()

aDVRs = []

nStepDelay = 0.350  # number of seconds to pause between H1 interactions
nPollPeriod = 5.0  # seconds, time between polls
jsonResponse = ""  # not threadsafe so will handle with the mutexH1 method


# Sid has provided the following stubs...
def _connect(h1, ip, port):
    return h1.connect(ip, port)


def _getJsonResponse(h1):
    global jsonResponse
    time.sleep(0.200)
    h1.poll()
    jsonResponse = h1.serializedResponse()
    pass


def _login(h1, officer, password, unit):
    h1.sendjson({"command": "login", "officer": officer, "password": password, "partner": "", "unit": unit})
    _getJsonResponse(h1)
    pass


def _emlogin(h1, officer, password, unit):
    h1.sendjson({"command": "login", "officer": officer, "password": password, "partner": "", "unit": unit})
    _getJsonResponse(h1)
    pass


def _logout(h1):
    h1.sendjson({"command": "logout"})
    _getJsonResponse(h1)
    pass

def _restart(h1):
    h1.sendjson({"command": "restart"})
    _getJsonResponse(h1)
    pass

def _startRecord(h1, camera):
    h1.sendjson({"command": "record", "camera": camera})
    _getJsonResponse(h1)
    pass


def _stopRecord(h1, camera):
    h1.sendjson({"command": "stoprecord", "camera": camera})
    _getJsonResponse(h1)
    pass


def _getStatus(h1):
    h1.sendjson({"command": "status"})
    _getJsonResponse(h1)
    pass


def _getPendingEventList(h1):
    h1.sendjson({"command": "pendingeventlist"})
    _getJsonResponse(h1)
    pass


def _sendModifyEvent(h1, eventName, eventID):
    h1.sendjson({"command": "modifyevent", "eventname": eventName,
                 "event": {
                     "event_id": eventID,
                     "case": "",
                     "case2": "",
                     "dispatch": "",
                     "dispatch2": "",
                     "ticket": "",
                     "ticket2": "",
                     "first_name": "",
                     "last_name": "",
                     "location": "",
                     "notes": ""
                 }})
    _getJsonResponse(h1)
    pass


def _bookmark(h1, camera):
    h1.sendjson({"command": "bookmark", "camera": camera})
    _getJsonResponse(h1)
    pass


def _snapshot(h1, camera):
    h1.sendjson({"command": "snapshot", "camera": camera})
    _getJsonResponse(h1)
    pass


def GetLogFile(h1, unitName):
    bytesRead = 0
    # strFileName = "//var//log//" + unitName + "_CobanH1.log"
    strFileName = "//var//log//" + unitName + ".log"
    # strFileName = "//var//log//" + unitName + "_installUpdates.log"
    h1.sendjson({"command": "readfile", "filename": strFileName})
    time.sleep(nPollPeriod)
    retval = ReceiveBinaryResponse(h1, bytesRead)
    return retval


def GetMetadataFile(h1, unitName):
    bytesRead = 0
    strFileName = "//home//ubuntu//Metadata//" + unitName + "_metadata.txt"
    # strFileName = "//var//log//" + unitName + "_installUpdates.log"
    h1.sendjson({"command": "readfile", "filename": strFileName})
    time.sleep(nPollPeriod)
    retval = ReceiveBinaryResponse(h1, bytesRead)
    return retval


def GetConfigFile(h1, unitName):
    bytesRead = 0
    strFileName = "//home//ubuntu//h1_exe//config//template.config"
    # strFileName = "//var//log//" + unitName + "_installUpdates.log"
    h1.sendjson({"command": "readfile", "filename": strFileName})
    time.sleep(nPollPeriod)
    retval = ReceiveBinaryResponse(h1, bytesRead)
    return retval


def GetStats(h1, unitName):
    bytesRead = 0
    h1.sendjson({"command": "version"})
    time.sleep(nPollPeriod)
    retval = ReceiveBinaryResponse(h1, bytesRead)
    print(retval)

    h1.sendjson({"command": "gps"})
    time.sleep(nPollPeriod)
    retval = ReceiveBinaryResponse(h1, bytesRead)
    print(retval)

    # h1.sendjson({"command": "network"})
    # time.sleep(nPollPeriod)
    # retval = ReceiveBinaryResponse(h1, bytesRead)
    # print(retval)
    # return retval


# end GetStats

def ReceiveBinaryResponse(h1, byteRead):
    retval = h1.receiveBinaryData()
    return retval


def GetFileNames(h1, path, filter):
    h1.sendjson({"command": "ls", "path": path, "filters": filter, "sort": "time"})
    time.sleep(nPollPeriod)
    retval = h1.ReceiveStringResponse();
    # for file in retval['files']:
    # print(file)
    return retval


def GetMeta(h1, unitName):
    h1.sendjson({"command": "3v"})
    time.sleep(nPollPeriod)
    retval = h1.ReceiveStringResponse();
    # for file in retval['files']:
    # print(file)
    return retval


def _getPendingEventList(h1):
    h1.sendjson({"command": "pendingeventlist"})
    _getJsonResponse(h1)
    pass


def GetFilePaths(h1):
    h1.sendjson({"command": "paths"})
    time.sleep(nPollPeriod)
    retval = h1.ReceiveStringResponse();
    # print('H1 cache Path =', retval['cache'])
    # print('H1 failsafe Path =', retval['failsafe'])
    # print('H1 focus_x1 Path =', retval['focus_x1'])
    # print('H1 snapshot Path =', retval['snapshot'])
    # print('H1 videos Path =', retval['videos'])
    # print('H1 xml Path =', retval['xml'])
    # print('H1 xmlfirst Path =', retval['xmlfirst'])
    # retval = GetFileNames(h1, retval['videos'], 9)
    return retval


def GetICVFileNames(h1):
    Paths = GetFilePaths(h1)
    retval = GetFileNames(h1, Paths['videos'], ['*.mp4'])
    # for file in retval['files']:
    # print(file)
    return retval


# end of Sid's stubs
def sid_Test():
    """The following was used to test Jonathan's H1 object."""
    h1 = H1()
    global jsonResponse

    resp = _connect("10.18.21.8", "9999")
    if not resp:
        exit()

    h1.setpollstatus(True)

    # if not logged in then login
    # need to repeat if login falis??
    _getStatus(h1)
    if not jsonResponse["login"]:
        _login(h1, "Bench1", "6666", "HenryBench1")

    # start record you could cylce thru 0 to installed cameras
    # this is available from the config file
    _startRecord(h1, 0)

    # record for num seconds
    time.sleep(5.0)

    # stop record
    _stopRecord(h1, 0)

    # get the pending event list
    _getPendingEventList(h1)

    # number of events to modify
    numEvents = len(jsonResponse['events'])

    # send the modify event for each eventName
    i = 0
    while i < numEvents:
        _sendModifyEvent(h1, jsonResponse['events'][i], "0")
        i += 1

    # time.sleep(10.0)

    # disconnect
    h1.disconnect()
# end of sid_Test()

def Gs_Test1(nDVR):
    DEBUG = False
    #DEBUG = True

    global aDVRs
    global mutexERB24
    global ERB24_BOARD_NUM
    global nRelayOffset_Ignition
    global nRelayOffset_Power

    logging.info("GSNEXT: Syncing the DVR For LATEST SETTINGS")
#    Update1.Update()
    logging.info("GSNEXT: **** \n **** TESTCASE 1 COMPLETED : TESTED OK **** \n **** CONFIGURATION UPDATE DONE")
    SyncDVR(nDVR)
    logging.info("GSNEXT: **** \n **** TESTCASE 2 COMPLETED : TESTED OK **** \n **** Syncing DONE")
    #Gs_Restart()
    # exit()
    REST_TIME_MIN = 5 * 60  # 5 min
    REST_TIME_MAX = 30 * 60  # 30 min
    if 1:
        REST_TIME_MIN = 5
        REST_TIME_MAX = 30

    logging.debug('DVR%d: GSNEXT Thread:%s has started...' % (nDVR, threading.currentThread().getName()))

    lHeadings = aDVRs[0].tolist()  # build a set of keys to the values
    nERB24Port = int(aDVRs[nDVR][lHeadings.index('ERB24')])

    mutexERB24.acquire()
    ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power + nERB24Port)
    mutexERB24.release()
    logging.info("DVR%d: Power relay On" % (nDVR))

    # build event times based on local time
    dIgnitionOnTime = datetime.datetime.strptime('07:00', '%H:%M')
    dICVSyncTime = datetime.datetime.strptime('20:00', '%H:%M')
    dIgnitionOffTime = datetime.datetime.strptime('21:30', '%H:%M')
    dHouseKeepingTime = datetime.datetime.strptime('23:55', '%H:%M')
    if 0:
        dIgnitionOnTime = datetime.datetime.strptime('06:00', '%H:%M')
        dICVSyncTime = datetime.datetime.strptime('07:30', '%H:%M')
        dIgnitionOffTime = datetime.datetime.strptime('21:30', '%H:%M')

    bICVSyncDone = True
    nSleep = 6
    bStartup = False
    dNow = datetime.datetime.now()
    dStopTime = dNow.replace(minute=(dNow.minute + 3))
    dStop = dNow.replace(minute=(dNow.minute + 4))
    while dNow < dStop:
        logging.debug("At the 1st while loop")
        while dNow < dStop:  # I am doing this as an unnecessary while loop in case I need to break out of it... via poor mans's GOTO

            if DEBUG: logging.debug("01")
            nSleepCounter = 6
            logging.info("DVR%d: GSNEXT Sleeping %dsec before next run..." % (nDVR, nSleepCounter))

            # the following lines will simply add some stagger to the threads
            time.sleep(nSleepCounter % nSleep)
            nSleepCounter -= nSleepCounter % nSleep

            while nSleepCounter > 0:
                #                dNow = datetime.datetime.now()
                #                dStopTime = dNow.replace(minute= (dNow.minute + 1))
                if dNow.time() > dHouseKeepingTime.time():
                    logging.info("DVR%d: HouseKeeping session started..." % (nDVR))
                    HouseKeeping(nDVR)
                    logging.info("DVR%d: HouseKeeping session done" % (nDVR))
                    break
                time.sleep(nSleep)
                nSleepCounter -= nSleep
                if not nSleepCounter % 60:
                    logging.debug("DVR%d: Sleeping %dsec before next time check..." % (nDVR, nSleepCounter))

            # get the state of the ignition
            if DEBUG: logging.debug("02")
            bIgnition = True
            dNow = datetime.datetime.now()
            if dNow.time() < dIgnitionOnTime.time():
                bIgnition = False
            if dNow.time() > dIgnitionOffTime.time():
                bIgnition = False

            # deal with ERB24
            if DEBUG: logging.debug("03")
            if bIgnition:
                logging.info("DVR%d: Ignition relay On" % (nDVR))
                mutexERB24.acquire()
                ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition + nERB24Port)
                mutexERB24.release()
                if bStartup:
                    bStartup = False
                    break
            else:
                logging.info("DVR%d: Ignition relay Off" % (nDVR))
                mutexERB24.acquire()
                ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition + nERB24Port)
                mutexERB24.release()
                bStartup = True

            # logic for sessions
            # (datetime.datetime.now()).time() < dStopTime.time()
            if dNow.time() < dStopTime.time():
                if DEBUG: logging.debug("05")
                bICVSyncDone = False
            if dNow.time() < dStopTime.time():
                h1 = H1()
                logging.info("Restarting")
                logging.info("Waiting till H1 restarts")
                time.sleep(1)
                logging.info(
                    " **** TESTCASE 3 COMPLETED : TESTED OK **** \n **** DVR%d:GSNEXT Record session started..." % (
                        nDVR))

                Videos1 = RecordSession1(nDVR)
                logging.info(
                    "**** TESTCASE 4 COMPLETED : TESTED OK **** \n **** DVR%d:GSNEXT Record session done" % (nDVR))
                bICVSyncDone = True
                #h1 = H1()
                global jsonResponse
                time.sleep(3)
                logging.info("**** TESTCASE 5 COMPLETED : TESTED OK **** \n **** DVR%d:GSNEXT Logging out" % (nDVR))
                Gs_Test()
                logging.info("**** TESTCASE 6 COMPLETED : TESTED OK **** \n **** EMERGENCY LOGIN DONE")
                time.sleep(3)
                _getStatus(h1)
                #                if not jsonResponse["login"]:
                #                    logging.info("DVR%d:Emergency Logging IN" % (nDVR))
                #                    _login(h1, "1111", "1111", "gsnext1")
                logging.info(
                    "**** TESTCASE 7 COMPLETED : TESTED OK **** \n **** DVR%d:Emergency Record session started..." % (
                        nDVR))
                Videos2 = RecordSession1(nDVR)
                logging.info("Printing List of Videos Recorded")
                print(Videos1 + Videos2)
                logging.info(
                    "**** TESTCASE 8 COMPLETED : TESTED OK **** \n **** EMERGENCY SESSION DONE :: GSNEXT UPLOADING EVIDENCE")
                SyncDVR(nDVR)
                logging.info("Allowing time for the videos to upload on the server")
                time.sleep(100)
                ##SyncDVR(nDVR)
                Gs_Back()
                logging.info(
                    "**** TESTCASE 9 COMPLETED : TESTED OK **** \n **** VIDEOS UPLOADED FROM H1 SIDE:: Checking Uploaded Videos")
                Videos3 = Check1.Check()
                #Videos3 = []
                Uploaded_Videos = set(Videos3)
                logging.info("**** TESTCASE 10 COMPLETED : TESTED OK **** \n **** Following Video are on the server")
                print(Videos3)
                logging.info("Printing List of Videos Recorded")
                print(Videos1 + Videos2)
                Gs_Test3()
                Recorded_Videos = set(Videos1 + Videos2)
                if (Uploaded_Videos & Recorded_Videos):
                    logging.info(
                        "**** TESTCASE 11 COMPLETED :  TESTED OK **** \n Following Videos Match on the server ****")
                    # print("Following Videos Match on the server")
                    print(Uploaded_Videos & Recorded_Videos)
                else:
                    print("No common elements")
                logging.info("DVR%d:Emergency Record session done" % (nDVR))
            else:
                logging.info("DVR%d:GSNEXT Record session ENDED..." % (nDVR))

        #           if dNow.time() > dICVSyncTime.time() and not bICVSyncDone:
        #               logging.info("DVR%d: ICVSync session started..." % (nDVR))
        #               SyncDVR(nDVR)
        #               logging.info("DVR%d: ICVSync session done" % (nDVR))
        #               bICVSyncDone = True

        pass  # end while
    logging.info("EXITING LOOP...")

    parse.parser("Error")
    parse.parser("w")
    parse.fwtimer()
    pass  # end while


# end Gs_Test1()

def Check_Videos():
    Videos3 = Check1.Check()
    logging.info("Following Video are on the server")
    print(Videos3)


def Update_Server():
    logging.info("GSNEXT: Syncing the DVR For LATEST SETTINGS")
    Update1.Update()
    SyncDVR(nDVR)
    logging.info("GSNEXT: Syncing DONE")


def Gs_Test2(nDVR):
    DEBUG = False
    #    DEBUG = True

    global aDVRs
    global mutexERB24
    global ERB24_BOARD_NUM
    global nRelayOffset_Ignition
    global nRelayOffset_Power

    logging.info("GSNEXT: Syncing the DVR For LATEST SETTINGS")
    Update.Update()
    #    SyncDVR(nDVR)
    logging.info("GSNEXT: Syncing DONE")

    REST_TIME_MIN = 5 * 60  # 5 min
    REST_TIME_MAX = 30 * 60  # 30 min
    if 1:
        REST_TIME_MIN = 5
        REST_TIME_MAX = 30

    logging.debug('DVR%d: GSNEXT Thread:%s has started...' % (nDVR, threading.currentThread().getName()))

    lHeadings = aDVRs[0].tolist()  # build a set of keys to the values
    nERB24Port = int(aDVRs[nDVR][lHeadings.index('ERB24')])

    mutexERB24.acquire()
    ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power + nERB24Port)
    mutexERB24.release()
    logging.info("DVR%d: Power relay On" % (nDVR))

    # build event times based on local time
    dIgnitionOnTime = datetime.datetime.strptime('07:00', '%H:%M')
    dICVSyncTime = datetime.datetime.strptime('20:00', '%H:%M')
    dIgnitionOffTime = datetime.datetime.strptime('21:30', '%H:%M')
    dHouseKeepingTime = datetime.datetime.strptime('23:55', '%H:%M')
    if 0:
        dIgnitionOnTime = datetime.datetime.strptime('06:00', '%H:%M')
        dICVSyncTime = datetime.datetime.strptime('07:30', '%H:%M')
        dIgnitionOffTime = datetime.datetime.strptime('21:30', '%H:%M')

    bICVSyncDone = True
    nSleep = 6
    bStartup = False
    dNow = datetime.datetime.now()
    dStopTime = dNow.replace(minute=(dNow.minute + 2))
    dStop = dNow.replace(minute=(dNow.minute + 3))
    while dNow < dStop:
        logging.debug("At the 1st while loop")
        while dNow < dStop:  # I am doing this as an unnecessary while loop in case I need to break out of it... via poor mans's GOTO

            if DEBUG: logging.debug("01")
            nSleepCounter = 6
            logging.info("DVR%d: GSNEXT Sleeping %dsec before next run..." % (nDVR, nSleepCounter))

            # the following lines will simply add some stagger to the threads
            time.sleep(nSleepCounter % nSleep)
            nSleepCounter -= nSleepCounter % nSleep

            while nSleepCounter > 0:
                #                dNow = datetime.datetime.now()
                #                dStopTime = dNow.replace(minute= (dNow.minute + 1))
                if dNow.time() > dHouseKeepingTime.time():
                    logging.info("DVR%d: HouseKeeping session started..." % (nDVR))
                    HouseKeeping(nDVR)
                    logging.info("DVR%d: HouseKeeping session done" % (nDVR))
                    break
                time.sleep(nSleep)
                nSleepCounter -= nSleep
                if not nSleepCounter % 60:
                    logging.debug("DVR%d: Sleeping %dsec before next time check..." % (nDVR, nSleepCounter))

            # get the state of the ignition
            if DEBUG: logging.debug("02")
            bIgnition = True
            dNow = datetime.datetime.now()
            if dNow.time() < dIgnitionOnTime.time():
                bIgnition = False
            if dNow.time() > dIgnitionOffTime.time():
                bIgnition = False

            # deal with ERB24
            if DEBUG: logging.debug("03")
            if bIgnition:
                logging.info("DVR%d: Ignition relay On" % (nDVR))
                mutexERB24.acquire()
                ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition + nERB24Port)
                mutexERB24.release()
                if bStartup:
                    bStartup = False
                    break
            else:
                logging.info("DVR%d: Ignition relay Off" % (nDVR))
                mutexERB24.acquire()
                ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition + nERB24Port)
                mutexERB24.release()
                bStartup = True

            # logic for sessions
            # (datetime.datetime.now()).time() < dStopTime.time()
            if dNow.time() < dStopTime.time():
                if DEBUG: logging.debug("05")
                bICVSyncDone = False
            if dNow.time() < dStopTime.time():
                logging.info("DVR%d:GSNEXT Record session started..." % (nDVR))

                Videos1 = RecordSession2(nDVR)
                logging.info("DVR%d:GSNEXT Record session done" % (nDVR))
                bICVSyncDone = True
                h1 = H1()
                global jsonResponse
                time.sleep(3)
                logging.info("DVR%d:GSNEXT Logging out" % (nDVR))
                Gs_Test()
                time.sleep(3)
                _getStatus(h1)
                #                if not jsonResponse["login"]:
                #                    logging.info("DVR%d:Emergency Logging IN" % (nDVR))
                #                    _login(h1, "1111", "1111", "gsnext1")
                logging.info("DVR%d:Emergency Record session started..." % (nDVR))
                Videos2 = RecordSession2(nDVR)
                logging.info("Printing List of Videos Recorded")
                print(Videos1 + Videos2)
                logging.info("GSNEXT UPLOADING EVIDENCE")
                #                SyncDVR(nDVR)
                logging.info("Checking Uploaded Videos")
                Videos3 = Check.Check()
                logging.info("Following Video are on the server")
                print(Videos3)
                logging.info("Printing List of Videos Recorded")
                print(Videos1 + Videos2)
                logging.info("DVR%d:Emergency Record session done" % (nDVR))

            #                SyncDVR(nDVR)

            else:
                logging.info("DVR%d:GSNEXT Record session ENDED..." % (nDVR))
        #           if dNow.time() > dICVSyncTime.time() and not bICVSyncDone:
        #               logging.info("DVR%d: ICVSync session started..." % (nDVR))
        #               SyncDVR(nDVR)
        #               logging.info("DVR%d: ICVSync session done" % (nDVR))
        #               bICVSyncDone = True

        pass  # end while
    logging.info("EXITING LOOP...")
    pass  # end while


# end Gs_Test2()

def Gs_Test3():
    h1 = H1()
    global jsonResponse

    resp = _connect(h1, "172.18.4.100", "9999")
    #    s1 = Status()
    #    a = s1.getversion()
    #    print(a)
    fnames = GetICVFileNames(h1)
    print(fnames)
    open('Logfile.log', 'w').close()
    log_file = GetLogFile(h1, 'runlog')
    text_file = open('Logfile.log', 'w')
    text_file.writelines(log_file)
    text_file.close()
    #    metadata_file = open('Metadata_file.log')

    logging.info("Now Fetching Checked Metadata from Firmware")
    open('Metadata_file.log', 'w').close()
    meta_file = GetMetadataFile(h1, 'gsnext1')
    meta_file1 = open('Metadata_file.log', 'w')
    meta_file1.writelines(meta_file)
    meta_file1.close()

    open('template.config', 'w').close()
    config_file = GetConfigFile(h1, 'gsnext')
    config_file1 = open('template.config', 'w')
    config_file1.writelines(config_file)
    config_file1.close()

    GetMeta(h1, 'gsnext')
    if not resp:
        exit()

    h1.setpollstatus(True)


def Gs_Test():
    h1 = H1()
    global jsonResponse

    resp = _connect(h1, "172.18.4.100", "9999")

    time.sleep(3)
    h1.sendjson({"command": "logout"})
    time.sleep(10)
    logging.info("DVR%d:Emergency Logging IN")
    # if not logged in then login
    # need to repeat if login falis??
    time.sleep(3)

    logging.info("GSTEST:Emergency Logging IN")
    _emlogin(h1, "1111", "1111", "gsnext1")

#    s1 = Status()
#    a = s1.getversion()
#    print(a)

# fnames = GetICVFileNames(h1)
# print(fnames)
# open('Logfile.log', 'w').close()
# log_file = GetLogFile(h1, 'gsnext')
# text_file = open('Logfile.log', 'w')
# text_file.writelines(log_file)
# text_file.close()
# metadata_file = open('Metadata_file.log')

# logging.info("Now Fetching Checked Metadata from Firmware")
# open('Metadata_file.log', 'w').close()
# meta_file = GetMetadataFile(h1, 'gsnext1')
# meta_file1 = open('Metadata_file.log', 'w')
# meta_file1.writelines(meta_file)
# meta_file1.close()

# open('template.config', 'w').close()
# config_file = GetConfigFile(h1, 'gsnext')
# config_file1 = open('template.config', 'w')
# config_file1.writelines(config_file)
# config_file1.close()

# GetMeta(h1, 'gsnext')
# if not resp:
#    exit()

# h1.setpollstatus(True)

# time.sleep(3)
# h1.sendjson({"command": "logout"})
# time.sleep(10)
# logging.info("DVR%d:Emergency Logging IN")
## if not logged in then login
## need to repeat if login falis??
# time.sleep(3)

# logging.info("GSTEST:Emergency Logging IN")
# _emlogin(h1, "1111", "1111","gsnext1")


#    _getStatus(h1)
#    if not jsonResponse["login"]:
#        _login(h1, "1111", "1111", "1111")
##       _login(h1,"gsnext1","123","gsnext1")

#    _startRecord(h1,0)
#    time.sleep(3)
#    _stopRecord(h1,0)
#    time.sleep(3)
#    h1.sendjson({"command": "logout"})
#    time.sleep(3)

#    _getStatus(h1)
#    if not jsonResponse["login"]:
#        _login(h1,"gsnext1","123","gsnext1")

#    _startRecord(h1, 2)
#    time.sleep(3)
#    _stopRecord(h1, 2)
#    time.sleep(3)
#    h1.sendjson({"command": "logout"})
#    time.sleep(5)
#    h1.sendjson({"command": "shutdown"})
#    _getStatus(h1)
#    if jsonResponse["login"]:
#        _login(h1, "gsnext1", "1111", "gsnext1")
# start record you could cylce thru 0 to installed cameras
# this is available from the config file
#    _startRecord(h1,0)

# record for num seconds
#    time.sleep(5.0)

# stop record
#    _stopRecord(h1,0)

# get the pending event list
#    _getPendingEventList(h1)

# number of events to modify
#    numEvents = len(jsonResponse['events'])

# send the modify event for each eventName
#    i = 0
#    while i < numEvents:
#        _sendModifyEvent(h1,jsonResponse['events'][i],"0")
#        i += 1

# time.sleep(10.0)

# disconnect
#    h1.disconnect()
# end Gs_Test
def Gs_Restart():
    global aDVRs
    global jsonResponse
    global mutexH1
    global nPollPeriod
    global nStepDelay
    parent = None
    DEBUG = False
    h1 = H1()
    global jsonResponse

    resp = _connect(h1, "172.18.4.100", "9999")
    if not resp:
        exit()

    h1.setpollstatus(True)

    time.sleep(3)
    h1.sendjson({"command": "restart"})
    time.sleep(100)
    logging.info("DVR%d:User Logging IN")
    # if not logged in then login
    # need to repeat if login falis??
    time.sleep(3)

    logging.info("GSTEST:GSNEXT1 Logging IN")
    _emlogin(h1, "gagan", "123", "gsnext1")

    time.sleep(3)

def Gs_Back():
    global aDVRs
    global jsonResponse
    global mutexH1
    global nPollPeriod
    global nStepDelay
    parent = None
    DEBUG = False
    h1 = H1()
    global jsonResponse

    resp = _connect(h1, "172.18.4.100", "9999")
    if not resp:
        exit()

    h1.setpollstatus(True)

    time.sleep(3)
    h1.sendjson({"command": "logout"})
    time.sleep(10)
    logging.info("DVR%d:User Logging IN")
    # if not logged in then login
    # need to repeat if login falis??
    time.sleep(3)

    logging.info("GSTEST:GSNEXT1 Logging IN")
    _emlogin(h1, "gagan", "123", "gsnext1")

    time.sleep(3)

    if resp:
        # ================================================================================
        # Initialize the H1
        # ================================================================================
        mutexH1.acquire()
        if DEBUG: logging.debug("999")
        h1.sendjson({"command": "init"})
        time.sleep(nStepDelay)
        _getStatus(h1)
        time.sleep(nStepDelay)

        if 1:  # Sid debug line
            logging.info("gsnext1-->FINAL status-->jsonResponse:")
            logging.info(pprint.pformat(jsonResponse))

        mutexH1.release()


'''
    try:
        import tkinter as tk
    except:
        import Tkinter as tk

    root = tk.Tk()
    tk.Tk()

    from h1tcpclient import Login

    L1 = Login(parent=root)
    L1.logout()
    time.sleep(3)
    L1.login()

    G1 = GetEvent(parent=root)
    G1.get()
'''

#   GetStats(h1, 'gsnext')

'''   
        _getStatus(h1)
        if not jsonResponse["login"]:
            _login(h1, "1111", "1111", "1111")
    #       _login(h1,"gsnext1","123","gsnext1")
'''


#    _startRecord(h1,0)
#    time.sleep(3)
#    _stopRecord(h1,0)
#    time.sleep(3)
#    h1.sendjson({"command": "logout"})
#    time.sleep(3)

#    _getStatus(h1)
#    if not jsonResponse["login"]:
#        _login(h1,"gsnext1","123","gsnext1")

#    _startRecord(h1, 2)
#    time.sleep(3)
#    _stopRecord(h1, 2)
#    time.sleep(3)
#    h1.sendjson({"command": "logout"})
#    time.sleep(5)
#    h1.sendjson({"command": "shutdown"})
#    _getStatus(h1)
#    if jsonResponse["login"]:
#        _login(h1, "gsnext1", "1111", "gsnext1")
# start record you could cylce thru 0 to installed cameras
# this is available from the config file
#    _startRecord(h1,0)

# record for num seconds
#    time.sleep(5.0)

# stop record
#    _stopRecord(h1,0)

# get the pending event list
#    _getPendingEventList(h1)

# number of events to modify
#    numEvents = len(jsonResponse['events'])

# send the modify event for each eventName
#    i = 0
#    while i < numEvents:
#        _sendModifyEvent(h1,jsonResponse['events'][i],"0")
#        i += 1

# time.sleep(10.0)

# disconnect
#    h1.disconnect()
# end Gs_Back


def RandomText(nMaxLength):
    """A simple routine to return a random string built using the panagram."""
    DEBUG = False
    DEBUG = True

    Panagram = ['The', 'quick', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy', 'dog.']  # simple panagram
    # Panagram = ['The', 'quick', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy', 'dog.', "#%*/+-()'", ',;@=?!:"']
    str = ""

    random.shuffle(Panagram)
    nLen = random.randint(1, len(Panagram))
    for xx in range(nLen):
        str += Panagram[xx] + " "
    # str = str.rstrip()     # strip the trailing space
    str = str[:nMaxLength]

    if 0:
        print(str)

    return str


# end RandomText()


def HouseKeeping(nDVR):
    """Proc to power cycle to the DVRs around midnight"""

    DEBUG = False
    DEBUG = True

    global aDVRs
    global mutexERB24
    global ERB24_BOARD_NUM
    global nRelayOffset_Power

    lHeadings = aDVRs[0].tolist()  # build a set of keys to the values
    nERB24Port = int(aDVRs[nDVR][lHeadings.index('ERB24')])

    logging.info("DVR%d: HouseKeeping()" % (nDVR))
    # mutexERB24.acquire()
    # ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power+nERB24Port)
    # mutexERB24.release()
    # logging.info("DVR%d: Power relay Off" % (nDVR))

    time.sleep(600.0)

    mutexERB24.acquire()
    ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power + nERB24Port)
    mutexERB24.release()
    logging.info("DVR%d: Power relay On" % (nDVR))


# end HouseKeeping()


def SyncDVR(nDVR):
    """Proc to sync DVR with the Coban Command Center"""

    DEBUG = False
    DEBUG = True

    global aDVRs
    global jsonResponse
    global mutexH1
    global nPollPeriod
    global nStepDelay
    global file_object

    lHeadings = aDVRs[0].tolist()   # build a set of keys to the values
    strIPAddress = aDVRs[nDVR][lHeadings.index('IP')]
    strPort = aDVRs[nDVR][lHeadings.index('Port')]
    strUnit = aDVRs[nDVR][lHeadings.index('Unit')]
    strOfficer = aDVRs[nDVR][lHeadings.index('Officer')]
    strPassword = aDVRs[nDVR][lHeadings.index('Password')]
    logging.info("DVR%d: SyncDVR() where Unit=%s, IP=%s, Port=%s, Officer=%s" % (nDVR, strUnit, strIPAddress, strPort, strOfficer))

    if DEBUG: logging.debug("31")
    mutexH1.acquire()
    logging.debug("Initializing DVR%d object" % (nDVR))
    h1 = H1()
    resp = _connect(h1, strIPAddress, strPort)
    mutexH1.release()

    if resp:
        if DEBUG: logging.debug("32")
        # login if necessary
        mutexH1.acquire()
        nMaxTrys = 5
        nTrys = 0
        while True:
            if nTrys > nMaxTrys:
                logging.warning("DVR%d: Unable to login to the DVR!" % (nDVR))
                mutexH1.release()
                return
            _getStatus(h1)     # get the status message
            time.sleep(nStepDelay)

            nTrys += 1
            if jsonResponse["command"] != "status":
                continue
            if jsonResponse["login"] == False:
                logging.info("DVR%d: Logging in..." % (nDVR))
                _login(h1, strOfficer, strPassword, strUnit)
                time.sleep(nPollPeriod)
                continue
            elif jsonResponse["login"] == True:
                # good to go
                break
        mutexH1.release()


        if 0:     # Sid test code
            #myBuffer = ""
            myBuffer = GetICVFileNames(h1)
            for file in myBuffer['files']:
                print(file)

            myBuffer = GetLogFile(h1, strUnit)
            lines = myBuffer.split('\n')
            #for line in lines:
                #print(line)
            bob = 0
            return


        if DEBUG: logging.debug("34")
        time.sleep(nPollPeriod)
        mutexH1.acquire()
        h1.sendjson({"command":"cm_starttransfer"})
        time.sleep(nStepDelay)
        mutexH1.release()

        # the following will monitor the start of ICV Sync.
        if DEBUG: logging.debug("35")
        bKattywampus = False
        nProgress = 0
        nMaxTrys = 30     # 2.5 min. with no progress
        nTrys = 0
        wlstatus = ""
        while True:
            nTrys += 1
            if nTrys > nMaxTrys:
                if DEBUG: logging.debug("35a")
                logging.warning("DVR%d: ICV Upload failed to start?" % (nDVR))
                bKattywampus = True     # set an abort flag
                break

            mutexH1.acquire()
            _getStatus(h1)     # get the status message
            time.sleep(nStepDelay)
            jsonResponse2 = jsonResponse
            mutexH1.release()
            if jsonResponse2["command"] != "status":
                if DEBUG: logging.debug("35b")
                continue
            else:
                nProgress = jsonResponse2["uploadpercentage"]
                wlstatus = jsonResponse2["wlstatus"]
            if wlstatus != "IDLE":
                if DEBUG: logging.debug("35c")
                logging.info("DVR%d: ICV Upload underway..." % (nDVR))
                time.sleep(nPollPeriod)
                break
            time.sleep(nPollPeriod)
        pass     #end while

        # the followig will monitor the progress of ICV Sync.
        if DEBUG: logging.debug("36")
        nLastProgress = nProgress
        nMaxTrys = 120     # 10 min. with no progress
        nTrys = 0
        wlstatus = ""
        while not bKattywampus:
            mutexH1.acquire()
            _getStatus(h1)     # get the status message
            time.sleep(nStepDelay)
            jsonResponse2 = jsonResponse
            mutexH1.release()
            if jsonResponse2["command"] != "status":
                continue
            elif "uploadpercentage" in jsonResponse2:
                nProgress = jsonResponse2["uploadpercentage"]
                wlstatus = jsonResponse2["wlstatus"]
            if nProgress == 100 or wlstatus == "CNFG" or wlstatus == "UPDT":
                if DEBUG: logging.debug("36a")
                tString = '*' * 20
                logging.info(tString)
                logging.info("DVR%d: ICV Upload DONE, %d%%, wlstatus=%s" % (nDVR, nProgress, wlstatus))
                logging.info(tString)
                break
            else:
                if nProgress == nLastProgress:
                    nTrys += 1
                    if nTrys > nMaxTrys:
                        if DEBUG: logging.debug("36b")
                        logging.warning("DVR%d: ICV Upload may be stalled (@%d%%), wlstatus=%s?" % (nDVR, nProgress, wlstatus))
                        bKattywampus = True
                else:
                    nLastProgress = nProgress
                    nTrys = 0
                logging.debug("DVR%d: ICV Upload =%d%%." % (nDVR, nProgress))
                time.sleep(nPollPeriod)
        pass     #end while

    else:
        logging.warning("DVR%d: ICV Sync failed as the DVR did not respond")

    # clean-up
    if DEBUG: logging.debug("37")
    mutexH1.acquire()
    h1.disconnect()
    time.sleep(nStepDelay)
    del h1
    mutexH1.release()
#end SyncDVR()

# def ProcessPendingEvents(nDVR, h1, nRecordTime, list1)
def ProcessPendingEvents(nDVR, h1, nRecordTime):
    """Proc to get and process pending events"""

    DEBUG = False
    DEBUG = True

    global aDVRs
    global jsonResponse
    global mutexH1
    global mutexReportFile
    global all_videos

    lHeadings = aDVRs[0].tolist()  # build a set of keys to the values
    strUnit = aDVRs[nDVR][lHeadings.index('Unit')]
    strOfficer = aDVRs[nDVR][lHeadings.index('Officer')]
    nCameras = int(float(aDVRs[nDVR][lHeadings.index('Cam')]))
    Video_List = []
    #    Video_List = list1

    strReportFile = "ReportH1.%s.json" % (time.strftime("%Y%m%d"))  # update the name of the report file

    # get the pending events
    logging.info("DVR%d: Getting pending events..." % (nDVR))
    #    time.sleep(5)
    nMaxTrys = 5
    nTrys = 0
    while True:
        nTrys += 1
        if nTrys > nMaxTrys:
            if DEBUG: logging.debug("21")
            logging.error("DVR%d: Unable to get pending events!" % (nDVR))
            mutexH1.release()
            raise  # this will take me to the try/catch in RecordSession()

        if DEBUG: logging.debug("22")
        mutexH1.acquire()
        _getPendingEventList(h1)
        time.sleep(nStepDelay)
        jsonResponse2 = jsonResponse
        mutexH1.release()
        if jsonResponse2["command"] != "pendingeventlist":
            logging.info("sid2-->pendingeventlist-->jsonResponse:")
            logging.info(pprint.pformat(jsonResponse2))
            time.sleep(1)
            continue
        else:
            eventList = jsonResponse2["events"]
            if len(eventList) != 0:
                break
    pass  # end while

    if nTrys > 1:
        logging.warning("DVR%d: Received pendingeventlist after %d trys" % (nDVR, nTrys))

    # start building metadata
    logging.info("DVR%d: Updating session metadata..." % (nDVR))
    tCurrentTime = time.localtime()
    with open("MetadataH1.json", "r") as f:
        data = json.load(f)
    nNames = len(data["driver"])
    nFirstName = random.randint(1, nNames) - 1
    nLastName = nFirstName
    while nLastName == nFirstName:  # to protect the innocent
        nLastName = random.randint(1, nNames) - 1
    strFirstName, junk = data["driver"][nFirstName].split(maxsplit=1)
    junk, strLastName = data["driver"][nLastName].split(maxsplit=1)
    nEvent = random.randint(1, len(data["eventname"])) - 1
    strEventID = data["eventid"][nEvent]
    strEventName = data["eventname"][nEvent]
    nGender = random.randint(1, len(data["gender"])) - 1
    strGender = "Gender = %s" % (data["gender"][nGender])
    nRace = random.randint(1, len(data["race"])) - 1
    strRace = "Race = %s" % (data["race"][nRace])
    strLocation = RandomText(40)
    strNotes = threading.currentThread().getName()

    # send the modify event for each event
    mutexH1.acquire()
    if DEBUG: logging.debug("25")
    for event in eventList:
        h1.sendjson({"command": "modifyevent", "eventname": event,
                     "event": {
                         "event_id": strEventID,
                         "case": "",
                         "case2": "",
                         "dispatch": strGender,
                         "dispatch2": strRace,
                         "ticket": strEventID,
                         "ticket2": strEventName,
                         "first_name": strFirstName,
                         "last_name": strLastName,
                         "location": strLocation,
                         "notes": strNotes
                     }})
        Video_List.append(event)
        logging.info("The Following Video is to be uploaded:: ")
        print(Video_List[:])
        #        all_videos.append = Video_List
        #        print(all_videos[:])
        time.sleep(nStepDelay)
        _getJsonResponse(h1)
        time.sleep(nStepDelay)
        if jsonResponse["status"] != 0:
            logging.warning("DVR%d: Response from 'modifyevent' was False?" % (nDVR))
        pass  # next event
    mutexH1.release()

    # build the report.json
    if DEBUG: logging.debug("26")
    jMetaData = '{\n'
    strTemp = time.strftime("%Y/%m/%d-%H:%M:%S", tCurrentTime)
    jMetaData += '\t"time": "' + '%s' % (strTemp) + '",\n'
    if DEBUG: logging.debug("26a")
    print(eventList[0])
    jMetaData += '\t"sessions": [\n\t\t"%s' % (eventList[0]) + '"'
    if DEBUG: logging.debug("26b")
    # tBob = len(eventList)
    # if tBob != nCameras:
    #    logging.error("DVR%d: problem with eventlist!!!" % (nDVR))
    #    logging.error("26c, numEvents=%d, nCameras=%d" % (tBob, nCameras))
    #    logging.error("%s" % (eventList))
    if DEBUG: logging.debug("26d, numEvents=%d" % (len(eventList)))
    if len(eventList) > 1:
        jMetaData += ',\n\t\t"%s' % (eventList[1]) + '"'
    if len(eventList) > 2:
        jMetaData += ',\n\t\t"%s' % (eventList[2]) + '"'
    jMetaData += '\n\t],\n'
    jMetaData += '\t"details": [\n\t\t{\n'
    jMetaData += '\t\t"unit": "%s' % (strUnit) + '",\n'
    jMetaData += '\t\t"officer": "%s' % (strOfficer) + '",\n'
    jMetaData += '\t\t"record_time": "%s' % (str(int(nRecordTime))) + 'sec."\n'
    jMetaData += '\t\t}\n\t],\n'
    jMetaData += '\t"metadata": [\n\t\t{\n'
    jMetaData += '\t\t"event_id": "' + strEventID + '",\n'
    jMetaData += '\t\t"case": "' + '",\n'
    jMetaData += '\t\t"case2": "' + '",\n'
    jMetaData += '\t\t"dispatch": "' + strGender + '",\n'
    jMetaData += '\t\t"dispatch2": "' + strRace + '",\n'
    jMetaData += '\t\t"ticket": "' + '",\n'
    jMetaData += '\t\t"ticket2": "' + '",\n'
    jMetaData += '\t\t"first_name": "' + strFirstName + '",\n'
    jMetaData += '\t\t"last_name": "' + strLastName + '",\n'
    jMetaData += '\t\t"location": "' + strLocation + '",\n'
    jMetaData += '\t\t"notes": "' + strNotes + '"\n'
    jMetaData += '\t\t}\n\t]\n'
    jMetaData += '}\n\n'
    if 0:
        print(jMetaData)

    # update the Session report
    if DEBUG: logging.debug("27")
    mutexReportFile.acquire()
    hWin.hWin_WriteToLog(strReportFile, jMetaData)
    logging.info("Returning Video List")

    mutexReportFile.release()
    #    logging.info(":::::::::here is a list of all the videos to be uploaded::::::::")
    return Video_List


# end ProcessPendingEvents()


def RecordSession(nDVR):
    """Proc to initiate a recording session"""

    DEBUG = False
    DEBUG = True

    global aDVRs
    global jsonResponse
    global mutexH1
    global nPollPeriod
    global nStepDelay

    RECORD_TIME_MIN = 60  # seconds
    RECORD_TIME_MAX = 600  # seconds
    if 1:
        RECORD_TIME_MIN = 5
        RECORD_TIME_MAX = 10

    STATE_IDLE = 0
    STATE_RECORDING = 1

    bBookmarkDone = True
    bSnapshotDone = True
    bReverseCameras = False

    lHeadings = aDVRs[0].tolist()  # build a set of keys to the values
    strIPAddress = aDVRs[nDVR][lHeadings.index('IP')]
    strPort = aDVRs[nDVR][lHeadings.index('Port')]
    strUnit = aDVRs[nDVR][lHeadings.index('Unit')]
    strOfficer = aDVRs[nDVR][lHeadings.index('Officer')]
    strPassword = aDVRs[nDVR][lHeadings.index('Password')]
    nCameras = int(float(aDVRs[nDVR][lHeadings.index('Cam')]))
    logging.info("DVR%d: RecordSession() where Unit=%s, IP=%s, Port=%s, Officer=%s" % (
    nDVR, strUnit, strIPAddress, strPort, strOfficer))

    logging.debug("Initializing DVR%d object" % (nDVR))
    h1 = H1()

    logging.info("DVR%d: Trying a record session..." % (nDVR))
    try:
        mutexH1.acquire()
        resp = _connect(h1, strIPAddress, strPort)
        mutexH1.release()

        if resp:
            # ================================================================================
            # Initialize the H1
            # ================================================================================
            mutexH1.acquire()
            if DEBUG: logging.debug("101")
            h1.sendjson({"command": "init"})
            time.sleep(nStepDelay)
            _getStatus(h1)
            time.sleep(nStepDelay)

            if 1:  # Sid debug line
                logging.info("sid1-->status-->jsonResponse:")
                logging.info(pprint.pformat(jsonResponse))

            mutexH1.release()

            # ================================================================================
            # Login if necessary
            # ================================================================================
            mutexH1.acquire()
            nMaxTrys = 5
            nTrys = 0
            while True:
                if nTrys > nMaxTrys:
                    logging.warning("DVR%d: Unable to login to the DVR!" % (nDVR))
                    mutexH1.release()
                    raise
                _getStatus(h1)  # get the status message
                time.sleep(nStepDelay)

                nTrys += 1
                if jsonResponse["command"] != "status":
                    continue
                if jsonResponse["login"] == False:
                    logging.info("DVR%d: Logging in..." % (nDVR))
                    _login(h1, strOfficer, strPassword, strUnit)
                    time.sleep(nPollPeriod)
                    continue
                elif jsonResponse["login"] == True:
                    # good to go
                    break
            mutexH1.release()

            # ================================================================================
            # DVR should be IDLE
            # ================================================================================
            nTryStatus = 0
            while True:  # get the status
                nTryStatus += 1
                if nTryStatus > 10:
                    logging.error("DVR%d: Unable to get the Status!" % (nDVR))
                    raise
                if DEBUG: logging.info("111")
                mutexH1.acquire()
                _getStatus(h1)  # get the status message
                time.sleep(nStepDelay)
                jsonResponse2 = jsonResponse
                mutexH1.release()
                if jsonResponse2["command"] != "status":
                    logging.warning("DVR%d: Trying to get status... nTryStatus=%d" % (nDVR, nTryStatus))
                    continue
                else:
                    break

            # get the recording state
            if DEBUG: logging.info("112")
            nDVRState = STATE_IDLE
            for key in jsonResponse2["camera"]:  # check if any camera is recording
                if key["recording"] == True:
                    nDVRState = STATE_RECORDING

            if nDVRState == STATE_RECORDING:
                logging.warning("DVR%d: Already in record...?" % (nDVR))

            # ================================================================================
            # I need to put the DVR into Record
            # ================================================================================
            # set random record timer
            nRecordTime = random.randint(RECORD_TIME_MIN, RECORD_TIME_MAX)
            if nRecordTime % 2:
                bReverseCameras = True  # trigger to start/stop the cameras in reverse order
            if nRecordTime > 240:
                bSnapshotDone = False
            if nRecordTime > 180:
                bBookmarkDone = False

            if DEBUG: logging.info("115")
            mutexH1.acquire()
            if bReverseCameras:
                if DEBUG: logging.info("116")
                if (nCameras > 2):
                    _startRecord(h1, 2)
                    time.sleep(2.0)
                if (nCameras > 1):
                    _startRecord(h1, 1)
                    time.sleep(2.0)
                _startRecord(h1, 0)
                time.sleep(nStepDelay)
            else:
                if DEBUG: logging.info("117")
                _startRecord(h1, 0)
                time.sleep(2.0)
                if (nCameras > 1):
                    _startRecord(h1, 1)
                    time.sleep(2.0)
                if (nCameras > 2):
                    _startRecord(h1, 2)
                    time.sleep(nStepDelay)
            mutexH1.release()

            if DEBUG: logging.info("118")
            logging.info("DVR%d: Record session of %dsec started..." % (nDVR, nRecordTime))

            # ================================================================================
            # DVR should be in Record
            # ================================================================================
            nTrys = 0
            while 1:
                nTrys += 1
                if nTrys > 10:
                    logging.error("DVR%d: Unable to keep the DVR in record!" % (nDVR))
                    raise
                nTryStatus = 0
                while True:  # get the status
                    nTryStatus += 1
                    if nTryStatus > 10:
                        logging.error("DVR%d: Unable to get the Status!" % (nDVR))
                        raise
                    if DEBUG: logging.info("121")
                    mutexH1.acquire()
                    _getStatus(h1)  # get the status message
                    time.sleep(nStepDelay)
                    jsonResponse2 = jsonResponse
                    mutexH1.release()
                    if jsonResponse2["command"] != "status":
                        logging.warning("DVR%d: Trying to get status... nTryStatus=%d" % (nDVR, nTryStatus))
                        continue
                    else:
                        break

                # get the recording state
                if DEBUG: logging.info("122")
                nDVRState = STATE_IDLE
                for key in jsonResponse2["camera"]:  # check if any camera is recording
                    if DEBUG: logging.info("123")
                    if key["recording"] == True:
                        nDVRState = STATE_RECORDING

                if nDVRState != STATE_RECORDING:
                    logging.warning("DVR%d: Was not in Record! nLoop=%d" % (nDVR, nTrys))
                    mutexH1.acquire()
                    if bReverseCameras:
                        if (nCameras > 2):
                            _startRecord(h1, 2)
                            time.sleep(2.0)
                        if (nCameras > 1):
                            _startRecord(h1, 1)
                            time.sleep(2.0)
                        _startRecord(h1, 0)
                        time.sleep(nStepDelay)
                    else:
                        _startRecord(h1, 0)
                        time.sleep(2.0)
                        if (nCameras > 1):
                            _startRecord(h1, 1)
                            time.sleep(2.0)
                        if (nCameras > 2):
                            _startRecord(h1, 2)
                            time.sleep(nStepDelay)
                    mutexH1.release()
                    continue
                else:
                    break

            # dump the mod
            nCounter = nRecordTime
            time.sleep(nCounter % nPollPeriod)
            nCounter -= nCounter % nPollPeriod

            # ================================================================================
            # DVR should stay in Record for nCounter
            # ================================================================================
            while nCounter > 0:
                nTryStatus = 0
                while True:  # get the status
                    nTryStatus += 1
                    if nTryStatus > 10:
                        logging.error("DVR%d: Unable to get the Status!" % (nDVR))
                        raise
                    if DEBUG: logging.info("124")
                    mutexH1.acquire()
                    _getStatus(h1)  # get the status message
                    time.sleep(nStepDelay)
                    jsonResponse2 = jsonResponse
                    mutexH1.release()
                    if jsonResponse2["command"] != "status":
                        logging.warning("DVR%d: Trying to get status... nTryStatus=%d" % (nDVR, nTryStatus))
                        continue
                    else:
                        break

                # get the recording state
                if DEBUG: logging.info("125")
                nDVRState = STATE_IDLE
                for key in jsonResponse2["camera"]:  # check if any camera is recording
                    if DEBUG: logging.info("123")
                    if key["recording"] == True:
                        nDVRState = STATE_RECORDING

                if nDVRState != STATE_RECORDING:
                    logging.error("DVR%d: Dropped out of record early! nCounter=%d" % (nDVR, nCounter))
                    break
                else:
                    if DEBUG: logging.info("126")
                    if nCounter <= 240:
                        if not bSnapshotDone:
                            logging.info("DVR%d: Recording a Snapshot" % (nDVR))
                            nTargetCamera = random.randint(1, RECORD_TIME_MAX) % nCameras
                            mutexH1.acquire()
                            _snapshot(h1, nTargetCamera)
                            time.sleep(nStepDelay)
                            mutexH1.release()
                            bSnapshotDone = True
                    if nCounter <= 180:
                        if not bBookmarkDone:
                            logging.info("DVR%d: Recording a Bookmark" % (nDVR))
                            mutexH1.acquire()
                            _bookmark(h1, 0)
                            mutexH1.release()
                            bBookmarkDone = True
                    time.sleep(nPollPeriod)
                    nCounter -= nPollPeriod
                    continue

            # ================================================================================
            # Record session is done, turn off camera(s)
            # ================================================================================
            nTrys = 0
            while nDVRState == STATE_RECORDING:
                nTryStatus = 0
                while True:  # get the status
                    nTryStatus += 1
                    if nTryStatus > 10:
                        logging.error("DVR%d: Unable to get the Status!" % (nDVR))
                        raise
                    if DEBUG: logging.info("131")
                    mutexH1.acquire()
                    _getStatus(h1)  # get the status message
                    time.sleep(nStepDelay)
                    jsonResponse2 = jsonResponse
                    mutexH1.release()
                    if jsonResponse2["command"] != "status":
                        logging.warning("DVR%d: Trying to get status... nTryStatus=%d" % (nDVR, nTryStatus))
                        continue
                    else:
                        break

                nDVRState = STATE_IDLE
                for key in jsonResponse2["camera"]:  # check if any camera is recording
                    if DEBUG: logging.info("132")
                    if key["recording"] == True:
                        nDVRState = STATE_RECORDING

                if nDVRState == STATE_RECORDING:
                    nTrys += 1
                    if nTrys > 10:
                        logging.error("DVR%d: Unable to stop recording!" % (nDVR))
                        raise
                    # Try to stop the Recording
                    if DEBUG: logging.info("133")
                    for key in jsonResponse2["camera"]:  # check if any camera is recording
                        if key["recording"] == True:
                            logging.info("Stopping record on cameraId=%d" % (key["id"]))
                            mutexH1.acquire()
                            _stopRecord(h1, key["id"])
                            time.sleep(nStepDelay)
                            mutexH1.release()
                    if nTrys > 1:
                        logging.warning("DVR%d: Trying to STOP record... nLoop=%d" % (nDVR, nTrys))
                    continue

            # ================================================================================
            # Process Pending Events
            # ================================================================================
            ProcessPendingEvents(nDVR, h1, nRecordTime)

            # ================================================================================
            # Add in some session logouts
            # ================================================================================
            if DEBUG: logging.info("140")
            now = datetime.datetime.now()
            if now.hour % 4 == 0:
                if DEBUG: logging.info("141")
                logging.info("Sleeping for 30sec., to give the H1 time to copy the files and stabalize")
                time.sleep(30.0)  # give the H1 time to copy the files and stabalize
                logging.info("DVR%d: Logging out officer" % (nDVR))
                mutexH1.acquire()
                h1.sendjson({"command": "logout"})
                time.sleep(nStepDelay)
                mutexH1.release()

        else:
            logging.warning("DVR%d: Recording failed as the DVR did not respond" % (nDVR))

    except:
        logging.error("DVR%d: RecordSession() failed!" % (nDVR))
        if mutexH1.locked():  # a bit risky... but best I can do
            mutexH1.release()

    else:
        mutexH1.acquire()
        h1.disconnect()
        time.sleep(nStepDelay)
        mutexH1.release()

    # clean-up
    del h1


# end RecordSession()

def RecordSession1(nDVR):
    """Proc to initiate a recording session"""

    #    DEBUG = False
    DEBUG = True

    global aDVRs
    global jsonResponse
    global mutexH1
    global nPollPeriod
    global nStepDelay

    RECORD_TIME_MIN = 60  # seconds
    RECORD_TIME_MAX = 600  # seconds
    if 1:
        RECORD_TIME_MIN = 5
        RECORD_TIME_MAX = 10

    STATE_IDLE = 0
    STATE_RECORDING = 1

    bBookmarkDone = True
    bSnapshotDone = True
    bReverseCameras = False

    lHeadings = aDVRs[0].tolist()  # build a set of keys to the values
    strIPAddress = aDVRs[nDVR][lHeadings.index('IP')]
    strPort = aDVRs[nDVR][lHeadings.index('Port')]
    strUnit = aDVRs[nDVR][lHeadings.index('Unit')]
    strOfficer = aDVRs[nDVR][lHeadings.index('Officer')]
    strPassword = aDVRs[nDVR][lHeadings.index('Password')]
    nCameras = int(float(aDVRs[nDVR][lHeadings.index('Cam')]))
    logging.info("DVR%d: RecordSession() where Unit=%s, IP=%s, Port=%s, Officer=%s" % (
    nDVR, strUnit, strIPAddress, strPort, strOfficer))

    logging.debug("Initializing DVR%d object" % (nDVR))
    h1 = H1()

    logging.info("DVR%d: Trying a record session..." % (nDVR))
    try:
        mutexH1.acquire()
        resp = _connect(h1, strIPAddress, strPort)
        mutexH1.release()

        if resp:
            # ================================================================================
            # Initialize the H1
            # ================================================================================
            mutexH1.acquire()
            if DEBUG: logging.debug("101")
            h1.sendjson({"command": "init"})
            time.sleep(nStepDelay)
            _getStatus(h1)
            time.sleep(nStepDelay)

            if 1:  # Sid debug line
                logging.info("gsnext1-->status-->jsonResponse:")
                logging.info(pprint.pformat(jsonResponse))

            mutexH1.release()

            # ================================================================================
            # Login if necessary
            # ================================================================================
            mutexH1.acquire()
            nMaxTrys = 5
            nTrys = 0
            while True:
                if nTrys > nMaxTrys:
                    logging.warning("DVR%d: Unable to login to the DVR!" % (nDVR))
                    mutexH1.release()
                    raise
                _getStatus(h1)  # get the status message
                time.sleep(nStepDelay)

                nTrys += 1
                if jsonResponse["command"] != "status":
                    continue
                if jsonResponse["login"] == False:
                    logging.info("DVR%d: Logging in..." % (nDVR))
                    _login(h1, strOfficer, strPassword, strUnit)
                    time.sleep(nPollPeriod)
                    continue
                elif jsonResponse["login"] == True:
                    # good to go
                    break
            mutexH1.release()

            # ================================================================================
            # DVR should be IDLE
            # ================================================================================
            nTryStatus = 0
            while True:  # get the status
                nTryStatus += 1
                if nTryStatus > 10:
                    logging.error("DVR%d: Unable to get the Status!" % (nDVR))
                    raise
                if DEBUG: logging.info("111")
                mutexH1.acquire()
                _getStatus(h1)  # get the status message
                time.sleep(nStepDelay)
                jsonResponse2 = jsonResponse
                mutexH1.release()
                if jsonResponse2["command"] != "status":
                    logging.warning("DVR%d: Trying to get status... nTryStatus=%d" % (nDVR, nTryStatus))
                    continue
                else:
                    break

            # get the recording state
            if DEBUG: logging.info("112")
            nDVRState = STATE_IDLE
            for key in jsonResponse2["camera"]:  # check if any camera is recording
                if key["recording"] == True:
                    nDVRState = STATE_RECORDING

            if nDVRState == STATE_RECORDING:
                logging.warning("DVR%d: Already in record...?" % (nDVR))

            # ================================================================================
            # I need to put the DVR into Record
            # ================================================================================
            # set random record timer
            #            nRecordTime = random.randint(RECORD_TIME_MIN, RECORD_TIME_MAX)
            nRecordTime = 10
            if nRecordTime % 2:
                bReverseCameras = True  # trigger to start/stop the cameras in reverse order
                bSnapshotDone = True
                bBookmarkDone = True
            if nRecordTime > 240:
                bSnapshotDone = True
            if nRecordTime > 180:
                bBookmarkDone = True

            if DEBUG: logging.info("115")
            mutexH1.acquire()
            if bReverseCameras:
                if DEBUG: logging.info("116")
                if (nCameras > 2):
                    _startRecord(h1, 2)
                    logging.info("GSNEXt: Starting a Recording on Camera3")
                    _snapshot(h1, 2)
                    _bookmark(h1, 2)
                    time.sleep(2.0)
                if (nCameras > 1):
                    _startRecord(h1, 1)
                    logging.info("GSNEXt: Starting a Recording on Camera2")
                    _snapshot(h1, 1)
                    _bookmark(h1, 1)
                    time.sleep(2.0)
                _startRecord(h1, 0)
                time.sleep(nStepDelay)
            else:
                if DEBUG: logging.info("117")
                _startRecord(h1, 0)
                logging.info("GSNEXt: Starting a Recording on Camera1")
                logging.info("GSNEXt: Taking a Snapshot on Camera1")
                _snapshot(h1, 0)
                logging.info("GSNEXt: Capturing a Bookmark on Camera1")
                _bookmark(h1, 0)
                time.sleep(2.0)
                if (nCameras > 1):
                    _startRecord(h1, 1)
                    time.sleep(2.0)
                if (nCameras > 2):
                    _startRecord(h1, 2)
                    time.sleep(nStepDelay)
            mutexH1.release()

            if DEBUG: logging.info("118")
            logging.info("DVR%d: Record session of %dsec started..." % (nDVR, nRecordTime))

            # ================================================================================
            # DVR should be in Record
            # ================================================================================
            nTrys = 0
            while 1:
                nTrys += 1
                if nTrys > 10:
                    logging.error("DVR%d: Unable to keep the DVR in record!" % (nDVR))
                    raise
                nTryStatus = 0
                while True:  # get the status
                    nTryStatus += 1
                    if nTryStatus > 10:
                        logging.error("DVR%d: Unable to get the Status!" % (nDVR))
                        raise
                    if DEBUG: logging.info("121")
                    mutexH1.acquire()
                    _getStatus(h1)  # get the status message
                    time.sleep(nStepDelay)
                    jsonResponse2 = jsonResponse
                    mutexH1.release()
                    if jsonResponse2["command"] != "status":
                        logging.warning("DVR%d: Trying to get status... nTryStatus=%d" % (nDVR, nTryStatus))
                        continue
                    else:
                        break

                # get the recording state
                if DEBUG: logging.info("122")
                nDVRState = STATE_IDLE
                for key in jsonResponse2["camera"]:  # check if any camera is recording
                    if DEBUG: logging.info("123")
                    if key["recording"] == True:
                        nDVRState = STATE_RECORDING

                if nDVRState != STATE_RECORDING:
                    logging.warning("DVR%d: Was not in Record! nLoop=%d" % (nDVR, nTrys))
                    mutexH1.acquire()
                    if bReverseCameras:
                        if (nCameras > 2):
                            _startRecord(h1, 2)
                            time.sleep(2.0)
                        if (nCameras > 1):
                            _startRecord(h1, 1)
                            time.sleep(2.0)
                        _startRecord(h1, 0)
                        time.sleep(nStepDelay)
                    else:
                        _startRecord(h1, 0)
                        time.sleep(2.0)
                        if (nCameras > 1):
                            _startRecord(h1, 1)
                            time.sleep(2.0)
                        if (nCameras > 2):
                            _startRecord(h1, 2)
                            time.sleep(nStepDelay)
                    mutexH1.release()
                    continue
                else:
                    break

            # dump the mod
            nCounter = nRecordTime
            time.sleep(nCounter % nPollPeriod)
            nCounter -= nCounter % nPollPeriod

            # ================================================================================
            # DVR should stay in Record for nCounter
            # ================================================================================
            while nCounter > 0:
                nTryStatus = 0
                while True:  # get the status
                    nTryStatus += 1
                    if nTryStatus > 10:
                        logging.error("DVR%d: Unable to get the Status!" % (nDVR))
                        raise
                    if DEBUG: logging.info("124")
                    mutexH1.acquire()
                    _getStatus(h1)  # get the status message
                    time.sleep(nStepDelay)
                    jsonResponse2 = jsonResponse
                    mutexH1.release()
                    if jsonResponse2["command"] != "status":
                        logging.warning("DVR%d: Trying to get status... nTryStatus=%d" % (nDVR, nTryStatus))
                        continue
                    else:
                        break

                # get the recording state
                if DEBUG: logging.info("125")
                nDVRState = STATE_IDLE
                for key in jsonResponse2["camera"]:  # check if any camera is recording
                    if DEBUG: logging.info("123")
                    if key["recording"] == True:
                        nDVRState = STATE_RECORDING

                if nDVRState != STATE_RECORDING:
                    logging.error("DVR%d: Dropped out of record early! nCounter=%d" % (nDVR, nCounter))
                    break
                else:
                    if DEBUG: logging.info("126")
                    if nCounter <= 240:
                        if not bSnapshotDone:
                            logging.info("DVR%d: Recording a Snapshot" % (nDVR))
                            nTargetCamera = random.randint(1, RECORD_TIME_MAX) % nCameras
                            mutexH1.acquire()
                            _snapshot(h1, nTargetCamera)
                            time.sleep(nStepDelay)
                            mutexH1.release()
                            bSnapshotDone = True
                    if nCounter <= 180:
                        if not bBookmarkDone:
                            logging.info("DVR%d: Recording a Bookmark" % (nDVR))
                            mutexH1.acquire()
                            _bookmark(h1, 0)
                            mutexH1.release()
                            bBookmarkDone = True
                    time.sleep(nPollPeriod)
                    nCounter -= nPollPeriod
                    continue

            # ================================================================================
            # Record session is done, turn off camera(s)
            # ================================================================================
            nTrys = 0
            while nDVRState == STATE_RECORDING:
                nTryStatus = 0
                while True:  # get the status
                    nTryStatus += 1
                    if nTryStatus > 10:
                        logging.error("DVR%d: Unable to get the Status!" % (nDVR))
                        raise
                    if DEBUG: logging.info("131")
                    mutexH1.acquire()
                    _getStatus(h1)  # get the status message
                    time.sleep(nStepDelay)
                    jsonResponse2 = jsonResponse
                    mutexH1.release()
                    if jsonResponse2["command"] != "status":
                        logging.warning("DVR%d: Trying to get status... nTryStatus=%d" % (nDVR, nTryStatus))
                        continue
                    else:
                        break

                nDVRState = STATE_IDLE
                for key in jsonResponse2["camera"]:  # check if any camera is recording
                    if DEBUG: logging.info("132")
                    if key["recording"] == True:
                        nDVRState = STATE_RECORDING

                if nDVRState == STATE_RECORDING:
                    nTrys += 1
                    if nTrys > 10:
                        logging.error("DVR%d: Unable to stop recording!" % (nDVR))
                        raise
                    # Try to stop the Recording
                    if DEBUG: logging.info("133")
                    for key in jsonResponse2["camera"]:  # check if any camera is recording
                        if key["recording"] == True:
                            logging.info("Stopping record on cameraId=%d" % (key["id"]))
                            mutexH1.acquire()
                            _stopRecord(h1, key["id"])
                            time.sleep(nStepDelay)
                            mutexH1.release()
                    if nTrys > 1:
                        logging.warning("DVR%d: Trying to STOP record... nLoop=%d" % (nDVR, nTrys))
                    continue

            # ================================================================================
            # Process Pending Events
            # ================================================================================
            #            list2 =
            list9 = ProcessPendingEvents(nDVR, h1, nRecordTime)
            #            logging.info("##################Got the list back###################")
            #            print(list2)
            # ================================================================================
            # Add in some session logouts
            # ================================================================================
            if DEBUG: logging.info("140")
            now = datetime.datetime.now()
            if now.hour % 4 == 0:
                if DEBUG: logging.info("141")
                logging.info("Sleeping for 30sec., to give the H1 time to copy the files and stabalize")
                time.sleep(30.0)  # give the H1 time to copy the files and stabalize
                logging.info("DVR%d: Logging out officer" % (nDVR))
                mutexH1.acquire()
                #                h1.sendjson({"command":"logout"})
                time.sleep(nStepDelay)
                mutexH1.release()

        else:
            logging.warning("DVR%d: Recording failed as the DVR did not respond" % (nDVR))

    except:
        logging.error("DVR%d: RecordSession() failed!" % (nDVR))
        if mutexH1.locked():  # a bit risky... but best I can do
            mutexH1.release()

    else:
        mutexH1.acquire()
        h1.disconnect()
        time.sleep(nStepDelay)
        mutexH1.release()

    # clean-up
    del h1
    return list9


# end RecordSession1()

def RecordSession2(nDVR):
    """Proc to initiate a recording session"""

    #    DEBUG = False
    DEBUG = True

    global aDVRs
    global jsonResponse
    global mutexH1
    global nPollPeriod
    global nStepDelay

    RECORD_TIME_MIN = 60  # seconds
    RECORD_TIME_MAX = 600  # seconds
    if 1:
        RECORD_TIME_MIN = 5
        RECORD_TIME_MAX = 10

    STATE_IDLE = 0
    STATE_RECORDING = 1

    bBookmarkDone = True
    bSnapshotDone = True
    bReverseCameras = False

    lHeadings = aDVRs[0].tolist()  # build a set of keys to the values
    strIPAddress = aDVRs[nDVR][lHeadings.index('IP')]
    strPort = aDVRs[nDVR][lHeadings.index('Port')]
    strUnit = aDVRs[nDVR][lHeadings.index('Unit')]
    strOfficer = aDVRs[nDVR][lHeadings.index('Officer')]
    strPassword = aDVRs[nDVR][lHeadings.index('Password')]
    nCameras = int(float(aDVRs[nDVR][lHeadings.index('Cam')]))
    logging.info("DVR%d: RecordSession() where Unit=%s, IP=%s, Port=%s, Officer=%s" % (
    nDVR, strUnit, strIPAddress, strPort, strOfficer))

    logging.debug("Initializing DVR%d object" % (nDVR))
    h1 = H1()

    logging.info("DVR%d: Trying a record session..." % (nDVR))
    try:
        mutexH1.acquire()
        resp = _connect(h1, strIPAddress, strPort)
        mutexH1.release()

        if resp:
            # ================================================================================
            # Initialize the H1
            # ================================================================================
            mutexH1.acquire()
            if DEBUG: logging.debug("101")
            h1.sendjson({"command": "init"})
            time.sleep(nStepDelay)
            _getStatus(h1)
            time.sleep(nStepDelay)

            if 1:  # Sid debug line
                logging.info("sid1-->status-->jsonResponse:")
                logging.info(pprint.pformat(jsonResponse))

            mutexH1.release()

            # ================================================================================
            # Login if necessary
            # ================================================================================
            mutexH1.acquire()
            nMaxTrys = 5
            nTrys = 0
            while True:
                if nTrys > nMaxTrys:
                    logging.warning("DVR%d: Unable to login to the DVR!" % (nDVR))
                    mutexH1.release()
                    raise
                _getStatus(h1)  # get the status message
                time.sleep(nStepDelay)

                nTrys += 1
                if jsonResponse["command"] != "status":
                    continue
                if jsonResponse["login"] == False:
                    logging.info("DVR%d: Logging in..." % (nDVR))
                    _login(h1, strOfficer, strPassword, strUnit)
                    time.sleep(nPollPeriod)
                    continue
                elif jsonResponse["login"] == True:
                    # good to go
                    break
            mutexH1.release()

            # ================================================================================
            # DVR should be IDLE
            # ================================================================================
            nTryStatus = 0
            while True:  # get the status
                nTryStatus += 1
                if nTryStatus > 10:
                    logging.error("DVR%d: Unable to get the Status!" % (nDVR))
                    raise
                if DEBUG: logging.info("111")
                mutexH1.acquire()
                _getStatus(h1)  # get the status message
                time.sleep(nStepDelay)
                jsonResponse2 = jsonResponse
                mutexH1.release()
                if jsonResponse2["command"] != "status":
                    logging.warning("DVR%d: Trying to get status... nTryStatus=%d" % (nDVR, nTryStatus))
                    continue
                else:
                    break

            # get the recording state
            if DEBUG: logging.info("112")
            nDVRState = STATE_IDLE
            for key in jsonResponse2["camera"]:  # check if any camera is recording
                if key["recording"] == True:
                    nDVRState = STATE_RECORDING

            if nDVRState == STATE_RECORDING:
                logging.warning("DVR%d: Already in record...?" % (nDVR))

            # ================================================================================
            # I need to put the DVR into Record
            # ================================================================================
            # set random record timer
            #            nRecordTime = random.randint(RECORD_TIME_MIN, RECORD_TIME_MAX)
            nRecordTime = 60
            if nRecordTime % 2:
                bReverseCameras = True  # trigger to start/stop the cameras in reverse order
                bSnapshotDone = True
                bBookmarkDone = True
            if nRecordTime > 240:
                bSnapshotDone = True
            if nRecordTime > 180:
                bBookmarkDone = True

            if DEBUG: logging.info("115")
            mutexH1.acquire()
            if bReverseCameras:
                if DEBUG: logging.info("116")
                if (nCameras > 2):
                    _startRecord(h1, 2)
                    logging.info("GSNEXt: Starting a Recording on Camera3")
                    _snapshot(h1, 2)
                    _bookmark(h1, 2)
                    time.sleep(2.0)
                if (nCameras > 1):
                    _startRecord(h1, 1)
                    logging.info("GSNEXt: Starting a Recording on Camera2")
                    _snapshot(h1, 1)
                    _bookmark(h1, 1)
                    time.sleep(2.0)
                _startRecord(h1, 0)
                time.sleep(nStepDelay)
            else:
                if DEBUG: logging.info("117")
                _startRecord(h1, 0)
                logging.info("GSNEXt: Starting a Recording on Camera1")
                logging.info("GSNEXt: Taking a Snapshot on Camera1")
                _snapshot(h1, 0)
                logging.info("GSNEXt: Capturing a Bookmark on Camera1")
                _bookmark(h1, 0)
                time.sleep(2.0)
                if (nCameras > 1):
                    _startRecord(h1, 1)
                    time.sleep(2.0)
                if (nCameras > 2):
                    _startRecord(h1, 2)
                    time.sleep(nStepDelay)
            mutexH1.release()

            if DEBUG: logging.info("118")
            logging.info("DVR%d: Record session of %dsec started..." % (nDVR, nRecordTime))

            # ================================================================================
            # DVR should be in Record
            # ================================================================================
            nTrys = 0
            while 1:
                nTrys += 1
                if nTrys > 10:
                    logging.error("DVR%d: Unable to keep the DVR in record!" % (nDVR))
                    raise
                nTryStatus = 0
                while True:  # get the status
                    nTryStatus += 1
                    if nTryStatus > 10:
                        logging.error("DVR%d: Unable to get the Status!" % (nDVR))
                        raise
                    if DEBUG: logging.info("121")
                    mutexH1.acquire()
                    _getStatus(h1)  # get the status message
                    time.sleep(nStepDelay)
                    jsonResponse2 = jsonResponse
                    mutexH1.release()
                    if jsonResponse2["command"] != "status":
                        logging.warning("DVR%d: Trying to get status... nTryStatus=%d" % (nDVR, nTryStatus))
                        continue
                    else:
                        break

                # get the recording state
                if DEBUG: logging.info("122")
                nDVRState = STATE_IDLE
                for key in jsonResponse2["camera"]:  # check if any camera is recording
                    if DEBUG: logging.info("123")
                    if key["recording"] == True:
                        nDVRState = STATE_RECORDING

                if nDVRState != STATE_RECORDING:
                    logging.warning("DVR%d: Was not in Record! nLoop=%d" % (nDVR, nTrys))
                    mutexH1.acquire()
                    if bReverseCameras:
                        if (nCameras > 2):
                            _startRecord(h1, 2)
                            time.sleep(2.0)
                        if (nCameras > 1):
                            _startRecord(h1, 1)
                            time.sleep(2.0)
                        _startRecord(h1, 0)
                        time.sleep(nStepDelay)
                    else:
                        _startRecord(h1, 0)
                        time.sleep(2.0)
                        if (nCameras > 1):
                            _startRecord(h1, 1)
                            time.sleep(2.0)
                        if (nCameras > 2):
                            _startRecord(h1, 2)
                            time.sleep(nStepDelay)
                    mutexH1.release()
                    continue
                else:
                    break

            # dump the mod
            nCounter = nRecordTime
            time.sleep(nCounter % nPollPeriod)
            nCounter -= nCounter % nPollPeriod

            # ================================================================================
            # DVR should stay in Record for nCounter
            # ================================================================================
            while nCounter > 0:
                nTryStatus = 0
                while True:  # get the status
                    nTryStatus += 1
                    if nTryStatus > 10:
                        logging.error("DVR%d: Unable to get the Status!" % (nDVR))
                        raise
                    if DEBUG: logging.info("124")
                    mutexH1.acquire()
                    _getStatus(h1)  # get the status message
                    time.sleep(nStepDelay)
                    jsonResponse2 = jsonResponse
                    mutexH1.release()
                    if jsonResponse2["command"] != "status":
                        logging.warning("DVR%d: Trying to get status... nTryStatus=%d" % (nDVR, nTryStatus))
                        continue
                    else:
                        break

                # get the recording state
                if DEBUG: logging.info("125")
                nDVRState = STATE_IDLE
                for key in jsonResponse2["camera"]:  # check if any camera is recording
                    if DEBUG: logging.info("123")
                    if key["recording"] == True:
                        nDVRState = STATE_RECORDING

                if nDVRState != STATE_RECORDING:
                    logging.error("DVR%d: Dropped out of record early! nCounter=%d" % (nDVR, nCounter))
                    break
                else:
                    if DEBUG: logging.info("126")
                    if nCounter <= 240:
                        if not bSnapshotDone:
                            logging.info("DVR%d: Recording a Snapshot" % (nDVR))
                            nTargetCamera = random.randint(1, RECORD_TIME_MAX) % nCameras
                            mutexH1.acquire()
                            _snapshot(h1, nTargetCamera)
                            time.sleep(nStepDelay)
                            mutexH1.release()
                            bSnapshotDone = True
                    if nCounter <= 180:
                        if not bBookmarkDone:
                            logging.info("DVR%d: Recording a Bookmark" % (nDVR))
                            mutexH1.acquire()
                            _bookmark(h1, 0)
                            mutexH1.release()
                            bBookmarkDone = True
                    time.sleep(nPollPeriod)
                    nCounter -= nPollPeriod
                    continue

            # ================================================================================
            # Record session is done, turn off camera(s)
            # ================================================================================
            nTrys = 0
            while nDVRState == STATE_RECORDING:
                nTryStatus = 0
                while True:  # get the status
                    nTryStatus += 1
                    if nTryStatus > 10:
                        logging.error("DVR%d: Unable to get the Status!" % (nDVR))
                        raise
                    if DEBUG: logging.info("131")
                    mutexH1.acquire()
                    _getStatus(h1)  # get the status message
                    time.sleep(nStepDelay)
                    jsonResponse2 = jsonResponse
                    mutexH1.release()
                    if jsonResponse2["command"] != "status":
                        logging.warning("DVR%d: Trying to get status... nTryStatus=%d" % (nDVR, nTryStatus))
                        continue
                    else:
                        break

                nDVRState = STATE_IDLE
                for key in jsonResponse2["camera"]:  # check if any camera is recording
                    if DEBUG: logging.info("132")
                    if key["recording"] == True:
                        nDVRState = STATE_RECORDING

                if nDVRState == STATE_RECORDING:
                    nTrys += 1
                    if nTrys > 10:
                        logging.error("DVR%d: Unable to stop recording!" % (nDVR))
                        raise
                    # Try to stop the Recording
                    if DEBUG: logging.info("133")
                    for key in jsonResponse2["camera"]:  # check if any camera is recording
                        if key["recording"] == True:
                            logging.info("Stopping record on cameraId=%d" % (key["id"]))
                            mutexH1.acquire()
                            _stopRecord(h1, key["id"])
                            time.sleep(nStepDelay)
                            mutexH1.release()
                    if nTrys > 1:
                        logging.warning("DVR%d: Trying to STOP record... nLoop=%d" % (nDVR, nTrys))
                    continue

            # ================================================================================
            # Process Pending Events
            # ================================================================================
            #            list2 =
            list9 = ProcessPendingEvents(nDVR, h1, nRecordTime)
            #            logging.info("##################Got the list back###################")
            #            print(list2)
            # ================================================================================
            # Add in some session logouts
            # ================================================================================
            if DEBUG: logging.info("140")
            now = datetime.datetime.now()
            if now.hour % 4 == 0:
                if DEBUG: logging.info("141")
                logging.info("Sleeping for 30sec., to give the H1 time to copy the files and stabalize")
                time.sleep(30.0)  # give the H1 time to copy the files and stabalize
                logging.info("DVR%d: Logging out officer" % (nDVR))
                mutexH1.acquire()
                #                h1.sendjson({"command":"logout"})
                time.sleep(nStepDelay)
                mutexH1.release()

        else:
            logging.warning("DVR%d: Recording failed as the DVR did not respond" % (nDVR))

    except:
        logging.error("DVR%d: RecordSession() failed!" % (nDVR))
        if mutexH1.locked():  # a bit risky... but best I can do
            mutexH1.release()

    else:
        mutexH1.acquire()
        h1.disconnect()
        time.sleep(nStepDelay)
        mutexH1.release()

    # clean-up
    del h1
    return list9


# end RecordSession2()

def tWorker(nDVR):
    """Thread for main 'Worker' loop to trigger actions based on ToD"""

    DEBUG = False
    DEBUG = True

    global aDVRs
    global mutexERB24
    global ERB24_BOARD_NUM
    global nRelayOffset_Ignition
    global nRelayOffset_Power

    REST_TIME_MIN = 5 * 60  # 5 min
    REST_TIME_MAX = 30 * 60  # 30 min
    if 1:
        REST_TIME_MIN = 5
        REST_TIME_MAX = 30

    logging.debug('DVR%d: Thread:%s has started...' % (nDVR, threading.currentThread().getName()))

    lHeadings = aDVRs[0].tolist()  # build a set of keys to the values
    nERB24Port = int(aDVRs[nDVR][lHeadings.index('ERB24')])

    mutexERB24.acquire()
    ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power + nERB24Port)
    mutexERB24.release()
    logging.info("DVR%d: Power relay On" % (nDVR))

    # build event times based on local time
    dIgnitionOnTime = datetime.datetime.strptime('07:00', '%H:%M')
    dICVSyncTime = datetime.datetime.strptime('20:00', '%H:%M')
    dIgnitionOffTime = datetime.datetime.strptime('21:30', '%H:%M')
    dHouseKeepingTime = datetime.datetime.strptime('23:55', '%H:%M')
    if 0:
        dIgnitionOnTime = datetime.datetime.strptime('06:00', '%H:%M')
        dICVSyncTime = datetime.datetime.strptime('07:30', '%H:%M')
        dIgnitionOffTime = datetime.datetime.strptime('21:30', '%H:%M')

    bICVSyncDone = False
    nSleep = 6

    bStartup = False

    while True:
        logging.debug("At the 1st while loop")
        while True:  # I am doing this as an unnecessary while loop in case I need to break out of it... via poor mans's GOTO

            if DEBUG: logging.debug("01")
            nSleepCounter = random.randint(REST_TIME_MIN, REST_TIME_MAX)
            logging.info("DVR%d: Sleeping %dsec before next run..." % (nDVR, nSleepCounter))

            # the following lines will simply add some stagger to the threads
            time.sleep(nSleepCounter % nSleep)
            nSleepCounter -= nSleepCounter % nSleep

            while nSleepCounter > 0:
                dNow = datetime.datetime.now()
                if dNow.time() > dHouseKeepingTime.time():
                    logging.info("DVR%d: HouseKeeping session started..." % (nDVR))
                    HouseKeeping(nDVR)
                    logging.info("DVR%d: HouseKeeping session done" % (nDVR))
                    break
                time.sleep(nSleep)
                nSleepCounter -= nSleep
                if not nSleepCounter % 60:
                    logging.debug("DVR%d: Sleeping %dsec before next time check..." % (nDVR, nSleepCounter))

            # get the state of the ignition
            if DEBUG: logging.debug("02")
            bIgnition = True
            dNow = datetime.datetime.now()
            if dNow.time() < dIgnitionOnTime.time():
                bIgnition = False
            if dNow.time() > dIgnitionOffTime.time():
                bIgnition = False

            # deal with ERB24
            if DEBUG: logging.debug("03")
            if bIgnition:
                logging.info("DVR%d: Ignition relay On" % (nDVR))
                mutexERB24.acquire()
                ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition + nERB24Port)
                mutexERB24.release()
                if bStartup:
                    bStartup = False
                    break
            else:
                logging.info("DVR%d: Ignition relay Off" % (nDVR))
                mutexERB24.acquire()
                ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition + nERB24Port)
                mutexERB24.release()
                bStartup = True

            # logic for sessions
            if dNow.time() < dICVSyncTime.time():
                if DEBUG: logging.debug("05")
                bICVSyncDone = False
            if bIgnition and dNow.time() < dICVSyncTime.time():
                logging.info("DVR%d: Record session started..." % (nDVR))
                RecordSession(nDVR)
                logging.info("DVR%d: Record session done" % (nDVR))
            if dNow.time() > dICVSyncTime.time() and not bICVSyncDone:
                logging.info("DVR%d: ICVSync session started..." % (nDVR))
                SyncDVR(nDVR)
                logging.info("DVR%d: ICVSync session done" % (nDVR))
                bICVSyncDone = True

        pass  # end while
    pass  # end while


# end tWorker()

def GWorker(nDVR):
    """Thread for main 'Worker' loop to trigger actions based on ToD"""

    DEBUG = False
    DEBUG = True

    global aDVRs
    global mutexERB24
    global ERB24_BOARD_NUM
    global nRelayOffset_Ignition
    global nRelayOffset_Power

    REST_TIME_MIN = 5 * 60  # 5 min
    REST_TIME_MAX = 30 * 60  # 30 min
    if 1:
        REST_TIME_MIN = 5
        REST_TIME_MAX = 30

    logging.debug('DVR%d: Thread:%s has started...' % (nDVR, threading.currentThread().getName()))

    lHeadings = aDVRs[0].tolist()  # build a set of keys to the values
    nERB24Port = int(aDVRs[nDVR][lHeadings.index('ERB24')])

    mutexERB24.acquire()
    ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power + nERB24Port)
    mutexERB24.release()
    logging.info("DVR%d: Power relay On" % (nDVR))

    # build event times based on local time
    dIgnitionOnTime = datetime.datetime.strptime('07:00', '%H:%M')
    dICVSyncTime = datetime.datetime.strptime('20:00', '%H:%M')
    dIgnitionOffTime = datetime.datetime.strptime('21:30', '%H:%M')
    dHouseKeepingTime = datetime.datetime.strptime('23:55', '%H:%M')
    if 0:
        dIgnitionOnTime = datetime.datetime.strptime('06:00', '%H:%M')
        dICVSyncTime = datetime.datetime.strptime('07:30', '%H:%M')
        dIgnitionOffTime = datetime.datetime.strptime('21:30', '%H:%M')

    bICVSyncDone = False
    nSleep = 6

    bStartup = False

    while True:
        logging.debug("At the 1st while loop")
        while True:  # I am doing this as an unnecessary while loop in case I need to break out of it... via poor mans's GOTO

            if DEBUG: logging.debug("01")
            nSleepCounter = random.randint(REST_TIME_MIN, REST_TIME_MAX)
            logging.info("DVR%d: Sleeping %dsec before next run..." % (nDVR, nSleepCounter))

            # the following lines will simply add some stagger to the threads
            time.sleep(nSleepCounter % nSleep)
            nSleepCounter -= nSleepCounter % nSleep

            while nSleepCounter > 0:
                dNow = datetime.datetime.now()
                if dNow.time() > dHouseKeepingTime.time():
                    logging.info("DVR%d: HouseKeeping session started..." % (nDVR))
                    HouseKeeping(nDVR)
                    logging.info("DVR%d: HouseKeeping session done" % (nDVR))
                    break
                time.sleep(nSleep)
                nSleepCounter -= nSleep
                if not nSleepCounter % 60:
                    logging.debug("DVR%d: Sleeping %dsec before next time check..." % (nDVR, nSleepCounter))

            # get the state of the ignition
            if DEBUG: logging.debug("02")
            bIgnition = True
            dNow = datetime.datetime.now()
            if dNow.time() < dIgnitionOnTime.time():
                bIgnition = False
            if dNow.time() > dIgnitionOffTime.time():
                bIgnition = False

            # deal with ERB24
            if DEBUG: logging.debug("03")
            if bIgnition:
                logging.info("DVR%d: Ignition relay On" % (nDVR))
                mutexERB24.acquire()
                ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition + nERB24Port)
                mutexERB24.release()
                if bStartup:
                    bStartup = False
                    break
            else:
                logging.info("DVR%d: Ignition relay Off" % (nDVR))
                mutexERB24.acquire()
                ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition + nERB24Port)
                mutexERB24.release()
                bStartup = True

            # logic for sessions
            if dNow.time() < dICVSyncTime.time():
                if DEBUG: logging.debug("05")
                bICVSyncDone = False
            if bIgnition and dNow.time() < dICVSyncTime.time():
                logging.info("DVR%d: Record session started..." % (nDVR))
                RecordSession(nDVR)
                logging.info("DVR%d: Record session done" % (nDVR))
            if dNow.time() > dICVSyncTime.time() and not bICVSyncDone:
                logging.info("DVR%d: ICVSync session started..." % (nDVR))
                SyncDVR(nDVR)
                logging.info("DVR%d: ICVSync session done" % (nDVR))
                bICVSyncDone = True

        pass  # end while
    pass  # end while


# end GWorker()


def powerOff(nDVR):
    """Simple routine to power off, via Ignition, a specific DVR -- AlanT"""
    # There is also IgnitionsOff.py (and Ninursag help us PowersOff.py if needed)
    DEBUG = False
    # DEBUG = True

    global aDVRs
    global mutexERB24
    global ERB24_BOARD_NUM
    global nRelayOffset_Ignition
    global nRelayOffset_Power

    lHeadings = aDVRs[0].tolist()  # build a set of keys to the values
    nERB24Port = int(aDVRs[nDVR][lHeadings.index('ERB24')])

    mutexERB24.acquire()
    ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition + nERB24Port)
    if 0:
        time.sleep(1.0)
        ERB24.hWin_ERB24RelayOff(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power + nERB24Port)
    mutexERB24.release()


def powerOn(nDVR):
    """Simple routine to power on, via Ignition, a specific DVR -- AlanT"""
    # There is also IgnitionsOn.py (and Ninursag help us PowersOn.py if needed)
    DEBUG = False
    # DEBUG = True

    global aDVRs
    global mutexERB24
    global ERB24_BOARD_NUM
    global nRelayOffset_Ignition
    global nRelayOffset_Power

    lHeadings = aDVRs[0].tolist()  # build a set of keys to the values
    nERB24Port = int(aDVRs[nDVR][lHeadings.index('ERB24')])

    mutexERB24.acquire()
    if 0:
        ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Power + nERB24Port)
        time.sleep(1.0)
    ERB24.hWin_ERB24RelayOn(nBoard=ERB24_BOARD_NUM, nRelay=nRelayOffset_Ignition + nERB24Port)
    mutexERB24.release()


def main():
    """Main entry point for H1 automation"""

    DEBUG = False
    DEBUG = True

    # load the DVR info and format into something I can use
    lines = numpy.genfromtxt("SetupH1.txt", comments="#", delimiter=",", dtype='str', autostrip=True, unpack=True)
    n = len(lines[0])
    i = 0
    while (i < n):
        aDVRs.append(lines[:, i])
        i += 1

    # spawn the tWorker threads
    threads = []
    i = 1  # aDVRs[0] are simply the column headings
    while (i < n):
        strName = "Worker%s" % (str(i))
        #        t1 = threading.Thread()
        #        h1 = H1()
        global jsonResponse
        #        resp = _connect(h1, "172.18.4.100", "9999")
        #        t = threading.Thread(name=strName, target=GetFilePaths(h1))
        t = threading.Thread(name=strName, target=Gs_Test1, args=(i,))
#        t = threading.Thread(name=strName, target=Check_Videos())
        #        t = threading.Thread(name=strName, target=Update_Server())
        #        GetFilePaths(h1)
        #        t = threading.Thread(name=strName, target=SyncDVR, args=(i,))
        #        t = threading.Thread(name=strName, target=HouseKeeping, args=(i,))
        #        t = threading.Thread(name=strName, target=sid_Test, args=())
        #        t = threading.Thread(name=strName, target=tWorker, args=(i,))
        #        t = threading.Thread(name=strName, target=powerOn, args=(i,))
        threads.append(t)
        t.start()
        i += 1

    pass


# end of main()


#
# Entry point for module
#
if (__name__ == "__main__"):
    # sid_Test()
    print("Run Main.py instead!")
