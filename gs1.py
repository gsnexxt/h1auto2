import json
import logging
import logging.config
import os


#
# Values of interest:
SIZE_LBA = 512
SIZE_KB  = 1024
SIZE_MB  = SIZE_KB * SIZE_KB
SIZE_GB  = SIZE_MB * SIZE_KB

SIZE_siKB = 1000
SIZE_siMB = SIZE_siKB * SIZE_siKB
#


#
# The following will calculate the NMEA 0183 checksum.
#
#     Note: http://www.hhhh.org/wiml/proj/nmeaxor.html can be used to verify
#
def hWin_NMEA0183Checksum(s):
    """Calculate a NMEA 0183 checksum."""
    DEBUG = False

    result = 0

    if DEBUG:
        s = "$GPRMC,183733.000,A,4913.4407,N,12249.2003,W,0.06,31.11,091008,,*2D"

    for c in s:
        if (c == '$'):
            continue
        elif (c == '*'):
            break
        else:
            result ^= ord(c)

    if DEBUG:
        print("result = %02X" %result)

    return result


#
# The following will reverse the order of a string.
#
def hWin_strReverse(s):
    """Simply reverse the characters in a srting."""

    txt = []
    for i in range(len(s)-1, -1, -1):
        txt.append(s[i])
    return ''.join(txt)

for i in range()
print(hWin_strReverse("hello"))